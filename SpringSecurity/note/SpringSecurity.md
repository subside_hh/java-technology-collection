<font color=FF0000> **Spring Security学习笔记**</font>

[TOC]

## 1、简介

Web 应用的安全性包括**用户认证**（Authentication）和**用户授权** （Authorization）两个部分；

+ 用户认证指的是：验证某个用户是否为系统中的合法主体，也就是说用户能否访问 该系统。用户认证一般要求用户提供用户名和密码。系统通过校验用户名和密码来完成认 证过程。通俗点说就是**系统认为用户是否能登录** 。
+ 用户授权指的是验证某个用户是否有权限执行某个操作。在一个系统中，不同用户 所具有的权限是不同的。比如对一个文件来说，有的用户只能进行读取，而有的用户可以 进行修改。一般来说，系统会为不同的用户分配不同的角色，而每个角色则对应一系列的 权限。通俗点讲就是**系统判断用户是否有权限去做某些事情**。

`Spring Security`的特点：

+ 和 Spring 无缝整合。
+ 全面的权限控制。 
+ 专门为 Web 开发而设计。
  + 旧版本不能脱离 Web 环境使用。 
  + 新版本对整个框架进行了分层抽取，分成了核心模块和 Web 模块。单独 引入核心模块就可以脱离 Web 环境。
+ 重量级。

`Shiro`：

+ Apache 旗下的轻量级权限控制框架。
+ 特点： 
  + 轻量级。`Shiro `主张的理念是把复杂的事情变简单。针对对性能有更高要求的互联网应用有更好表现。 
  + 通用性。
    + 好处：不局限于 Web 环境，可以脱离 Web 环境使用。 
    + 缺陷：在 Web 环境下一些特定的需求需要手动编写代码定制。

两个框架的选择：

​        Spring Security 是 Spring 家族中的一个安全管理框架，实际上，在 Spring Boot 出现之 前，`Spring Security `就已经发展了多年了，但是使用的并不多，安全管理这个领域，一直 是` Shiro` 的天下。

​         相对于 `Shiro`，在 `SSM` 中整合 Spring Security 都是比较麻烦的操作，所以，Spring Security 虽然功能比 `Shiro `强大，但是使用反而没有` Shiro `多（`Shir`o 虽然功能没有 Spring Security 多，但是对于大部分项目而言，`Shiro` 也够用了）。 <u>自从有了 Spring Boot 之后，Spring Boot 对于 Spring Security 提供了自动化配置方 案，可以使用更少的配置来使用 Spring Security</u>。

​	因此，一般来说，常见的安全管理技术栈的组合是这样的： 

+  `SSM` + `Shiro `
+  `Spring Boot/Spring Cloud` + `Spring Security`

[Spring Security使用之Hello World](https://spring.io/guides/gs/securing-web/)



## 2、原理

**SpringSecurity 本质是一个过滤器链；**

| 从启动是可以获取到过滤器链                                   |
| ------------------------------------------------------------ |
| org.springframework.security.web.context.request.async.**WebAsyncManagerIntegrationFilter** |
| org.springframework.security.web.context.**SecurityContextPersistenceFilter** |
| org.springframework.security.web.header.HeaderWriterFilter org.springframework.security.web.csrf.**CsrfFilter** |
| org.springframework.security.web.authentication.logout.**LogoutFilter** |
| org.springframework.security.web.authentication.**UsernamePasswordAuthenticationFilter** |
| org.springframework.security.web.authentication.ui.**DefaultLoginPageGeneratingFilter** |
| org.springframework.security.web.authentication.ui.**DefaultLogoutPageGeneratingFilter** |
| org.springframework.security.web.savedrequest.**RequestCacheAwareFilter** |
| org.springframework.security.web.servletapi.**SecurityContextHolderAwareRequestFilter** |
| org.springframework.security.web.authentication.**AnonymousAuthenticationFilter** |
| org.springframework.security.web.session.**SessionManagementFilter** |
| org.springframework.security.web.access.**ExceptionTranslationFilter** |
| org.springframework.security.web.access.intercept.**FilterSecurityInterceptor** |

> 过滤器加载过程

1. DelegatingFilterProxy.java

   ```java
   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
         throws ServletException, IOException {
   
      // Lazily initialize the delegate if necessary.
      Filter delegateToUse = this.delegate;
      ……………………
               delegateToUse = initDelegate(wac);
            }
       ……………………
   }
   ```

   initDelegate()方法被调用

   ```java
   protected Filter initDelegate(WebApplicationContext wac) throws ServletException {
      String targetBeanName = getTargetBeanName();
      Assert.state(targetBeanName != null, "No target bean name set");
      Filter delegate = wac.getBean(targetBeanName, Filter.class);
      if (isTargetFilterLifecycle()) {
         delegate.init(getFilterConfig());
      }
      return delegate;
   }
   ```

   在SpringSecurity中`targetBeanName`为**FilterChainProxy**；

2. FilterChainProxy.java

   ```java
   @Override
   public void doFilter(ServletRequest request, ServletResponse response,
         FilterChain chain) throws IOException, ServletException {
      boolean clearContext = request.getAttribute(FILTER_APPLIED) == null;
      ………………
            doFilterInternal(request, response, chain);
         }
        ………………
         doFilterInternal(request, response, chain);
      }
   }
   ```

   该方法的判断后都在调用`doFilterInternal()`方法：

   ```java
   private void doFilterInternal(ServletRequest request, ServletResponse response,
         FilterChain chain) throws IOException, ServletException {
      ………………
      //这里的filter就是过滤器，加载所有的过滤器
      List<Filter> filters = getFilters(fwRequest);
      ………………
   }
   ```

   getFilters()方法获取所有的过滤器：  

   ```java
   private List<Filter> getFilters(HttpServletRequest request) {
      for (SecurityFilterChain chain : filterChains) {
         if (chain.matches(request)) {
            return chain.getFilters();
         }
      }
   
      return null;
   }
   ```

> UserDetailsService 接口

当什么也没有配置的时候，账号和密码是由 Spring Security 定义生成的。而在实际项目中 账号和密码都是从数据库中查询出来的。 所以我们要通过自定义逻辑控制认证逻辑。 如果需要自定义逻辑时，只需要实现 UserDetailsService 接口即可。

```
public interface UserDetailsService {
   UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
```

返回值 **UserDetails** 这个类是系统默认的用户“主体”;

```java
public interface UserDetails extends Serializable {
   /**
    *  表示获取登录用户所有权限
    */
   Collection<? extends GrantedAuthority> getAuthorities();

   /**
    *  表示获取密码
    */
   String getPassword();

   /**
    * 表示获取用户名
    */
   String getUsername();

   /**
    * 表示判断账户是否过期
    */
   boolean isAccountNonExpired();

   /**
    * 表示判断账户是否被锁定
    */
   boolean isAccountNonLocked();

   /**
    * 表示凭证{密码}是否过期
    */
   boolean isCredentialsNonExpired();

   /**
    * 表示当前用户是否可用
    */
   boolean isEnabled();
}
```

![image-20220912164624781](E:\IDEA_Projects\JavaTechnologyCollection\SpringSecurity\note\SpringSecurity.assets\image-20220912164624781.png)

使用该User类即可，

> PasswordEncoder 接口

```java
public interface PasswordEncoder {

   /**
    * 表示把参数按照特定的解析规则进行解析
    */
   String encode(CharSequence rawPassword);

   /**
    * 表示验证从存储中获取的编码密码与编码后提交的原始密码是否匹配。如果密码匹配，则返回 true；如果不匹配，则返回 false。第一个	* 参数表示需要被解析的密码。第二个参数表示存储的密码。
    */
   boolean matches(CharSequence rawPassword, String encodedPassword);

   /**
    * 表示如果解析的密码能够再次进行解析且达到更安全的结果则返回 true，否则返回false。默认返回 false。
    */
   default boolean upgradeEncoding(String encodedPassword) {
      return false;
   }
}
```

![image-20220912170007014](E:\IDEA_Projects\JavaTechnologyCollection\SpringSecurity\note\SpringSecurity.assets\image-20220912170007014.png)

BCryptPasswordEncoder 是 Spring Security 官方推荐的密码解析器，平时多使用这个解析器。

## 3、Web 权限方案

### 1、设置登录系统的账号、密码

>  方式一   配置文件按

```yaml
spring:
  security:
    user:
      name: ThreePure
      password: ThreePure
```

> 方式二  配置类

```java
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("user")
                        .password("password")
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }
}
```

以上这两种方法均不能够灵活运用于现实的业务场景，通常在数据库中保存用户名以及密码进行登录验证；

> 方式三  自定义实现类

从数据库中获取用户名和密码==》

`配置类`

```java
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    PasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder());
        super.configure(auth);
    }
}
```

`实现类`

```java
@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        Users users = usersMapper.selectOne(wrapper);
        if (users == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        System.out.println("查询结果=》user:"+users);
        List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("role");
        return new User(users.getUsername(), new BCryptPasswordEncoder().encode(users.getPassword()), auths);
    }
}
```

`实体类以及方便查询所使用的mybatis-plus接口`

```Java
@Data
@Accessors(chain = true)
public class Users implements Serializable {
    private Integer id;
    private String username;
    private String password;
}
```

```java
@Mapper
public interface UsersMapper extends BaseMapper<Users> {
}
```



### 2、自定义登陆页面

```java
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    ……

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //自定义登录页面
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/user/login")
                //登录成功后跳转的页面
                .defaultSuccessUrl("/test/index").permitAll()
                .and().authorizeRequests()
                //访问白名单
                .antMatchers("/", "/hello/getHello", "/user/login").permitAll()
                .anyRequest().authenticated()
                //关闭csrf防护
                .and().csrf().disable();
    }
}
```

登录页面中用户名以及密码，默认只能使用`username`和`password`，因为在`UsernamePasswordAuthenticationFilter`中有一下配置：

```java
public class UsernamePasswordAuthenticationFilter extends
      AbstractAuthenticationProcessingFilter {
   // ~ Static fields/initializers
   // =====================================================================================

   public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "username";
   public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "password";

   private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;
   private String passwordParameter = SPRING_SECURITY_FORM_PASSWORD_KEY;
   private boolean postOnly = true;
   ……
}
```

如若要改变这个默认配置，可在调用 `usernameParameter()`和`passwordParameter()`方法。

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    http
            //自定义登录页面
            .formLogin()
            .usernameParameter("***")
            .passwordParameter("***")
            ……
```



### 3、基于角色或权限进行访问控制 

**【以下所需数据表资源参见文末附件】**

> **hasAuthority 方法**

**如果当前的主体具有指定的权限，则返回 true,否则返回 false;**

1、在配置类中设置当前访问地址需要哪些权限

```java
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

	……

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                //访问白名单
                .antMatchers("/", "/hello/getHello", "/user/login").permitAll()
                //权限设置 当前登录用户，只有拥有admins权限才可以访问这个路径
                .antMatchers("/test/index").hasAuthority("admins")
                .anyRequest().authenticated() //其他请求， 需要认证
                ……
                //关闭csrf防护
                .and().csrf().disable();
    }
}
```

2、在UserDetailsService中返回User对象设置权限

```java
@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        Users users = usersMapper.selectOne(wrapper);
        if (users == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("admins");
        return new User(users.getUsername(), new BCryptPasswordEncoder().encode(users.getPassword()), auths);
    }
}
```



> **hasAnyAuthority 方法**

**如果当前的主体有任何提供的角色（给定的作为一个逗号分隔的字符串列表）的话，返回 true;**

```
http
        .authorizeRequests()
        //访问白名单
        .antMatchers("/", "/hello/getHello", "/user/login").permitAll()
        //权限设置 当前登录用户，只有拥有admins权限才可以访问这个路径
        //.antMatchers("/test/index").hasAnyAuthority("admins","abcd")
        ……
```



> **hasRole 方法**

**如果用户具备给定角色就允许访问,否则出现 403。 如果当前主体具有指定的角色，则返回 true。**

```
http
        .authorizeRequests()
        //访问白名单
        .antMatchers("/", "/hello/getHello", "/user/login").permitAll()
        .antMatchers("/test/index").hasRole("saler")
```

在UserDetailsService中返回User对象设置权限，默认需要加上1前缀`ROLE_`

```
List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_saler");
```

其原因是在源码中：

```Java
public ExpressionUrlAuthorizationConfigurer(ApplicationContext context) {
   String[] grantedAuthorityDefaultsBeanNames = context.getBeanNamesForType(GrantedAuthorityDefaults.class);
   if (grantedAuthorityDefaultsBeanNames.length == 1) {
      GrantedAuthorityDefaults grantedAuthorityDefaults = context.getBean(grantedAuthorityDefaultsBeanNames[0],
            GrantedAuthorityDefaults.class);
      this.rolePrefix = grantedAuthorityDefaults.getRolePrefix();
   }
   else {
      this.rolePrefix = "ROLE_";
   }
   this.REGISTRY = new ExpressionInterceptUrlRegistry(context);
}
```



> **hasAnyRole方法**

**表示用户具备任何一个条件都可以访问。**

```java
http
        .authorizeRequests()
        //访问白名单
        .antMatchers("/", "/hello/getHello", "/user/login").permitAll()
        .antMatchers("/test/index").hasAnyRole("saler","**")
```

### 4、基于数据库实现权限认证

1. 添加实体类，mapper映射等配置

2. 编写Mapper接口（Mybatis-Plus）

   ```java
   @Mapper
   public interface UsersMapper extends BaseMapper<Users> {
       /**
        * 根据用户 Id 查询用户角色
        */
       List<Role> selectRoleByUserId(Integer userId);
   
       /**
        * 根据用户 Id 查询菜单(权限)
        */
       List<Menu> selectMenuByUserId(Integer userId);
   }
   ```

3. 编写mapper.xml

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
           "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
   <mapper namespace="com.th.securityuse.mapper.UsersMapper">
   
       <!--根据用户 Id 查询角色信息-->
       <select id="selectRoleByUserId" resultType="com.th.securityuse.pojo.Role">
           SELECT r.id, r.name
           FROM study_chip.role r
                    INNER JOIN study_chip.role_user ru ON ru.rid = r.id
           where ru.uid = #{0}
       </select>
   
       <!--根据用户 Id 查询权限信息-->
       <select id="selectMenuByUserId" resultType="com.th.securityuse.pojo.Menu">
           SELECT m.id, m.name, m.url, m.parentid, m.permission
           FROM study_chip.menu m
                    INNER JOIN study_chip.role_menu rm ON m.id = rm.mid
                    INNER JOIN study_chip.role r ON r.id = rm.rid
                    INNER JOIN study_chip.role_user ru ON r.id = ru.rid
           WHERE ru.uid = #{0}
       </select>
   </mapper>
   ```

4. 实现类

   ```java
   @Service("userDetailsService")
   public class CustomUserDetailsServiceImpl implements UserDetailsService {
   
       @Autowired
       private UsersMapper usersMapper;
   
       @Override
       public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
           QueryWrapper<Users> wrapper = new QueryWrapper<>();
           wrapper.eq("username", username);
           Users users = usersMapper.selectOne(wrapper);
           if (users == null) {
               throw new UsernameNotFoundException("用户名不存在");
           }
   
           List<Role> roleList = usersMapper.selectRoleByUserId(users.getId());
           List<Menu> menuList = usersMapper.selectMenuByUserId(users.getId());
   
           //声明一个集合list
           List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
           //处理角色
           for(Role role:roleList){
               SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" + role.getName());
               grantedAuthorityList.add(simpleGrantedAuthority);
           }
           //处理权限
           for (Menu menu:menuList){
               grantedAuthorityList.add(new SimpleGrantedAuthority(menu.getPermission()));
           }
   
           return new User(users.getUsername(), new BCryptPasswordEncoder().encode(users.getPassword()), grantedAuthorityList);
       }
   }
   ```

5. 配置类

   ```java
   @EnableWebSecurity
   public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
   
       @Autowired
       private UserDetailsService userDetailsService;
   
       @Bean
       PasswordEncoder bCryptPasswordEncoder() {
           return new BCryptPasswordEncoder();
       }
   
       @Override
       protected void configure(AuthenticationManagerBuilder auth) throws Exception {
           auth.userDetailsService(userDetailsService)
                   .passwordEncoder(bCryptPasswordEncoder());
           super.configure(auth);
       }
   
       @Override
       protected void configure(HttpSecurity http) throws Exception {
           //自定义403页面
           http.exceptionHandling().accessDeniedPage("/unauth.html");
           http
                   .authorizeRequests()
                   //访问白名单
                   .antMatchers("/", "/user/login").permitAll()
                   //权限设置 当前登录用户，只有拥有admins权限才可以访问这个路径
                   //.antMatchers("/text/index").hasAnyAuthority("admins","abcd")
                   .antMatchers("/test/index").hasRole("管理员")
                   .antMatchers( "/hello/getHello").hasAnyAuthority("menu:system")
                   .anyRequest().authenticated() //其他请求， 需要认证
                   .and()
                   //自定义登录页面
                   .formLogin()
                   .loginPage("/login.html")
                   .loginProcessingUrl("/user/login")
                   //登录成功后跳转的页面
                   .defaultSuccessUrl("/test/index").permitAll()
                   //关闭csrf防护
                   .and().csrf().disable();
       }
   }
   ```

   

### 5、注解开发

一下代码基于上节**基于数据库实现权限认证**。

所有的注解使用前提条件是在配置类或者启动类上开启注解开发：

```java
@EnableGlobalMethodSecurity(securedEnabled = true,prePostEnabled = true,***)
```

> **@Secured**

**判断是否具有角色**，另外需要注意的是这里匹配的字符串需要添加前缀“ROLE_“。

```java
@RequestMapping("/testSecured")
@ResponseBody
@Secured({"ROLE_normal","ROLE_管理员"})
public String helloUser() {
    return "hello,@Secured";
}
```

> **@PreAuthorize**

**进入方法前的权限验证**， @PreAuthorize 可以将登录用 户的 roles/permissions 参数传到方法中。

```java
@RequestMapping("/preAuthorize")
@ResponseBody
//@PreAuthorize("hasRole('ROLE_管理员')")
@PreAuthorize("hasAnyAuthority('menu:system')")
public String preAuthorize(){
    System.out.println("preAuthorize");
    return "preAuthorize";
}
```

`@PreAuthorize`内部可以使用`hasAuthority `、`hasAnyAuthority `、`hasRole`以及`hasAnyRole`四种方法；

> **@PostAuthorize**

**在方法执行后再进行权限验证**，适合验证带有返回值的权限。使用较少；

```java
@RequestMapping("/postAuthorize")
@ResponseBody
@PostAuthorize("hasAnyAuthority('menu:system')")
public String postAuthorize(){
    System.out.println("test--PostAuthorize");
    return "PostAuthorize";
}
```

方法内部代码执行，但是如果没有权限则不会对结果进行返回；

> @PreFilter

**对进入控制器之前对数据进行过滤；**

```
@RequestMapping("getTestPreFilter")
    @PreAuthorize("hasRole('ROLE_管理员')")
    @PreFilter(value = "filterObject.id%2==0")
    @ResponseBody
    public List<Users> getTestPreFilter(@RequestBody List<Users> list){
        list.forEach(t-> {
            System.out.println(t.getId()+"\t"+t.getUsername());
        });
        return list;
    }
```

对于传入的list数组，只有Users对象中id能被2整除的数组才会被传入方法中；

> @PostFilter

**权限验证之后对数据进行过滤** ，留下用户名是 `admin1` 的数据；

```
@RequestMapping("getAll")
@PreAuthorize("hasRole('ROLE_管理员')")
@PostFilter("filterObject.username == 'admin1'")
@ResponseBody
public List<Users> getAllUser(){
    ArrayList<Users> list = new ArrayList<>();
    list.add(new Users(1, "admin1", "admin1"));
    list.add(new Users(2, "admin2", "admin2"));
    return list;
}
```

表达式中的 filterObject 引用的是方法返回值 List 中的某一个元素，此时只能收到`admin1`的相关信息；



### 6、注销登录

配置类：

```
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    ……

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.logout()
                //退出登录地址
                .logoutUrl("/logout")
                //退出登录成功后跳转页面
                        .logoutSuccessUrl("/test/logout").permitAll();
    ……                    
   }                     
```

注销接口：

```html
<body>
登录成功<br>
<a href="/logout">退出</a>
</body>
```

### 7、自动登录|记住我

> 原理

![image-20220918215454361](E:\IDEA_Projects\JavaTechnologyCollection\SpringSecurity\note\SpringSecurity.assets\image-20220918215454361.png)

基于数据库的`记住我`功能，需要建立数据库，当然也可以由SpringSecurity自动创建，这在`JdbcTokenRepositoryImpl.java`中有相关建表语句；

> 创建配置类  或者可以直接写在以上WebSecurityConfig.java配置类中

```java
@Configuration
public class RememberMeConfig {
    @Autowired
    private DataSource dataSource;

    @Bean
    public PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        // 赋值数据源
        jdbcTokenRepository.setDataSource(dataSource);
        // 自动创建表,第一次执行会创建，以后要执行就要删除掉！
        //jdbcTokenRepository.setCreateTableOnStartup(true);
        return jdbcTokenRepository;
    }
}
```

对于这里自动创建数据表，也可以手动创建；

> 在安全配置类中设置

```java
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PersistentTokenRepository persistentTokenRepository;

    ……

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.rememberMe()
                //记住我有效时间，单位s
                .tokenValiditySeconds(60)
                        .tokenRepository(persistentTokenRepository)
                                .userDetailsService(userDetailsService);
                                
    }
}
```

记住时间默认 **2 周时间**。但是可以通过设置状态有效时间，即使项目重新启动下次也可以正常登录。

> 登录页面添加记住我多选框

```html
记住我：<input type="checkbox"name="remember-me"title="记住密码"/><br/>
```

 此处：name 属性值必须位 remember-me.不能改为其他值；

### 8、CSRF 

> 了解CSRF

​		跨站请求伪造（英语：Cross-site request forgery），也被称为 one-click attack 或者 session riding，通常缩写为 CSRF 或者 XSRF， 是一种挟制用户在当前已 登录的 Web 应用程序上执行非本意的操作的攻击方法。跟跨网站脚本（XSS）相比，XSS 利用的是用户对指定网站的信任，CSRF 利用的是网站对用户网页浏览器的信任。

 跨站请求攻击，简单地说，是攻击者通过一些技术手段欺骗用户的浏览器去访问一个 自己曾经认证过的网站并运行一些操作（如发邮件，发消息，甚至财产操作如转账和购买 商品）。由于浏览器曾经认证过，所以被访问的网站会认为是真正的用户操作而去运行。 这利用了 web 中用户身份验证的一个漏洞：简单的身份验证只能保证请求发自某个用户的 浏览器，却不能保证请求本身是用户自愿发出的。 

从 Spring Security 4.0 开始，**默认情况下会启用 CSRF 保护**，以防止 CSRF 攻击应用 程序，Spring Security CSRF 会针对 `PATCH`，`POST`，`PUT `和 `DELETE `方法进行防护。

![image-20220919081917786](E:\IDEA_Projects\JavaTechnologyCollection\SpringSecurity\note\SpringSecurity.assets\image-20220919081917786.png)

> 案例

1. 表单添加隐藏域

   ```html
   <input type="hidden"th:if="${_csrf}!=null"th:value="${_csrf.token}"name="_csrf "/>
   ```

2. 关闭安全配置的类中的 csrf

   ```java
   // http.csrf().disable();
   ```


### 9、单点登录

![image-20220919214129979](E:\IDEA_Projects\JavaTechnologyCollection\SpringSecurity\note\SpringSecurity.assets\image-20220919214129979.png)

在微服务中，各个服务相互独立协同合作，单点登录就是能在一个服务中认证授权后在其他服务中依旧保持认证授权有效；



## 4、微服务权限管理案例

















## 附件：

```sql
/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.27-log : Database - study_chip
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`study_chip` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `study_chip`;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `name` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '菜单名',
  `url` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址',
  `parentid` bigint(20) DEFAULT NULL COMMENT '父id',
  `permission` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `menu` */

insert  into `menu`(`id`,`name`,`url`,`parentid`,`permission`) values (1,'系统管理','',0,'menu:system'),(2,'用户管理','',0,'menu:user');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `name` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色表';

/*Data for the table `role` */

insert  into `role`(`id`,`name`) values (1,'管理员'),(2,'普通用户');

/*Table structure for table `role_menu` */

DROP TABLE IF EXISTS `role_menu`;

CREATE TABLE `role_menu` (
  `mid` bigint(20) DEFAULT NULL COMMENT '菜单id',
  `rid` bigint(20) DEFAULT NULL COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='菜单角色表';

/*Data for the table `role_menu` */

insert  into `role_menu`(`mid`,`rid`) values (1,1),(2,1),(2,2);

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `uid` bigint(20) DEFAULT NULL COMMENT '用户id',
  `rid` bigint(20) DEFAULT NULL COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `role_user` */

insert  into `role_user`(`uid`,`rid`) values (1,1),(2,2);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`) values (1,'tp','123456'),(2,'threepure','threepure');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
```











