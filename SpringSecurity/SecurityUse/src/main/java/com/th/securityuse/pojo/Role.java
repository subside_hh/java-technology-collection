package com.th.securityuse.pojo;

import lombok.Data;

/**
 * @description: 角色实体类
 * @author: HuangJiBin
 * @date: 2022/9/18  14:58
 */
@Data
public class Role {
    private Long id;
    private String name;
}
