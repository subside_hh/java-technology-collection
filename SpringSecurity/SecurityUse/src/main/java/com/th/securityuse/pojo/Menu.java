package com.th.securityuse.pojo;

import lombok.Data;

/**
 * @description: 菜单实体类
 * @author: HuangJiBin
 * @date: 2022/9/18  14:55
 */
@Data
public class Menu {
    private Integer id;
    private String name;
    private String url;
    private Long parentId;
    private String permission;
}
