package com.th.securityuse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.securityuse.pojo.Menu;
import com.th.securityuse.pojo.Role;
import com.th.securityuse.pojo.Users;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description: 用户
 * @author: HuangJiBin
 * @date: 2022/9/14  21:06
 */
@Mapper
public interface UsersMapper extends BaseMapper<Users> {
    /**
     * 根据用户 Id 查询用户角色
     */
    List<Role> selectRoleByUserId(Integer userId);

    /**
     * 根据用户 Id 查询菜单
     */
    List<Menu> selectMenuByUserId(Integer userId);
}
