package com.th.securityuse.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @description: Spring Security配置
 * @author: HuangJiBin
 * @date: 2022/9/12  19:18
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PersistentTokenRepository persistentTokenRepository;

    @Bean
    PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder());
        super.configure(auth);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.rememberMe()
                //记住我有效时间，单位s
                .tokenValiditySeconds(60)
                        .tokenRepository(persistentTokenRepository)
                                .userDetailsService(userDetailsService);

        http.logout()
                //退出登录地址
                .logoutUrl("/logout")
                //退出登录成功后跳转页面
                        .logoutSuccessUrl("/test/logout").permitAll();
        //自定义403页面
        http.exceptionHandling().accessDeniedPage("/unauth.html");
        http
                .authorizeRequests()
                //访问白名单
                .antMatchers("/", "/user/login").permitAll()
                //权限设置 当前登录用户，只有拥有admins权限才可以访问这个路径
                //.antMatchers("/text/index").hasAnyAuthority("admins","abcd")
                .antMatchers("/test/index").hasRole("管理员")
                .antMatchers( "/hello/getHello").hasAnyAuthority("menu:system")
                .anyRequest().authenticated() //其他请求， 需要认证
                .and()
                //自定义登录页面
                .formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/user/login")
                //登录成功后跳转的页面
                .defaultSuccessUrl("/success.html").permitAll()
                //关闭csrf防护
                .and().csrf().disable();
    }
}
