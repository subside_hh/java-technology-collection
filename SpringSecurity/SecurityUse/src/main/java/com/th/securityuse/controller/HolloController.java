package com.th.securityuse.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: controller
 * @author: HuangJiBin
 * @date: 2022/9/12  19:24
 */
@RestController
@RequestMapping("/hello")
public class HolloController {

    @GetMapping("/getHello")
    public String getHello(){
        return "Hello!";
    }
}
