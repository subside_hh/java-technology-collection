package com.th.securityuse.controller;

import com.th.securityuse.pojo.Users;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: HuangJiBin
 * @date: 2022/9/15  8:16
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/index")
    public String index() {
        return "/text/index";
    }

    @RequestMapping("/testSecured")
    @ResponseBody
    @Secured({"ROLE_normal","ROLE_管理员"})
    public String helloUser() {
        return "hello,@Secured";
    }

    @RequestMapping("/preAuthorize")
    @ResponseBody
    //@PreAuthorize("hasRole('ROLE_管理员')")
    @PreAuthorize("hasAnyAuthority('menu:system')")
    public String preAuthorize(){
        System.out.println("preAuthorize");
        return "preAuthorize";
    }

    @RequestMapping("/postAuthorize")
    @ResponseBody
    @PostAuthorize("hasAnyAuthority('menu:system')")
    public String postAuthorize(){
        System.out.println("test--PostAuthorize");
        return "PostAuthorize";
    }

    @RequestMapping("getTestPreFilter")
    @PreAuthorize("hasRole('ROLE_管理员')")
    @PreFilter(value = "filterObject.id%2==0")
    @ResponseBody
    public List<Users> getTestPreFilter(@RequestBody List<Users> list){
        list.forEach(t-> {
            System.out.println(t.getId()+"\t"+t.getUsername());
        });
        return list;
    }


    @RequestMapping("getAll")
    @PreAuthorize("hasRole('ROLE_管理员')")
    @PostFilter("filterObject.username == 'admin1'")
    @ResponseBody
    public List<Users> getAllUser(){
        ArrayList<Users> list = new ArrayList<>();
        list.add(new Users(1, "admin1", "admin1"));
        list.add(new Users(2, "admin2", "admin2"));
        return list;
    }


    @RequestMapping("/logout")
    public String logout() {
        return "logout";
    }

}
