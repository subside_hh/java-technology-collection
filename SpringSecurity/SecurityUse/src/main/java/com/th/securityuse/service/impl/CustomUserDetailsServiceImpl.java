package com.th.securityuse.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.th.securityuse.mapper.UsersMapper;
import com.th.securityuse.pojo.Menu;
import com.th.securityuse.pojo.Role;
import com.th.securityuse.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService; 
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 自定义UserDetailsService接口
 * @author: HuangJiBin
 * @date: 2022/9/12  19:27
 */
@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        Users users = usersMapper.selectOne(wrapper);
        if (users == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        System.out.println("查询结果=》user:"+users);

        List<Role> roleList = usersMapper.selectRoleByUserId(users.getId());
        List<Menu> menuList = usersMapper.selectMenuByUserId(users.getId());

        //声明一个集合list
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        //处理角色
        for(Role role:roleList){
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("ROLE_" + role.getName());
            grantedAuthorityList.add(simpleGrantedAuthority);
        }
        //处理权限
        for (Menu menu:menuList){
            grantedAuthorityList.add(new SimpleGrantedAuthority(menu.getPermission()));
        }

        return new User(users.getUsername(), new BCryptPasswordEncoder().encode(users.getPassword()), grantedAuthorityList);
    }
}
