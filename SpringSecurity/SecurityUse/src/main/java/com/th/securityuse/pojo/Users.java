package com.th.securityuse.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @description: 用户实体类
 * @author: HuangJiBin
 * @date: 2022/9/14  20:56
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Users implements Serializable {
    private Integer id;
    private String username;
    private String password;
}
