package com.th.evolution.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: HelloController
 * @author: HuangJiBin
 * @date: 2022/8/25  7:30
 */
@RestController
@RequestMapping(value = "/hello")
public class HelloController {

}
