package com.th.evolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityEvolutionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityEvolutionApplication.class, args);
    }

}
