package com.th.evolution;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @description: BCryptPasswordEncoder密码解析器测试
 * @author: HuangJiBin
 * @date: 2022/9/12  17:04
 */
@SpringBootTest
public class BCryptPasswordencoderTest {

    private final Log logger = LogFactory.getLog(getClass());

    @Test
    public void test1(){
        // 创建密码解析器
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        // 密码加密
        String threePure = bCryptPasswordEncoder.encode("ThreePure");
        logger.info("加密后密文：" + threePure);
        boolean matches = bCryptPasswordEncoder.matches("ThreePure", threePure);
        logger.info("与明文对比：" + matches);


    }

}
