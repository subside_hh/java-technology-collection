package utils;

import com.th.pojo.Evection;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ThreePure
 * @createDate 2022/11/30
 * @description 个人任务
 */
public class AssigneeTask {

    private static TaskService getTaskService() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取操作任务的服务 TaskService
        return processEngine.getTaskService();
    }

    /**
     * 测试完成个人任务
     */
    @Test
    public void completTask() {
        TaskService taskService = getTaskService();
        //完成任务,参数：流程实例id,完成zhangsan的任务
        Task task = taskService.createTaskQuery()
                .processInstanceId("10001")
                .taskAssignee("rose")
                .singleResult();
        System.out.println("流程实例ID：" + task.getProcessInstanceId());
        System.out.println("任   务ID：" + task.getId());
        System.out.println("责任负责人：" + task.getAssignee());
        System.out.println("任务名称：" + task.getName());
        taskService.complete(task.getId());
    }

    /**
     * 设置流程负责
     */
    @Test
    public void assigneeUEL() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //设置assignee的取值，用户可以在界面上设置流程的执行
        Map<String, Object> assignMap = new HashMap<>();
        assignMap.put("assignee0", "张三");
        assignMap.put("assignee1", "李经理");
        assignMap.put("assignee2", "王总经理");
        assignMap.put("assignee3", "谢财务");
        //启动流程实例，同时还要设置流程定义的assignee的值
        runtimeService.startProcessInstanceByKey("evection", assignMap);
        //输出
        System.out.println(processEngine.getName());
    }

    /**
     * 查询当前个人待执行的任务
     */
    @Test
    public void findPersonalTaskList() {
        //查询当前个人待执行的任务
        String processDefinitionKey = "evection";
        //任务负责人
        String assignee = "A";
        //获取TaskService
        TaskService taskService = getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .includeProcessVariables()
                .taskAssignee(assignee)
                .list();

        for (Task task : list) {
            System.out.println("流程实例ID" + task.getProcessInstanceId());
            System.out.println("任   务ID" + task.getId());
            System.out.println("主任负责人：" + task.getAssignee());
            System.out.println("任务名称：" + task.getName());
        }
    }

    /**
     * 关联 businessKey
     */
    @Test
    public void findProcessInstance() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取操作任务的服务 TaskService
        TaskService taskService = processEngine.getTaskService();
        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //查询流程定义的对象
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("evection")
                .taskAssignee("张三")
                .singleResult();
        //使用task对象获取实例id
        String processInstanceId = task.getProcessInstanceId();
        //使用实例id，获取流程实例对象
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        //使用processInstance，得到 businessKey
        String businessKey = processInstance.getBusinessKey();
        System.out.println("businessKey:" + businessKey);
    }

    /**
     * 完成任务，判断当前用户是否有权限
     */
    public void completTask1() {
        //任务id
        String taskId = "10001";
        //任务负责人
        String assignee = "A";
        //TaskService
        TaskService taskService = getTaskService();
        //完成任务前，需要校验该负责人可以完成当前任务
        // 校验方法：
        // 根据任务id和任务负责人查询当前任务，如果查到该用户有权限，就完成
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskAssignee(assignee)
                .singleResult();
        if (task != null) {
            taskService.complete(taskId);
            System.out.println("完成任务");
        }
    }

}
