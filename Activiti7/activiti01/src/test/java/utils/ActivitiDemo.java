package utils;

import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.*;
import java.util.List;

/**
 * @author ThreePure
 * @createDate 2022/11/29
 * @description Activiti
 */
public class ActivitiDemo {

    /**
     * 流程部署流程
     */
    @Test
    public void testDeployment(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //得到RepositoryService实例
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //使用RepositoryService进行部署,
        //act_re_deployment 流程定义部署表，每部署一次增加一条记录
        //act_re_procdef   流程定义表，部署每个新的流程定义都会在这张表中增加一条记录
        //act_ge_bytearray 流程资源表
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("bpmn/evection.bpmn20.xml")
                .addClasspathResource("bpmn/evection.png")
                .name("出差申请流程")
                .deploy();
        //输出部署信息
        System.out.println("流程部署id："  + deployment.getId());
        System.out.println("流程部署名称："  + deployment.getName());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void testStartProcess(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //得到RuntimeService实例
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //根据流程定义Id启动流程   (key源于 act_re_procdef表)
        ProcessInstance processInstance  = runtimeService.startProcessInstanceByKey("evection");
        //输出内容
        //涉及到的表：
            //act_hi_actinst 流程实例执行历史
            //act_hi_identitylink 流程的参与用户历史信息
            //act_hi_procinst 流程实例历史信息
            // act_hi_taskinst 流程任务历史信息
            // act_ru_execution 流程执行信息
            // act_ru_identitylink 流程的参与用户信息
            // act_ru_task 任务信息
        System.out.println("流程定义id："  + processInstance.getProcessDefinitionId());
        System.out.println("流程实例id："  + processInstance.getId());
        System.out.println("流程活动id："  + processInstance.getActivityId());
    }

    /**
     * 查询当前个人待执行的任务
     */
    @Test
    public void testFindPersonalTaskList(){
        //任务负责人
        String assignee = "A";
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //得到TaskService实例
        TaskService taskService = processEngine.getTaskService();
        //根据流程key 和 任务负责人 查询任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("evection")
                .taskAssignee(assignee)
                .list();
        for (Task task : list) {
            System.out.println("流程实例ID：" + task.getProcessDefinitionId());
            System.out.println("任   务ID：" + task.getId());
            System.out.println("责任负责人：" + task.getAssignee());
            System.out.println("任务名称 ：" + task.getName());
        }
    }

    /**
     * 完成任务
     */
    @Test
    public void completTask(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //得到TaskService实例
        TaskService taskService = processEngine.getTaskService();
        //根据流程key 和 任务的负责人 查询任务返回一个任务对象
        Task task = taskService.createTaskQuery()
                //流程key
                .processDefinitionKey("evection")
                //查询的负责人
                .taskAssignee("A")
                .singleResult();
        //完成任务，参数：任务id
        taskService.complete(task.getId());
    }

    /**
     * 查询流程定义
     */
    @Test
    public void queryProcessDefinition(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //得到RepositoryService实例
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //得到ProcessDefinitionQuery 对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //查询出当前所有的流程定;          条件：processDefinitionKey =evection orderByProcessDefinitionVersion 按照版本排序desc倒叙
        List<ProcessDefinition> definitionList = processDefinitionQuery.processDefinitionKey("evection")
                .orderByProcessDefinitionVersion()
                .desc()
                .list();
        //输出流程定义信息
        for (ProcessDefinition processDefinition : definitionList) {
            System.out.println("流程定义id = " + processDefinition.getId());
            System.out.println("流程定义name = " + processDefinition.getName());
            System.out.println("流程定义key = " + processDefinition.getKey());
            System.out.println("流程定义version = " + processDefinition.getVersion());
            System.out.println("流程部署id = " + processDefinition.getDeploymentId());
        }
    }

    /**
     * 流程删除
     */
    @Test
    public void deleteDeployment(){
        // 流程部署id
        String deploymentId = "1";
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //通过流程引擎获取repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //删除流程定义，如果该流程定义已有流程实例启动则删除时出错
        repositoryService.deleteDeployment(deploymentId);
        //设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级别删除方式，如果流程
        //repositoryService.deleteDeployment(deploymentId, true);
    }

    /**
     * 流程资源下载
     */
    @Test
    public void queryBpmnFile() throws IOException {
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //通过流程引擎获取repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //得到查询器：ProcessDefinitionQuery，设置查询条件,得到想要的流程定义
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("evection")
                .singleResult();
        //通过流程定义信息，得到部署ID
        String deploymentId = processDefinition.getDeploymentId();
        //通过repositoryService的方法，实现读取图片信息和bpmn信息
        //png图片的流
        InputStream pngInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getDiagramResourceName());
        //bpmn文件流
        InputStream bpmnInput = repositoryService.getResourceAsStream(deploymentId, processDefinition.getResourceName());
        //构造OutputStream流
        File file_png  = new File("E:\\IDEA_Projects\\JavaTechnologyCollection\\Activiti7\\log\\evectionflow01.png");
        File file_bpmn = new File("E:\\IDEA_Projects\\JavaTechnologyCollection\\Activiti7\\log\\evectionflow01.bpmn");
        FileOutputStream bpmnOut = new FileOutputStream(file_bpmn);
        FileOutputStream pngOut  = new FileOutputStream(file_png);
        //输入流，输出流的转换
        IOUtils.copy(pngInput,pngOut);
        IOUtils.copy(bpmnInput,bpmnOut);
        //关闭流
        pngOut.close();
        bpmnOut.close();
        pngInput.close();
        bpmnInput.close();
    }

    /**
     * 查看历史信息
     */
    @Test
    public void findHistoryInfo(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //HistoryService
        HistoryService historyService = processEngine.getHistoryService();
        //获取 actinst表的查询对象
        HistoricActivityInstanceQuery historicActivityInstanceQuery = historyService.createHistoricActivityInstanceQuery();
        //查询 actinst表，条件：根据 InstanceId 查询
        //historicActivityInstanceQuery.processInstanceId("2504");
        // 查询 actinst表，条件：根据 DefinitionId 查询
        historicActivityInstanceQuery.processDefinitionId("evection:1:2504");
        //增加排序操作,orderByHistoricActivityInstanceStartTime 根据开始时间排序 asc 升序
        historicActivityInstanceQuery.orderByHistoricActivityInstanceStartTime()
                .asc();
        //查询所有内容
        List<HistoricActivityInstance> activityInstanceList = historicActivityInstanceQuery.list();
        //输出
        for (HistoricActivityInstance instance : activityInstanceList) {
            System.out.println(instance.getActivityId());
            System.out.println(instance.getActivityName());
            System.out.println(instance.getProcessDefinitionId());
            System.out.println(instance.getProcessInstanceId());
        }
    }

    /**
     * 启动流程实例，添加businessKey
     */
    @Test
    public void addBusinessKey(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //启动流程实例，同时还要指定业务标识businessKey，  也就是具体业务表，（比如出差申请表，这个申请表的主键交由activiti管理)与activiti相关联；
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("evection", "1001");
        //输出processInstance相关属性
        System.out.println("业务ID = " + processInstance.getBusinessKey());
    }

    /**
     * 查询流程实例
     */
    @Test
    public void queryProcessInstance(){
        //流程定义key
        String processDefinitionKey = "evection";
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery()
                .processDefinitionKey(processDefinitionKey)
                .list();
        for (ProcessInstance processInstance : list) {
            System.out.println("==================================");
            System.out.println("流程实例ID： " + processInstance.getProcessInstanceId());
            System.out.println("是否执行完成： " + processInstance.getProcessDefinitionId());
            System.out.println("是否暂停： " + processInstance.isSuspended());
            System.out.println("当前活动标识： " + processInstance.getActivityId());
        }
    }

    /**
     * 全部流程实例挂起与激活(操作流程定义为挂起状态，该流程定义下边所有的流程实例全部暂停：)
     */
    @Test
    public void SuspendAllProcessInstance(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取repositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("evection")
                .singleResult();
        //得到当前流程定义的实例是否都为暂停状态
        boolean suspended = processDefinition.isSuspended();
        //流程定义id
        String id = processDefinition.getId();
        //判断是否为暂停
        if (suspended){
            //如果是暂停，可以执行激活操作 ,参数1 ：流程定义id ，参数2：是否激活对应的实例，参数3：激活时间
            repositoryService.activateProcessDefinitionById(id,true,null);
            System.out.println("流程定义：" + id + "已激活");
        }else {
            repositoryService.suspendProcessDefinitionById(id,true,null);
            System.out.println("流程定义：" + id + "已挂起");
        }
    }

    /**
     * 单个流程实例挂起与激活
     */
    @Test
    public void SuspendSingleProcessInstance(){
        //创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //查询流程定义的对象
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId("10001")
                .singleResult();
        //得到当前流程定义的实例是否都为暂停状态
        boolean suspended = processInstance.isSuspended();
        //流程实例id
        String id = processInstance.getId();
        if (suspended){
            //如果是暂停，可以执行激活操作 ,参数：流程定义id
            runtimeService.activateProcessInstanceById(id);
            System.out.println("流程实例：" + id + "已激活");
        }else {
            //如果是激活状态，可以暂停，参数：流程定义id
            runtimeService.suspendProcessInstanceById(id);
            System.out.println("流程定义：" + id + "已挂起");
        }
    }

}
