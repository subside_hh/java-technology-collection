package utils;

import com.th.pojo.Evection;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Event;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ThreePure
 * @createDate 2022/11/30
 * @description Global以及local变量
 */
public class Variables {

    private static TaskService getTaskService() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取操作任务的服务 TaskService
        return processEngine.getTaskService();
    }

    private static RuntimeService getRuntimeService() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        return runtimeService;
    }

    /**
     * 启动流程实例,设置流程变量的值
     */
    @Test
    public void startProcess() {
        RuntimeService runtimeService = getRuntimeService();
        //流程定义key
        String key = "myEvection";
        //创建变量集合
        Map<String, Object> hashMap = new HashMap<>();
        //创建出差pojo对象
        Evection evection = new Evection();
        //设置出差天数
        evection.setNum(2d);
        hashMap.put("evection", hashMap);
        //设置assignee的取值，用户可以在界面上设置流程的执行
        hashMap.put("assignee0", "张三");
        hashMap.put("assignee1", "李经理");
        hashMap.put("assignee2", "王总经理");
        hashMap.put("assignee3", "谢财务");
        //启动流程实例，并设置流程变量的值（把map传入）
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key, hashMap);
        //输出
        System.out.println("流程实例名称=" + processInstance.getName());
        System.out.println("流程定义id==" + processInstance.getProcessDefinitionId());
    }

    /**
     * 完成任务，判断当前用户是否有权限
     */
    @Test
    public void completTask2() {
        //任务
        String key = "myEvection";
        //任务负责人
        String assignee = "张三";
        //TaskService
        TaskService taskService = getTaskService();
        Task task = taskService.createTaskQuery()
                .processDefinitionKey(key)
                .taskAssignee(assignee)
                .singleResult();
        if (task != null) {
            taskService.complete(task.getId());
            System.out.println("任务执行完成");
        }
    }

    /**
     * 通过流程实例id设置全局变量
     */
    @Test
    public void setGlobalVariableByExecutionId(){
        //当前流程实例执行 id，通常设置为当前执行的流程实例
        String executionId = "2601";
        //获取RuntimeService
        RuntimeService runtimeService = getRuntimeService();
        Evection evection = new Evection();
        evection.setNum(3d);
        //通过流程实例 id设置流程变
        runtimeService.setVariable(executionId, "evection",evection);
    }

    /**
     * 通过当前任务设置
     */
    @Test
    public void setGlobalVariableByTaskId(){
        //当前待办任务id
        String taskId = "1401";
        TaskService taskService = getTaskService();
        Evection evection = new Evection();
        evection.setNum(3d);
        taskService.setVariable(taskId, "evection", evection);
    }

    /*===================================local Variable==================================*/

    /**
     * 处理任务时设置local流程变量
     */
    @Test
    public void completTask(){
        //任务id
        String taskId = "10005";
        //TaskService
        TaskService taskService = getTaskService();
        //定义流程变量
        Map<String, Object> hashMap = new HashMap<>();
        Evection evection = new Evection();
        evection.setNum(3d);
        //定义流程变量
        Map<String, Object> variables  = new HashMap<>();
        //变量名是holiday，变量值是holiday对象
        variables.put("evection", evection);
        //设置local变量，作用域为该任务
        taskService.setVariablesLocal(taskId, variables);
        //完成任务
        taskService.complete(taskId);
    }

    /**
     * 通过当前任务设置
     */
    @Test
    public void setLocalVariableByTaskId(){
        //任务id
        String taskId = "10005";
        //TaskService
        TaskService taskService = getTaskService();
        //定义流程变量
        Map<String, Object> hashMap = new HashMap<>();
        Evection evection = new Evection();
        evection.setNum(3d);
        //设置local变量，作用域为该任务
        taskService.setVariableLocal(taskId,"evection", evection);
    }


}
