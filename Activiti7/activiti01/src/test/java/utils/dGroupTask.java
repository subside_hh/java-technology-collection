package utils;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.List;

/**
 * @author ThreePure
 * @createDate 2022/11/30
 * @description 组任务
 */
public class dGroupTask {
    private static TaskService getTaskService() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取操作任务的服务 TaskService
        return processEngine.getTaskService();
    }

    private static RuntimeService getRuntimeService() {
        //获取引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取 RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        return runtimeService;
    }

    /**
     * 根据候选人查询组任务
     */
    @Test
    public void findGroupTaskList() {
        // 流程定义key
        String processDefinitionKey = "evection3";
        // 任务候选人
        String candidateUser = "lisi";
        TaskService taskService = getTaskService();
        //查询组任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .taskCandidateUser(candidateUser)
                .list();
        for (Task task : list) {
            System.out.println("流程实例ID：" + task.getProcessInstanceId());
            System.out.println("任务ID：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("任务名称：" + task.getName());
        }
    }

    /**
     * 拾取组任务 候选人员拾取组任务后该任务变为自己的个人任务
     */
    public void claimTas() {
        TaskService taskService = getTaskService();
        //要拾取的任务id
        String taskId = "10005";
        //任务候选人id
        String userId = "lisi";
        //拾取任务 即使该用户不是候选人也能拾取(建议拾取时校验是否有资格) 校验该用户有没有拾取任务的资格
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskCandidateUser(userId)
                .singleResult();
        if (task != null) {
            //拾取任务
            taskService.claim(taskId, userId);
            System.out.println("任务拾取成功");
        }
    }

    /**
     * 查询个人待办任务
     */
    @Test
    public void findPersonalTaskList(){
        // 流程定义key
        String processDefinitionKey = "evection1";
        // 任务负责人
        String assognee = "zhangsan";
        TaskService taskService = getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .taskAssignee(assognee)
                .list();
        for (Task task : list) {
            System.out.println("流程实例ID：" + task.getProcessInstanceId());
            System.out.println("任务ID：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("任务名称：" + task.getName());
        }
    }

    /**
     * 归还组任务，由个人任务变为组任务，还可以进行任务交接
     */
    @Test
    public void setAssigneeToGroupTask(){
        TaskService taskService = getTaskService();
        // 当前待办任务
        String taskId = "10005";
        // 任务负责人
        String userId = "zhangsan2";
        // 校验userId是否是taskId的负责人，如果是负责人才可以归还组任务
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskAssignee(userId)
                .singleResult();
        if (task != null) {
            // 如果设置为null 归还组任务,该 任务没有负责人
            taskService.setAssignee(taskId, null);
        }
    }

    /**
     * 任务交接,任务负责人将任务交给其它候选人办理该任务
     */
    @Test
    public void setAssigneeToCandidateUser(){
        TaskService taskService = getTaskService();
        // 当前待办任务
        String taskId = "10005";
        // 任务负责人
        String userId = "zhangsan2";
        // 将此任务交给其它候选人办理该 任务
        String candidateuser = "zhangsan";
        // 校验userId是否是taskId的负责人，如果是负责人才可以归还组任务
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskAssignee(userId)
                .singleResult();
        if (task != null){
            taskService.setAssignee(taskId, candidateuser);
        }
    }

}
