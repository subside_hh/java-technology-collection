package com.th.document.metadata.web;

import cn.hutool.core.io.file.FileNameUtil;
import com.alibaba.fastjson.JSONObject;
import com.th.document.metadata.entiry.FileBaseAttributeVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * @date: 2025-01-19 21:42
 * @author: hjb
 * @description: 获取文档的元数据（最后修改人，作者、创建时间，最后修改时间）
 */
@Slf4j
@RestController
public class DocumentMetadataController {


    /**
     * 获取文件信息 <br>
     * 获取文件的[文件名]、[文件类型]、[文件大小]、[创建时间]、[最后修改时间]、[创建人]、[最后修改人]、
     *
     * @param multipartFile 上传的文件
     * @return BaseResult
     */
    @RequestMapping(value = {"/getFileAttribute"}, method = {RequestMethod.POST})
    public FileBaseAttributeVo getFileAttribute(@RequestParam("file") MultipartFile multipartFile) {
        if (multipartFile == null) {
            log.error("获取文件信息接口入参验证不通过");
            throw new RuntimeException("获取文件信息接口入参不合法");
        }

        FileBaseAttributeVo fileBaseAttributeFo = new FileBaseAttributeVo();
        String type = FileNameUtil.extName(multipartFile.getOriginalFilename());
        fileBaseAttributeFo
                .setFileName(multipartFile.getOriginalFilename())
                .setContentType(type)
                .setSize(multipartFile.getSize());

        try (InputStream inputStream = multipartFile.getInputStream()) {
            switch (Objects.requireNonNull(type).toLowerCase()) {
                case "pdf":
                    extractPdfMetadata(inputStream, fileBaseAttributeFo);
                    break;
                case "doc":
                    extractDocMetadata(inputStream, fileBaseAttributeFo);
                    break;
                case "docx":
                    extractDocxMetadata(inputStream, fileBaseAttributeFo);
                    break;
                case "xls":
                    extractXlsMetadata(inputStream, fileBaseAttributeFo);
                    break;
                case "xlsx":
                    extractXlsxMetadata(inputStream, fileBaseAttributeFo);
                    break;
                /* case "ppt":
                    extractPptxMetadata(inputStream, fileBaseAttributeFo);
                    break; */
                case "pptx":
                    extractPptxMetadata(inputStream, fileBaseAttributeFo);
                    break;
                default:
                    log.error("不支持的文件类型: {}", type);
                    throw new RuntimeException("文件类型不支持获取元数据");
            }
        } catch (IOException e) {
            log.error("获取文件属性时发生IO错误", e);
            throw new RuntimeException("获取文件属性时发生IO错误");
        } catch (OpenXML4JException e) {
            log.error("OpenXML4JException 异常", e);
            throw new RuntimeException("OpenXML4JException 异常");
        } catch (Exception e) {
            log.error("获取文件信息接口异常", e);
            throw new RuntimeException(e);
        }

        // fileBaseAttributeFo.getCreationTime()转换成yyyy-MM-dd HH:mm格式
        // 使用 SimpleDateFormat 将 creationTime 转换为 yyyy-MM-dd HH:mm 格式
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        fileBaseAttributeFo.setCreationTimeStr(dateFormat.format(fileBaseAttributeFo.getCreationTime()));
        fileBaseAttributeFo.setLastModifiedTimeStr(dateFormat.format(fileBaseAttributeFo.getLastModifiedTime()));

        return fileBaseAttributeFo;
    }

    /**
     * 获取PDF文件元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException 异常
     */
    private void extractPdfMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException {
        try (PDDocument document = PDDocument.load(inputStream)) {
            PDDocumentInformation info = document.getDocumentInformation();
            fileBaseAttributeFo.setCreator(info.getAuthor())
                    .setLastModifier(info.getAuthor())
                    .setCreationTime(info.getCreationDate().getTime())
                    .setLastModifiedTime(info.getModificationDate().getTime());
        }
    }

    /**
     * 获取doc文件元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException 异常
     */
    private void extractDocMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException {
        try (POIFSFileSystem fs = new POIFSFileSystem(inputStream)) {
            HWPFDocument doc = new HWPFDocument(fs);
            SummaryInformation si = doc.getSummaryInformation();

            String author = si.getAuthor();
            String lastModifiedBy = si.getLastAuthor();
            Date creationDate = si.getCreateDateTime();
            Date lastModifiedDate = si.getLastSaveDateTime();

            fileBaseAttributeFo.setCreator(author)
                    .setLastModifier(lastModifiedBy)
                    .setCreationTime(creationDate)
                    .setLastModifiedTime(lastModifiedDate);
        }
    }

    /**
     * 获取doc文件元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException 异常
     */
    private void extractDocxMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException, OpenXML4JException {
        try (OPCPackage opcPackage = OPCPackage.open(inputStream)) {
            XWPFDocument doc = new XWPFDocument(opcPackage);
            POIXMLProperties.CoreProperties coreProps = doc.getProperties().getCoreProperties();
            fileBaseAttributeFo.setLastModifier("undefined".equals(coreProps.getLastModifiedByUser()) ? null : coreProps.getLastModifiedByUser())
                    .setCreator(coreProps.getCreator())
                    .setCreationTime(coreProps.getCreated())
                    .setLastModifiedTime(coreProps.getModified());
        }
    }

    /**
     * 获取xls文件元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException 异常
     */
    private void extractXlsMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException {
        try (POIFSFileSystem fs = new POIFSFileSystem(inputStream)) {
            HSSFWorkbook workbook = new HSSFWorkbook(fs);
            SummaryInformation si = workbook.getSummaryInformation();
            fileBaseAttributeFo.setLastModifier(si.getLastAuthor())
                    .setCreator(si.getAuthor())
                    .setCreationTime(si.getCreateDateTime())
                    .setLastModifiedTime(si.getLastSaveDateTime());
        }
    }

    /**
     * 获取xlsx文件元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException        异常
     * @throws OpenXML4JException 异常
     */
    private void extractXlsxMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException, OpenXML4JException {
        try (OPCPackage opcPackage = OPCPackage.open(inputStream)) {
            XSSFWorkbook xlsx = new XSSFWorkbook(opcPackage);
            POIXMLProperties.CoreProperties coreProps = xlsx.getProperties().getCoreProperties();
            fileBaseAttributeFo.setLastModifier(coreProps.getLastModifiedByUser())
                    .setCreator(coreProps.getCreator())
                    .setCreationTime(coreProps.getCreated())
                    .setLastModifiedTime(coreProps.getModified());
        }
    }

    /**
     * 获取ppt元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException        异常
     * @throws OpenXML4JException 异常
     */
    private void extractPptMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException, OpenXML4JException {

    }

    /**
     * 获取pptx元数据信息
     *
     * @param inputStream         文件流
     * @param fileBaseAttributeFo 文件元数据信息
     * @throws IOException        异常
     * @throws OpenXML4JException 异常
     */
    private void extractPptxMetadata(InputStream inputStream, FileBaseAttributeVo fileBaseAttributeFo) throws IOException, OpenXML4JException {
        try (OPCPackage opcPackage = OPCPackage.open(inputStream)) {
            XMLSlideShow pptx = new XMLSlideShow(opcPackage);
            POIXMLProperties.CoreProperties coreProperties = pptx.getProperties().getCoreProperties();
            fileBaseAttributeFo.setLastModifier(coreProperties.getLastModifiedByUser())
                    .setCreator(coreProperties.getCreator())
                    .setCreationTime(coreProperties.getCreated())
                    .setLastModifiedTime(coreProperties.getModified());
        }
    }
    
}
