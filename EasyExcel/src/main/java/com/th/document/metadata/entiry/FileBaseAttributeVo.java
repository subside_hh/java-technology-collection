package com.th.document.metadata.entiry;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @date: 2025-01-17 14:36
 * @author: hjb
 * @description: 文件基础属性
 */
public class FileBaseAttributeVo {

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String contentType;

    /**
     * 文件大小
     */
    private long size;

    /**
     * 创建时间 这个字段可能不准确，因为上传的文件通常没有原始的创建时间
     */
    private Date creationTime;

    /**
     * 最后修改时间
     */
    private Date lastModifiedTime;

    /**
     * 创建时间 这个字段可能不准确，因为上传的文件通常没有原始的创建时间
     */
    private String creationTimeStr;

    /**
     * 最后修改时间
     */
    private String lastModifiedTimeStr;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 最后修改人
     */
    private String lastModifier;

    public FileBaseAttributeVo() {
    }

    public String getFileName() {
        return fileName;
    }

    public FileBaseAttributeVo setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public FileBaseAttributeVo setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public long getSize() {
        return size;
    }

    public FileBaseAttributeVo setSize(long size) {
        this.size = size;
        return this;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public FileBaseAttributeVo setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public FileBaseAttributeVo setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
        return this;
    }

    public String getCreator() {
        return creator;
    }

    public FileBaseAttributeVo setCreator(String creator) {
        this.creator = creator;
        return this;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public FileBaseAttributeVo setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
        return this;
    }

    public String getCreationTimeStr() {
        return creationTimeStr;
    }

    public FileBaseAttributeVo setCreationTimeStr(String creationTimeStr) {
        this.creationTimeStr = creationTimeStr;
        return this;
    }

    public String getLastModifiedTimeStr() {
        return lastModifiedTimeStr;
    }

    public FileBaseAttributeVo setLastModifiedTimeStr(String lastModifiedTimeStr) {
        this.lastModifiedTimeStr = lastModifiedTimeStr;
        return this;
    }

    @Override
    public String toString() {
        return "FileBaseAttributeVo{" +
                "fileName='" + fileName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", size=" + size +
                ", creationTime=" + creationTime +
                ", creationTimeStr='" + creationTimeStr + '\'' +
                ", lastModifiedTime=" + lastModifiedTime +
                ", lastModifiedTimeStr='" + lastModifiedTimeStr + '\'' +
                ", creator='" + creator + '\'' +
                ", lastModifier='" + lastModifier + '\'' +
                '}';
    }

    public static List<FileBaseAttributeVo> findSimilarObjects(List<FileBaseAttributeVo> list) {
        Set<FileBaseAttributeVo> result = new HashSet<>();

        list.stream()
                .flatMap(vo1 -> list.stream()
                        .filter(vo2 -> !vo1.equals(vo2) && compare(vo1, vo2))
                        .peek(vo2 -> {
                            result.add(vo1);
                            result.add(vo2);
                        }))
                .collect(Collectors.toList());

        return new ArrayList<>(result);
    }

    private static boolean compare(FileBaseAttributeVo vo1, FileBaseAttributeVo vo2) {
        if (vo1 == null || vo2 == null) {
            return false;
        }

        return areEqual(vo1.getFileName(), vo2.getFileName()) ||
                areEqual(vo1.getContentType(), vo2.getContentType()) ||
                vo1.getSize() == vo2.getSize() ||
                areEqual(vo1.getCreationTime(), vo2.getCreationTime()) ||
                areEqual(vo1.getCreationTimeStr(), vo2.getCreationTimeStr()) ||
                areEqual(vo1.getLastModifiedTime(), vo2.getLastModifiedTime()) ||
                areEqual(vo1.getLastModifiedTimeStr(), vo2.getLastModifiedTimeStr()) ||
                areEqual(vo1.getCreator(), vo2.getCreator()) ||
                areEqual(vo1.getLastModifier(), vo2.getLastModifier());
    }

    private static boolean areEqual(Object obj1, Object obj2) {
        if (obj1 == null && obj2 == null) {
            return true;
        }
        if (obj1 != null) {
            return obj1.equals(obj2);
        }
        return false;
    }
}
