package com.example.service;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-23
 * @Description:
 */
public class SmsServiceImpl implements ISmsService{

    /**
     * 访问ID、即帐号
     */
    private String accessKeyId;

    /**
     * 访问凭证，即密码
     */
    private String accessKeySecret;

    /**
     * 构造函数
     * @param accessKeyId key
     * @param accessKeySecret 密钥
     */
    public SmsServiceImpl(String accessKeyId, String accessKeySecret) {
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
    }

    /**
     * 发送短信
     *
     * @param phone        要发送的手机号
     * @param signName     短信签名-在短信控制台中找
     * @param templateCode 短信模板-在短信控制台中找
     * @param data         要发送的内容
     */
    @Override
    public void send(String phone, String signName, String templateCode, String data) {
        System.out.println("接入短信系统，accessKeyId = " + accessKeyId + ",accessKeySecret=" + accessKeySecret);
        System.out.println("短信发送，phone=" + phone + ",signName=" + signName + ",templateCode=" + templateCode + ",data=" + data);
    }
}
