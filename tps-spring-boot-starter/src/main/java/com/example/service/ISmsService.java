package com.example.service;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-23
 * @Description:
 */
public interface ISmsService {

    /**
     * 发送短信
     *
     * @param phone        要发送的手机号
     * @param signName     短信签名-在短信控制台中找
     * @param templateCode 短信模板-在短信控制台中找
     * @param data         要发送的内容
     */
    void send(String phone, String signName, String templateCode, String data);

}
