package com.example.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-22
 * @Description: 自定义starter属性类
 */
@Data
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tp.sms")
public class SmsProperties implements Serializable {

    /**
     * 访问ID、即帐号
     */
    private String accessKeyId = "a";

    /**
     * 访问凭证，即密码
     */
    private String accessKeySecret = "b";


}
