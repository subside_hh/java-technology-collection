package com.example.config;

import com.example.model.SmsProperties;
import com.example.service.SmsServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-23
 * @Description:
 */
@Configuration
@EnableConfigurationProperties({SmsProperties.class})
public class SmsAutoConfig {

    @Resource
    private SmsProperties smsProperties;

    @Bean
    @ConditionalOnMissingBean
    public SmsServiceImpl smsServiceImpl(){
        return new SmsServiceImpl(smsProperties.getAccessKeyId(), smsProperties.getAccessKeySecret());
    }



}
