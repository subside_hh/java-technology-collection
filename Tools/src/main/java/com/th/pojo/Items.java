package com.th.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author ThreePure
 * @createDate 2022/11/14
 * @description 事项实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Items implements Serializable {

    private String name;

    private String service;

    private String person;


}
