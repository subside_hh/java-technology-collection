package com.th.controller;

import com.deepoove.poi.data.PictureType;
import com.deepoove.poi.data.Pictures;
import com.th.utils.PoiTlUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author ThreePure
 * @createDate 2022/11/14
 * @description 文件Controller
 */
@RestController
public class FileController {

    //PoiTlUtils.generateWord(file.getInputStream(), "E:\\IDEA_Projects\\JavaTechnologyCollection\\Tools\\src\\main\\resources\\out\\AA.docx", map);

    /**
     * @description 渲染后返回网络流
     * @param file 模板文件
     * @param map 渲染数据
     * @param images 渲染的图片集合
     * @param request Request
     * @param response Response
     * @throws IOException IO异常
     */
    @PostMapping("/netStream")
    public void render2NetStream(@RequestPart("file") MultipartFile file,
                              @RequestPart("paramMap") Map<String, Object> map,
                              @RequestPart("images") List<MultipartFile> images,
                              HttpServletRequest request,
                              HttpServletResponse response) throws IOException {

        images.forEach((item) -> {
            String fileName = item.getOriginalFilename();
            try {
                assert fileName != null;
                map.put(fileName.substring(0, fileName.lastIndexOf(".")),
                        Pictures.ofStream(item.getInputStream(), PictureType.JPEG).size(80, 50).create());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        PoiTlUtils.netStream(file, map, request, response);
        //PoiTlUtils.netStreamPdf(file, map, request, response);
    }

}
