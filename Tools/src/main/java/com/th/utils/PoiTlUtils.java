package com.th.utils;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.util.PoitlIOUtils;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * @description: 使用poi_tl实现使用Word模板创建文件
 * 官   网：<a href="http://deepoove.com/poi-tl/">官网</a>
 * GitHub：<a href="https://github.com/Sayi/poi-tl">GitHub</a>
 * 标签：
 * 文本：{{var}}
 * String ：文本
 * TextRenderData ：有样式的文本
 * HyperlinkTextRenderData ：超链接和锚点文本
 * Object ：调用 toString() 方法转化为文本
 * 图片： {{@var}}
 * String ：图片url或者本地路径，默认使用图片自身尺寸
 * PictureRenderData
 * ByteArrayPictureRenderData
 * FilePictureRenderData
 * UrlPictureRenderData
 * 表格： {{#var}}
 * TableRenderData
 * 推荐使用工厂 Tables 、 Rows 和 Cells 构建表格模型
 * 列表： {{*var}}
 * List<String>
 * NumberingRenderData
 * 推荐使用工厂 Numberings 构建列表模型
 * 区块对：  前后两个标签组成，开始标签以?标识，结束标签以/标识：{{?sections}}{{/sections}}
 * 嵌套： {{+var}}
 * DocxRenderData
 * 推荐使用工厂 Includes 构建嵌套模型。
 * @author: HuangJiBin
 * @date: 2022/8/1  17:55
 * @since: OpenJDK 11
 */
public class PoiTlUtils {

    /**
     * @description: 根据Word模板生成Word文件
     * @Param: [templateUrl 模板文件输入流, outUrl 生成的文件路径, map 参数]
     * @Return: void
     */
    public static void generateWord(InputStream templateStream, String outUrl, Map<String, Object> map) throws IOException {
        //compile 编译模板    render 渲染数据
        XWPFTemplate render = XWPFTemplate.compile(templateStream).render(map);
        FileOutputStream fileOutputStream = new FileOutputStream(outUrl);
        //write 输出到流
        render.write(fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        render.close();
    }

    /**
     * @description 返回PDF的网络流
     * @param file 模板文件
     * @param map 渲染数据
     * @param request  Request
     * @param response Response
     * @throws IOException IO异常
     */
    public static void netStreamPdf(MultipartFile file, Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String filename = file.getOriginalFilename();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachment;filename=\"" + agentUtils(request, filename,"pdf") + "\"");
        response.setCharacterEncoding("utf-8");

        //将标签配置为SpringEL模式
        Configure build = Configure.builder().useSpringEL(false).build();
        //compile 编译模板    render 渲染数据
        XWPFTemplate render = XWPFTemplate.compile(file.getInputStream(), build).render(map);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        render.write(stream);
        stream.flush();

        byte[] bytes = stream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        OutputStream outPdf = response.getOutputStream();

        //读取word文档
        XWPFDocument document = new XWPFDocument(byteArrayInputStream);

        //将word转成pdf
        PdfOptions options = PdfOptions.create();
        PdfConverter.getInstance().convert(document, outPdf, options);
        //关闭所有的流
        PoitlIOUtils.closeQuietlyMulti(render, stream, outPdf,byteArrayInputStream);
    }

    public static void main(String[] args) throws IOException {
        /*try {
            //读取word文档
            XWPFDocument document = null;
            try (InputStream in = Files.newInputStream(Paths.get("C:\\Users\\ThreePure\\Desktop\\D.docx"))) {
                document = new XWPFDocument(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //将word转成pdf
            *//*PdfOptions options = PdfOptions.create();
            try (OutputStream outPDF = Files.newOutputStream(Paths.get("C:\\Users\\ThreePure\\Desktop\\D.pdf"))) {
                PdfConverter.getInstance().convert(document, outPDF, options);
            } catch (IOException e) {
                e.printStackTrace();
            }*//*
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        InputStream inDoc = Files.newInputStream(Paths.get("C:\\Users\\ThreePure\\Desktop\\A.docx"));
        byte[] bytes = ITextPdfUtils.docxToPdf(inDoc);
        OutputStream outPDF = Files.newOutputStream(Paths.get("C:\\Users\\ThreePure\\Desktop\\A.pdf"));
        outPDF.write(bytes);
        //outPDF.flush();
        outPDF.close();
    }

    /**
     * @param file     模板文件
     * @param map      渲染数据
     * @param request  Request
     * @param response ResPonse
     * @throws IOException IO异常
     * @description 使用网络流返回渲染后的docx文件
     */
    public static void netStream(MultipartFile file, Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws IOException {

        String filename = file.getOriginalFilename();
        response.setContentType("application/octet-stream;charset=UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=\"" + agentUtils(request, filename,null) + "\"");
        response.setCharacterEncoding("utf-8");

        //将标签配置为SpringEL模式
        Configure build = Configure.builder().useSpringEL(false).build();
        //compile 编译模板    render 渲染数据
        XWPFTemplate render = XWPFTemplate.compile(file.getInputStream(), build).render(map);

        OutputStream out = response.getOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(out);
        render.write(bos);
        bos.flush();
        out.flush();
        //关闭所有的流
        PoitlIOUtils.closeQuietlyMulti(render, bos, out);
    }


    /**
     * @param request  request
     * @param filename 文件名
     * @return 编码后的文件名
     * @throws UnsupportedEncodingException 不支持的编码异常
     * @description 添加对文件名的编码设置
     */
    public static String agentUtils(HttpServletRequest request, String filename, String suffix) throws UnsupportedEncodingException {

        if (suffix != null){
            filename = filename.substring(0, filename.lastIndexOf(".")) + "." + suffix;
        }

        String userAgent = request.getHeader("USER-AGENT");
        if (StringUtils.contains(userAgent, "MSIE")) {
            //IE浏览器
            filename = URLEncoder.encode(filename, "UTF8");
        } else if (StringUtils.contains(userAgent, "Mozilla")) {
            //google,火狐浏览器
            filename = new String(filename.getBytes(), "ISO8859-1");
        } else {
            //其他浏览器
            filename = URLEncoder.encode(filename, "UTF8");
        }
        return filename;
    }


}
