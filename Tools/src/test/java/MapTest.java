import com.sun.tracing.dtrace.ArgsAttributes;
import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author ThreePure
 * @createDate 2022/11/14
 * @description map
 */
public class MapTest {

    @Test
    public void test4Map(){
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Sayi");
        map.put("startTime", "2019-08-04");
        map.put("开发", "开发");
        map.put("测试", "测试");

        forTest(map);
        System.out.println("\n");
        keyTest(map);
        System.out.println("\n");
        entrySetTest(map);
        System.out.println("\n");
        iterator(map);
        System.out.println("\n");
        lambdaTesy(map);
    }

    public void forTest(Map<String, Object> map){
        for (Object value : map.values()) {
            System.out.println();
            System.out.println("第一种:" + value);
        }
    }

    public void keyTest(Map<String, Object> map){
        for (String key: map.keySet()) {
            System.out.println("第二种:" + key + ":" + map.get(key));
        }
    }

    public void entrySetTest(Map<String, Object> map){
        Set<Map.Entry<String, Object>> entrySet = map.entrySet();
        for (Map.Entry entry : entrySet) {
            System.out.println("第三种:" + entry.getKey() + " ：" + entry.getValue());
        }
    }

    public void iterator(Map<String, Object> map){
        Iterator<Map.Entry<String, Object>> entryIterator = map.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            System.out.println("第四种:" + entry.getKey() + " ：" + entry.getValue());
        }
    }

    public void lambdaTesy(Map<String, Object> map){
        map.forEach((key, value) -> {
            System.out.println("第五种:" + key + " ：" + value);
        });
    }



}
