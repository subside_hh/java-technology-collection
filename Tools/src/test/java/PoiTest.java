import com.th.utils.PoiTlUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ThreePure
 * @createDate 2022/11/14
 * @description TODO
 */
public class PoiTest {

    @Test
    public  void param2File() throws IOException {
        Map<String, Object> map = new HashMap<>();
        map.put("title", "Hi, poi-tl Word模板引擎");
        InputStream in = this.getClass().getResourceAsStream("/template.docx");
        //FileInputStream fileInputStream = new FileInputStream("./template.docx");
        PoiTlUtils.generateWord(in, "src/main/resources/out/templateOutPut.docx", map);
    }
}
