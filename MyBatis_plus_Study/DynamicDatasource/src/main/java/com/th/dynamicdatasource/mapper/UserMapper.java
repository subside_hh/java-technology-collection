package com.th.dynamicdatasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.dynamicdatasource.pojo.User;
import org.springframework.stereotype.Repository;
/**
 * @description: UserMapper
 * @author: HuangJiBin
 * @date: 2022/7/11 23:12
 * @since: 1.8
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
