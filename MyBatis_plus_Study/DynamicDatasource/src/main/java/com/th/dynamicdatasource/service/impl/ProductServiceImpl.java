package com.th.dynamicdatasource.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.dynamicdatasource.mapper.ProductMapper;
import com.th.dynamicdatasource.pojo.Product;
import com.th.dynamicdatasource.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * @description: ProductService实现类
 * @author: HuangJiBin
 * @date: 2022/7/17  16:12
 * @since: 1.8
 */
@Service
/**指定使用哪一个数据源*/
@DS("slave_1")
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {
}
