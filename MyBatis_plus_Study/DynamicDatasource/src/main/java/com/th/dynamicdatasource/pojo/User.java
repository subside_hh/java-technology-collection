package com.th.dynamicdatasource.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.th.dynamicdatasource.enums.SexEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: User实体类
 * @author: HuangJiBin
 * @date: 2022/7/11 23:08
 * @since: 1.8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")  //设置实体类对应的表名，解决实体类名与表名不一致的问题
public class User {

    /**value属性：指定主键字段
     * type属性：设置主键生产策略
     *          IdType.AUTO： 使用数据库自增策略，必须确保数据库设置了id自增，否则报错，
     *          ASSIGN_ID（默认）：基于雪花算法策略生成数据id，与数据库是否设置了自增无关
     * */

    /**设置主键，MybatisPlus默认选择实体类中id属性未主键，但是对于表中无id字段（如uid）时需要明确告诉MyBatis-Plus哪一个属性为表主键字段*/
    @TableId(type = IdType.AUTO)
    private Long id;

    /**普通字段，如果使用了驼峰命名，MyBatis会自动转换*/
    private String name;

    private SexEnums sex;

    private Integer age;

    private String email;

}
