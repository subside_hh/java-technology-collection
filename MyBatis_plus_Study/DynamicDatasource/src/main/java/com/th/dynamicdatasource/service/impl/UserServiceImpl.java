package com.th.dynamicdatasource.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.dynamicdatasource.mapper.UserMapper;
import com.th.dynamicdatasource.pojo.User;
import com.th.dynamicdatasource.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @description: UserService实现类
 * @author: HuangJiBin
 * @date: 2022/7/14  8:27
 * @since: 1.8
 */
@Service
/**@DS("master") 不写默认时master*/
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
