package com.th.dynamicdatasource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.dynamicdatasource.pojo.Product;

/**
 * @description: 产品服务
 * @author: HuangJiBin
 * @date: 2022/7/17  16:10
 * @since: 1.8
 */
public interface ProductService extends IService<Product> {
}
