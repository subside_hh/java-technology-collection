package com.th.mybatisx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.mybatisx.pojo.User;
import com.th.mybatisx.service.UserService;
import com.th.mybatisx.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**
* @author ThreePure_HP
* @description 针对表【user】的数据库操作Service实现
* @createDate 2022-07-17 17:00:23
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{

}




