package com.th.mybatisx.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.th.mybatisx.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ThreePure_HP
* @description 针对表【user】的数据库操作Service
* @createDate 2022-07-17 17:00:23
*/
public interface UserService extends IService<User> {

}
