package com.th.mybatisx.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.th.mybatisx.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ThreePure_HP
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-07-17 17:00:23
* @Entity com.th.mybatisx.pojo.User
*/
public interface UserMapper extends BaseMapper<User> {

    /**按照提示输入基本的关键字如inset，选择MyBatisX提示的关键字，然后回车，光标选中输入内容后Alt+Enter，可生产对应的sql*/

    /**添加（实体类属性不为null则插入）*/
    int insertSelective(User user);


    /**删除，根据id和name*/
    int deleteByIdAndName(@Param("id") Long id, @Param("name") String name);

    /**修改(根据id修改name和age)*/
    int updateNameAndAgeById(@Param("name") String name, @Param("age") Integer age, @Param("id") Long id);

    /**查询*/
    List<User> selectAgeAndNameByAgeBetween(@Param("beginAge") Integer beginAge, @Param("endAge") Integer endAge);

    /**排序*/
    List<User> selectAllByIdOrderByAgeDesc(@Param("id") Long id);
}




