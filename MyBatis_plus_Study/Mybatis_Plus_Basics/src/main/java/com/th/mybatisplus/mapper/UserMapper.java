package com.th.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.th.mybatisplus.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @description: TODO
 * @author: HuangJiBin
 * @date: 2022/7/11 23:12
 * @since: 1.8
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
    /**
     * @description: 根据Id查询用户信息未map集合
     * @Param: [id]
     * @Return: java.util.Map<java.lang.String, java.lang.Object>
     */
    Map<String, Object> selectMapById(Long id);

    /**
     * @description: 自定义分页查询Demo  分页查询年龄大于某个值的数据
     * @Param: [page MyBatisPlus所提供的分页对象，必须放在第一位, age 年龄]
     * @Return: com.baomidou.mybatisplus.extension.plugins.pagination.Page<com.th.mybatisplus.pojo.User>
     */
    Page<User> selectPageVo(@Param("page") Page<User> page, @Param("age") Integer age);
}
