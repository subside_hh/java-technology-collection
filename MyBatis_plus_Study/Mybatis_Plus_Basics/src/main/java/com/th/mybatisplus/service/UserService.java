package com.th.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.mybatisplus.pojo.User;

/**
 * @description: userService
 * @author: HuangJiBin
 * @date: 2022/7/14  8:25
 * @since: 1.8
 */
public interface UserService extends IService<User> {

}
