package com.th.mybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: Mybatis-Plus_Basics项目启动类
 * @author: HuangJiBin
 * @date: 2022/7/11 8:06
 * @since: 1.8
 */
@SpringBootApplication
@MapperScan("com.th.mybatisplus.mapper")
public class BasicsApplication {
    public static void main(String[] args) {
        SpringApplication.run(BasicsApplication.class, args);
    }
}
