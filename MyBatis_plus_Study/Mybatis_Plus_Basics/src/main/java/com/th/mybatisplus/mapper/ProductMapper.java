package com.th.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.mybatisplus.pojo.Product;
import org.springframework.stereotype.Repository;

/**
 * @description: 产品mapper
 * @author: HuangJiBin
 * @date: 2022/7/17  10:17
 * @since: 1.8
 */
@Repository
public interface ProductMapper extends BaseMapper<Product> {
}
