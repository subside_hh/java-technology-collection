package com.th.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.mybatisplus.mapper.UserMapper;
import com.th.mybatisplus.pojo.User;
import com.th.mybatisplus.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @description: TODO
 * @author: HuangJiBin
 * @date: 2022/7/14  8:27
 * @since: 1.8
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
