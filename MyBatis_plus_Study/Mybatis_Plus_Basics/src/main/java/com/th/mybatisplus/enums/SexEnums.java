package com.th.mybatisplus.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

/**
 * @description: 性别枚举
 * @author: HuangJiBin
 * @date: 2022/7/17  11:19
 * @since: 1.8
 */
@Getter
public enum SexEnums {

    MALE(1, "男"),
    FEMALE(2,"女");

    /**使用MyBatisPlus通用枚举时配置,另外还需要在配置文件中配置枚举扫描包； 将被标注该注解的属性的值存储到数据库中*/
    @EnumValue
    private Integer sex;
    private String sexName;

    SexEnums(Integer sex, String sexName) {
        this.sex = sex;
        this.sexName = sexName;
    }
}
