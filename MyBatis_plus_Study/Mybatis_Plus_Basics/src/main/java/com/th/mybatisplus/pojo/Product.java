package com.th.mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

/**
 * @description: 产品实体类
 * @author: HuangJiBin
 * @date: 2022/7/17  10:13
 * @since: 1.8
 */
@Data
@TableName("t_product")
public class Product {
    private Long id;
    private String name;
    private Integer price;

    /**表明乐观锁版本号字段*/
    @Version
    private Integer version;
}
