package com.th.mybatisplus;

import com.th.mybatisplus.enums.SexEnums;
import com.th.mybatisplus.mapper.UserMapper;
import com.th.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * @description: 通用枚举测试类
 * @author: HuangJiBin
 * @date: 2022/7/17  11:28
 * @since: 1.8
 */
@SpringBootTest
public class MyBatisPlusEnumTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testEnumsTest(){
        User user = new User();
        user.setName("TP");
        user.setAge(24);
        user.setEmail("th@163.com");
        user.setSex(SexEnums.MALE);
        int insert = userMapper.insert(user);
        System.out.println("受影响的行数：" + insert);
    }
}
