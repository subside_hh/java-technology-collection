package com.th.mybatisplus;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @description: 代码生成器测试类
 * @author: HuangJiBin
 * @date: 2022/7/17  13:55
 * @since: 1.8
 */
public class FastAutoGeneratorTest {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/mybatis_plus?characterEncoding=utf8&useSSL=false",
                        "root",
                        "mysqlpw")
                .globalConfig(builder -> {
                    builder.author("ThreePure") // 设置作者
                            //.enableSwagger()  // 开启 swagger 模式
                            .fileOverride()     // 覆盖已生成文件
                            .outputDir("E://IDEA_Projects//MyBatis_plus_Study//Mybatis_Plus_Basics//src//generator");  // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.th")                  // 设置父包名
                            .moduleName("auto_generator")     // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "E://IDEA_Projects//MyBatis_plus_Study//Mybatis_Plus_Basics//src//generator")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("user1")          // 设置需要生成的表名
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine())   // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
