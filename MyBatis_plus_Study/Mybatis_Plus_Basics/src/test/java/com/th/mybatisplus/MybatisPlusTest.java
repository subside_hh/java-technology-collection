package com.th.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.th.mybatisplus.mapper.UserMapper;
import com.th.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: TODO
 * @author: HuangJiBin
 * @date: 2022/7/11 23:16
 * @since: 1.8
 */
@SpringBootTest
public class MybatisPlusTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testSelectList(){
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);
    }

    @Test
    public void testInset(){
        User user = new User();
        user.setName("张三");
        user.setAge(23);
        user.setEmail("zhangsan@qq.com");
        //返回影响的行数
        int insert = userMapper.insert(user);
        System.out.println("insert Result:" + insert);
        System.out.println("雪花算法=》id:" + user.getId());
    }

    @Test
    public void testDeleteById(){
        int i = userMapper.deleteById(1546874243260694530L);
        System.out.println("delete result: " + i);
    }

    @Test
    public void testDeleteByMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("name", "张三");
        map.put("age", 23);
        int result = userMapper.deleteByMap(map);
        System.out.println("delete result: " + result);
    }

    @Test
    public void testDeleteBatchIds(){
        //  DELETE FROM user WHERE id IN ( ? , ? )
        List<Long> list = Arrays.asList(1L, 2L);
        int result = userMapper.deleteBatchIds(list);
        System.out.println("delete result: " + result);
    }

    @Test
    public void testUpdateById(){
        User user = new User();
        user.setId(4L);
        user.setName("李四");
        user.setEmail("lisi@qq.com");
        int result = userMapper.updateById(user);
        System.out.println("update result: " + result);
    }

    @Test
    public void testSelectById(){
        User user = userMapper.selectById(3L);
        System.out.println("select result:" + user);
    }

    @Test
    public void testSelectBatchIds(){
        List<Long> list = Arrays.asList(1L, 2L, 3L);
        //SELECT id,name,age,email FROM user WHERE id IN ( ? , ? , ? )
        List<User> userList = userMapper.selectBatchIds(list);
        userList.forEach(System.out::println);
    }

    @Test
    public void testSelectByMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Jack");
        map.put("age", 20);
        //SELECT id,name,age,email FROM user WHERE name = ? AND age = ?
        List<User> userList = userMapper.selectByMap(map);
        userList.forEach(System.out::println);
    }

    @Test
    public void testSelectListWrapper(){
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

    /**
     * @description:  自定义方法查询
     * @Return: void
     */
    @Test
    public void testSelectMapById(){
        Map<String, Object> map = userMapper.selectMapById(1L);
        System.out.println(map);
    }


}
