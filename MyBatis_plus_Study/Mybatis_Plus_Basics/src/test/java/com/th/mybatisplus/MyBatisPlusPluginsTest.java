package com.th.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.th.mybatisplus.mapper.ProductMapper;
import com.th.mybatisplus.mapper.UserMapper;
import com.th.mybatisplus.pojo.Product;
import com.th.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @description: MyBatisPlus插件相关的测试
 * @author: HuangJiBin
 * @date: 2022/7/17  0:10
 * @since: 1.8
 */
@SpringBootTest
public class MyBatisPlusPluginsTest {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;

    /**
     * @description: 测试分页插件的使用
     */
    @Test
    public void testPage(){
        Page<User> page = new Page<>(2, 3);

        //返回的还是Page对象，所以也可以直接使用以上page对象
        // SELECT id,name,age,email FROM user LIMIT ?,?
        Page<User> userPage = userMapper.selectPage(page, null);
        //userPage.getPages();     //获取当前分页总页数
        //userPage.getCurrent();   //获取当前页
        //userPage.getRecords();   //分页对象记录列表 (查询出的当前页的所有数据)
        //userPage.getSize();      //每页显示条数
        //userPage.getTotal();     //总条数
        //userPage.hasNext();      //是否存在下一
        //userPage.hasPrevious();  //是否存在上一页
        System.out.println(userPage);
    }

    /**
     * @description: 使用自定义分页查询年龄大于20的数据
     * @Return: void
     */
    @Test
    public void testCustomPage(){
        Page<User> page = new Page<>(2, 3);
        //查询年龄大于20的分页数据 ==》 select id, name, age, email from mybatis_plus.user where age > ? LIMIT ?,?
        Page<User> userPage = userMapper.selectPageVo(page, 20);
        //userPage.getPages();     //获取当前分页总页数
        //userPage.getCurrent();   //获取当前页
        //userPage.getRecords();   //分页对象记录列表 (查询出的当前页的所有数据)

        //userPage.getSize();      //每页显示条数
        //userPage.getTotal();     //总条数
        //userPage.hasNext();      //是否存在下一
        //userPage.hasPrevious();  //是否存在上一页
        System.out.println(userPage);
    }

    /**
     * @description: 乐观锁测试方法 （无锁导致的数据更新冲突异常 | 加锁致使小王因版本号不一致使得无法进行更新）
     */
    @Test
    public void testLock1(){
        // 小李查询商品价格
        Product productLi = productMapper.selectById(1);
        System.out.println("小李查询的商品价格："+productLi.getPrice());
        // 小王查询商品价格
        Product productWang = productMapper.selectById(1);
        System.out.println("小王查询的商品价格："+productWang.getPrice());
        //小李将价格+50
        productLi.setPrice(productLi.getPrice() + 50);
        productMapper.updateById(productLi);
        //小王将价格-30
        productWang.setPrice(productWang.getPrice() - 30);
        int result = productMapper.updateById(productWang);
        //加乐观锁后小李修改后版本号发生变化，使得小王修改无法匹配到条件，可以加一个判断，如果小王更新返回为0，则重新获取数据库中最新数据
        if (result == 0){
            // 重新获取最新数据和更新
            Product productNew = productMapper.selectById(1);
            productNew.setPrice(productNew.getPrice() - 30);
            productMapper.updateById(productNew);
        }

        // 老板查询商品价格
        Product productBoss = productMapper.selectById(1);
        //老板查询的商品价格：70  (但是老板期望的是先加50，再减30，最后价格是120)
        System.out.println("老板查询的商品价格："+productBoss.getPrice());
    }



}
