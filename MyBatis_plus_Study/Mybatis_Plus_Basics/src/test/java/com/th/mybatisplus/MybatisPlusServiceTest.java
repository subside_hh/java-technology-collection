package com.th.mybatisplus;

import com.th.mybatisplus.pojo.User;
import com.th.mybatisplus.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: TODO
 * @author: HuangJiBin
 * @date: 2022/7/14  8:31
 * @since: 1.8
 */
@SpringBootTest
public class MybatisPlusServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void testGetCount(){
        long count = userService.count();

        System.out.println("总记录数："+count);
    }

    @Test
    public void test(){
        //userService.saveOrUpdateBatch() //批量保存或者修改（有id则需改，无id则添加）

        //INSERT INTO user ( id, name, age ) VALUES ( ?, ?, ? )  并不是一次性全部插入，而是单挑循环插入
        List<User> list = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            User user = new User();
            user.setName("TP" + i);
            user.setAge(20 + i);
            list.add(user);
        }
        boolean b = userService.saveBatch(list);
        System.out.println("批量添加成功与否：" + b);
    }
}
