package com.th.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.th.mybatisplus.mapper.UserMapper;
import com.th.mybatisplus.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

/**
 * @description: 测试类
 * @author: HuangJiBin
 * @date: 2022/7/15  7:38
 * @since: 1.8
 */
@SpringBootTest
public class MyBatisPlusWrapperTest {
    @Autowired
    private UserMapper userMapper;

    /**
     * @description: 条件查询 QuaryWrapper
     */
    @Test
    public void testQueryWrapper(){
        //查询用户名包含a，年龄在20到30之间，邮箱信息不为null的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", "a")
                .between("age", 20, 30)
                .isNotNull("email");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
        //SELECT id,name,age,email FROM user WHERE (name LIKE ? AND age BETWEEN ? AND ? AND email IS NOT NULL)
    }

    /**
     * @description: 排序条件
     */
    @Test
    public void testOrderBy(){
        //查询用户信息，先按年龄降序，年龄相同则按id升序排列
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("age")
                .orderByAsc("id");
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
        //SELECT id,name,age,email FROM user ORDER BY age DESC,id ASC
    }

    /**
     * @description: 条件删除。也是使用QueryWrapper
     */
    @Test
    public void testQueryWrapperDelete(){
        //删除邮箱为null的用户信息
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("email");
        int delete = userMapper.delete(queryWrapper);
        System.out.println("result:" + delete);
    }

    /**
     * @description: 条件修改1，使用QueryWrapper
     */
    @Test
    public void testUpdateWrapper1(){
        // 将（年龄大于20并且用户名中包含a）或者邮箱为null的用户信息修改
        // UPDATE user SET name=?, email=? WHERE (age > ? AND name LIKE ? OR email IS NULL)
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.gt("age", 20)
                .like("name", "a")
                .or()
                .isNull("email");
        User user = new User();
        user.setName("小明");
        user.setEmail("xiugai@163.com");
        int result = userMapper.update(user, wrapper);
        System.out.println("Result: " + result);
    }

    /**
     * @description: 条件修改2，使用QueryWrapper  [条件优先级]
     */
    @Test
    public void testUpdateWrapper2(){
        // 将用户名中包含a并且（年龄大于20或邮箱为null）的用户信息修改
        // lambda表达式中条件优先执行
        // UPDATE user SET name=?, email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", "a")
                .and(i->i.gt("age", 20).or().isNull("email"));
        User user = new User();
        user.setName("小红");
        user.setEmail("xiugai@163.com");
        int result = userMapper.update(user, queryWrapper);
        System.out.println("Result: " + result);
    }

    /**
     * @description: 条件组装，selectMaps  查询表中部分字段
     */
    @Test
    public void testSelectMap(){
        //查询用户的用户名，年龄信息
        //SELECT name,age FROM user
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("name", "age");
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        maps.forEach(System.out::println);
    }

    /**
     * @description: 子查询 ，使用inSql方法
     */
    @Test
    public void testInSQL(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // column: 子查询与主查询之间的联系 也就是子查询出的结果的字段，  inValue： 子查询语句
        queryWrapper.inSql("id", "select id from user where id <= 3");
        List<User> list = userMapper.selectList(queryWrapper);
        // SELECT id,name,age,email FROM user WHERE (id IN (select id from user where id <= 3))
        list.forEach(System.out::println);
    }

    /**
     * @description: UpdateWrapper修改
     */
    @Test
    public void testUpdateWrapper(){
        //将用户名中包含"小"并且（年龄大于20或邮箱为null）的用户信息修改
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.like("name", "小")
                .and(i ->i.gt("age", 20).or().isNull("email"));
        updateWrapper.set("name", "老黑")
                .set("email", "laohei@qq.com");
        // UPDATE user SET name=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        int result = userMapper.update(null, updateWrapper);
        System.out.println("result:" + result);
    }

    /**
     * @description: 模拟真实的业务逻辑，按用户提交条件进行查找【这种方式比较麻烦，可以使用condition判断是否使用】
     */
    @Test
    public void testCustomSelect(){
        String userName = "";
        Integer ageBegin = 20;
        Integer ageEnd = 30;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        // import com.baomidou.mybatisplus.core.toolkit.StringUtils; isNotBlank 判断某个字符是否不为空字符串，不为null，不为空白符
        if (StringUtils.isNotBlank(userName)){
            queryWrapper.like("name", userName);
        }
        if (ageBegin != null){
            queryWrapper.ge("age", ageBegin);
        }
        if (ageEnd != null){
            queryWrapper.le("age", ageEnd);
        }
        //SELECT id,name,age,email FROM user WHERE (age >= ? AND age <= ?)
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * @description: 使用condition条件查询,根据用户是否输入自动添加查询条件
     */
    @Test
    public void testCondiction(){
        String userName = "";
        Integer ageBegin = 20;
        Integer ageEnd = 30;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(userName), "name", userName)
                .ge(ageBegin != null, "age", ageBegin)
                .le(ageEnd   != null, "age", ageEnd);
        //SELECT id,name,age,email FROM user WHERE (age >= ? AND age <= ?)
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * @description: Lambda表达式，通过函数式接口，使用实体类属性代替表字段，避免写错字段
     */
    @Test
    public void testLambdaQueryWrapper(){
        String userName = "";
        Integer ageBegin = 20;
        Integer ageEnd = 30;
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(userName), User::getName, userName)
                .ge(ageBegin != null, User::getAge, ageBegin)
                .le(ageEnd != null, User::getAge, ageEnd);
        // SELECT id,name,age,email FROM user WHERE (age >= ? AND age <= ?)
        List<User> list = userMapper.selectList(queryWrapper);
        list.forEach(System.out::println);
    }

    /**
     * @description: LambdaUpdateWrapper修改数据
     */
    @Test
    public void testLambdaUpdateWrapper(){
        //将用户名中包含"小"并且（年龄大于20或邮箱为null）的用户信息修改
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.like(User::getName, "小")
                .and(i ->i.gt(User::getAge, 20).or().isNull(User::getEmail));
        updateWrapper.set(User::getName, "老黑")
                .set(User::getEmail, "laohei@qq.com");
        // UPDATE user SET name=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        int result = userMapper.update(null, updateWrapper);
        System.out.println("result:" + result);
    }

}
