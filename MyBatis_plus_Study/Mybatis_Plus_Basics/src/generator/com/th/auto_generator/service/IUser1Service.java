package com.th.auto_generator.service;

import com.th.auto_generator.entity.User1;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ThreePure
 * @since 2022-07-17
 */
public interface IUser1Service extends IService<User1> {

}
