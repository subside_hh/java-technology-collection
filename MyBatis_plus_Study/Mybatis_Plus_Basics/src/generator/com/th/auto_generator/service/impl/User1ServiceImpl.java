package com.th.auto_generator.service.impl;

import com.th.auto_generator.entity.User1;
import com.th.auto_generator.mapper.User1Mapper;
import com.th.auto_generator.service.IUser1Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ThreePure
 * @since 2022-07-17
 */
@Service
public class User1ServiceImpl extends ServiceImpl<User1Mapper, User1> implements IUser1Service {

}
