package com.th.auto_generator.mapper;

import com.th.auto_generator.entity.User1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ThreePure
 * @since 2022-07-17
 */
public interface User1Mapper extends BaseMapper<User1> {

}
