package com.th.auto_generator.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ThreePure
 * @since 2022-07-17
 */
@Controller
@RequestMapping("/auto_generator/user1")
public class User1Controller {

}
