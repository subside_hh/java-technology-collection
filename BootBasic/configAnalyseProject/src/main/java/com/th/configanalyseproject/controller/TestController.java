package com.th.configanalyseproject.controller;

import com.th.configanalyseproject.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: controlle
 * @author: HuangJiBin
 * @date: 2022/10/3  20:41
 */
@RestController
public class TestController {

    private final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private User user;

    @GetMapping("getUser")
    public Object getUser(){
        logger.info("姓名类型:{}",user.getNickName());
        return user.toString();
    }
}
