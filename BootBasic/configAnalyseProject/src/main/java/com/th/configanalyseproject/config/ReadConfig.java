package com.th.configanalyseproject.config;

import com.th.configanalyseproject.pojo.User;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description:
 * @author: HuangJiBin
 * @date: 2022/10/3  20:41
 */
@Configuration
public class ReadConfig {

    /**
     * ignoreInvalidFields:忽略错误
     * ignoreUnknownFields:忽略未知的属性，默认忽略
     * */
    @Bean
    @ConfigurationProperties(prefix = "u.property", ignoreInvalidFields = true )
    public User getUserInstance() {
        return new User();
    }
}
