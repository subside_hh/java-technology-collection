package com.th.configanalyseproject.pojo;

import lombok.Data;

/**
 * @description: 用户实体类
 * @author: HuangJiBin
 * @date: 2022/10/3  20:42
 */
@Data
public class User {
    private String nickName;

    private Integer age;

    private String address;

    private Boolean likeEat;
}
