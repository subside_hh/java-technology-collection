package com.th.startbeanloading.controller;

import com.th.beanloadingproject.way1.config.ConfigBeans;
import com.th.beanloadingproject.way1.plugin.ModuleAgePluginBean;
import com.th.beanloadingproject.way2.plugin.ModuleNamePluginBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: HuangJiBin
 * @date: 2022/10/2  11:05
 */
@RestController
public class InfoController {
    @Autowired
    private ModuleNamePluginBean nameObj;

    @Autowired
    private ModuleAgePluginBean ageObj;

    @Autowired
    private ConfigBeans configBeans;

    @GetMapping("/name")
    public Object get() {
        return nameObj.getName();
    }

    @GetMapping("/age")
    public Object get2() {
       return ageObj.getAge();
    }

    @GetMapping("/age2")
    public Object get3() {
        //需要再次执行构造函数，
        ModuleAgePluginBean moduleAgePluginBean = configBeans.get();
        return moduleAgePluginBean.getAge();
    }
}
