package com.th.startbeanloading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartBeanLoadingApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartBeanLoadingApplication.class, args);
    }

}
