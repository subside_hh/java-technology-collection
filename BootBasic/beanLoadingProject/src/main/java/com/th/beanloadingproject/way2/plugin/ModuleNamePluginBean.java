package com.th.beanloadingproject.way2.plugin;

import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: HuangJiBin
 * @date: 2022/10/2  10:32
 */
@Component
public class ModuleNamePluginBean {

    static {
        //最先执行
        System.out.println("\n\n ModuleNamePluginBean类中name被扫描到然后自动加载了 \n\n");
    }

    public ModuleNamePluginBean() {
        //其次执行
        System.out.println("ModuleNamePluginBean类中构造方法");
    }

    public String getName() {
        System.out.println("ModuleNamePluginBean类中普通方法");
        return "名字叫张三";
    }

}
