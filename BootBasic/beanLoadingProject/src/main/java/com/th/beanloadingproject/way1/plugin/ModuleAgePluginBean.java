package com.th.beanloadingproject.way1.plugin;

/**
 * @description: 自动配置的插件
 * @author: HuangJiBin
 * @date: 2022/10/2  10:15
 */
public class ModuleAgePluginBean {
    static {
        System.out.println("\n\n ModuleAgePluginBean中age被注入到然后自动加载了\n\n");
    }

    public ModuleAgePluginBean() {
        System.out.println("ModuleAgePluginBean类中无参构造");
    }

    public Integer getAge() {
        System.out.println("ModuleAgePluginBean类中普通方法");
        return 20;
    }
}
