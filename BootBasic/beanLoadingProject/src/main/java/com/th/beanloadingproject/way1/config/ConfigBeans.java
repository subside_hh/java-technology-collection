package com.th.beanloadingproject.way1.config;

import com.th.beanloadingproject.way1.plugin.ModuleAgePluginBean;
import org.springframework.context.annotation.Bean;

/**
 * @description: 配置Bean
 * @author: HuangJiBin
 * @date: 2022/10/2  10:28
 */
public class ConfigBeans {
    @Bean
    public ModuleAgePluginBean get() {
        return new ModuleAgePluginBean();
    }
}
