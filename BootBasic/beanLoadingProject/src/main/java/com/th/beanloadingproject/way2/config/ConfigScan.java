package com.th.beanloadingproject.way2.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * @description: 自动扫描并配置
 * @author: HuangJiBin
 * @date: 2022/10/2  10:35
 */
@ComponentScan("com.th.beanloadingproject.way2.plugin")
public class ConfigScan {
}
