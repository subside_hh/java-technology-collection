import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.RSA;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @description: 使用hutool获取一个密钥
 * @author: HuangJiBin
 * @date: 2022/7/19  15:28
 * @since: 1.8
 */
public class SecretKeyUtils {

    /**
     * @description: 使用hutool实现加密
     */
    @Test
    public void getSecretKey(){
        String source = "xinshanchuang@xsc.com";

        RSA rsa = new RSA();
        PrivateKey privateKey = rsa.getPrivateKey();
        PublicKey publicKey = rsa.getPublicKey();
        System.out.println("privateKey: " + privateKey.getAlgorithm());
        System.out.println("publicKey: " + publicKey.getEncoded());

        // MD加密
        System.out.println("MD5:" + SecureUtil.md5(source));
    }


    /**
     * @description: 解析Token中的值测试
     */
    @Test
    public void testResolveToken(){
        // 创建解析对象，使用的算法和secret要与创建token时保持一致
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("DF67EBA648A39D1C7B0E1DDAC9B4F286")).build();
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXgiOiLnlLciLCJ0ZWwiOiIxODMqKioqMDExMiIsImV4cCI6MTY1ODIyMjM4OSwidXNlcklkIjoiMTE4MDYwMTAwMjM3OCJ9.leS1X7YgziQMYIjbrFAPOTpOOK5I5f15O872Apzg8fU";
        // 解析指定的token
        DecodedJWT decodedJWT = jwtVerifier.verify(token);
        // 获取解析后的token中的payload信息
        Claim userId = decodedJWT.getClaim("userId");
        Claim userName = decodedJWT.getClaim("tel");
        Claim sex = decodedJWT.getClaim("sex");
        System.out.println(userId.asInt());
        System.out.println(userName.asString());
        System.out.println(sex.asString());
        // 输出超时时间
        System.out.println(decodedJWT.getExpiresAt());
    }
}
