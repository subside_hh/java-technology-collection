package com.th.jwt;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @description: 手写JWT加密 和解密校验原理
 * @author: HuangJiBin
 * @date: 2022/7/18  21:24
 * @since: 1.8
 * 1.第一部分：header （头部）
 * {
 *  Typ="jwt"  ---类型为jwt
 *  Alg:"HS256"  --加密算法为hs256
 * }
 * 2.第二部分：playload（载荷）  携带存放的数据  用户名称、用户头像之类 注意铭感数据
 * 标准中注册的声明 (建议但不强制使用) ：
 * iss: jwt签发者
 * sub: jwt所面向的用户
 * aud: 接收jwt的一方
 * exp: jwt的过期时间，这个过期时间必须要大于签发时间
 * nbf: 定义在什么时间之前，该jwt都是不可用的.
 * iat: jwt的签发时间
 * jti: jwt的唯一身份标识，主要用来作为一次性token,从而回避重放攻击。
 * 3.第三部分：Signature(签名）
 * JWT优缺点
 * 优点：
 * 1. 无需再服务器存放用户的数据，减轻服务器端压力
 * 2. 轻量级、json风格比较简单
 * 3. 跨语言
 * 缺点：
 * jwt一旦生成后期无法修改：
 * 1. 无法更新jwt有效期
 * 2. 无法销毁一个jwt
 * jwt 90天以后过期 提前60天过期
 */
public class Principle {

    // Signature(签名）
    static String signingKey = "ThreePure";

    public static void main(String[] args) throws UnsupportedEncodingException {
        String jwtToken = getJwtToken("ThreePure", "1180601002376");
        System.out.println("jwtToken: " + jwtToken);

        boolean verify = verify("eyJBbGciOiJIUzI1NiJ9.eyJ0aW1lIjoxNjU4MTUzMjA4NDc0LCJ1c2VyTmFtZSI6IlRocmVlUHVyZSIsInVzZXJJZCI6IjExODA2MDEwMDIzNzYifQ==.2e837c574dc34b38088d25d86e7dfd84");
        System.out.println("未被非法纂改：" + verify);
    }

    /**
     * @description: 传入用户名用户id，返回JWT
     * @Param: [userName 用户名, userId 用户id]
     * @Return: java.lang.String  JWT
     */
    public static String getJwtToken(String userName,String userId){
        //header
        JSONObject header = new JSONObject();
        header.put("Alg", "HS256");
        //payload
        JSONObject payload = new JSONObject();
        payload.put("userName", userName);
        payload.put("userId", userId);
        //存储当前时间毫秒描述，防止每次的JWT都一样
        payload.put("time", System.currentTimeMillis());
        //base64 对实现数据进行编码
        String jwtHeader = Base64.getEncoder().encodeToString(header.toJSONString().getBytes());
        String jwtPlaLoad = Base64.getEncoder().encodeToString(payload.toJSONString().getBytes());


        //前面，后面加盐，这个签名一般放在服务端，防止简单的数据被强行试出来
        String sign = DigestUtils.md5Hex(payload.toJSONString() + signingKey);
        return jwtHeader + "." +jwtPlaLoad + "." + sign;
    }

    /**
     * @description: 校验jwt数据死否被纂改
     * @Param: [jwt]  jwt
     * @Return: boolean
     */
    public static boolean verify(String jwt) throws UnsupportedEncodingException {
        String jwtPayLoadStr = new String(Base64.getDecoder().decode(jwt.split("\\.")[1].getBytes()), "UTF-8");
        String jwtSignatureStr = jwt.split("\\.")[2];
        return DigestUtils.md5Hex(jwtPayLoadStr + signingKey).equals(jwtSignatureStr);
    }


}
