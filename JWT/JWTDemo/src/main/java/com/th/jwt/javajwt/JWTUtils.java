package com.th.jwt.javajwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 使用java-jwt开源库实现JWT(对称加密)
 * @author: HuangJiBin
 * @date: 2022/7/19  14:34
 * @since: 1.8
 * @target: <a href="https://blog.csdn.net/weixin_45070175/article/details/118559272">...</a>
 */
public class JWTUtils {
    /** 签名密钥*/
    private static final String SECRET_KEY = "DF67EBA648A39D1C7B0E1DDAC9B4F286";

    /**
     * @description: 返回一个Token
     * @Param: [payload]  Token中payload中要保存的数据，
     * @Return: java.lang.String
     */
    public static String getToken(Map<String, String> payload){
        //指定Token的过期时间
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);

        JWTCreator.Builder builder = JWT.create();
        // 构建payload
        payload.forEach((k,v) -> builder.withClaim(k,v));

        //指定过期时间和签名算法
        String token = builder.withExpiresAt(calendar.getTime())
                .sign(Algorithm.HMAC256(SECRET_KEY));

        return token;
    }

    /**
     * @description: 解析token
     * @Param: [token]  Token字符串
     * @Return: com.auth0.jwt.interfaces.DecodedJWT  解析后的Token
     */
    public static DecodedJWT decodedToken(String token){
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(SECRET_KEY)).build();
        return jwtVerifier.verify(token);
    }

    public static void main(String[] args) throws InterruptedException {
        Map<String, String> payload = new HashMap<>();
        payload.put("userId", "1180601002378");
        payload.put("sex", "男");
        payload.put("tel", "183****0112");
        String token = getToken(payload);
        System.out.println("Token: " + token);
        System.out.println("===============================");
        Thread.sleep(1000);

        DecodedJWT decodedJWT = decodedToken(token);
        System.out.println("getToken:" + decodedJWT.getToken());
        System.out.println("getHeader:" + decodedJWT.getHeader());
        System.out.println("getPayload:" + decodedJWT.getPayload());
        System.out.println("getSignature:" + decodedJWT.getSignature());
        System.out.println("LocalDateTime:"+ LocalDateTime.now());
        //过期时间
        System.out.println("getExpiresAt:" + decodedJWT.getExpiresAt());

        Map<String, Claim> claims = decodedJWT.getClaims();
        for (Map.Entry<String, Claim> entry : claims.entrySet()) {
            System.out.println(entry.getKey() + "：" + entry.getValue().asString());
        }
    }

}
