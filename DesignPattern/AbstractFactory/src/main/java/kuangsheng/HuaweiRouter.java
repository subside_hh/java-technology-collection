package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:25
 * @description: 华为路由器类
 * @since 1.8
 */
public class HuaweiRouter implements RouterProduct{
    @Override
    public void start() {
        System.out.println("华为路由器正在开启");
    }

    @Override
    public void showdown() {
        System.out.println("华为路由器正在关机");
    }

    @Override
    public void openWifi() {
        System.out.println("华为路由器已打开Wifi");
    }

    @Override
    public void setting() {
        System.out.println("华为路由器正在进行设置");
    }
}
