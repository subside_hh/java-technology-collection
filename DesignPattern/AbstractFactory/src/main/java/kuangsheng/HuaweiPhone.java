package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:24
 * @description: 华为手机类
 * @since 1.8
 */
public class HuaweiPhone implements PhoneProduct{
    @Override
    public void start() {
        System.out.println("华为 Start");
    }

    @Override
    public void showdown() {
        System.out.println("华为 Showdown");
    }

    @Override
    public void callup() {
        System.out.println("华为 电话");
    }

    @Override
    public void sendSMS() {
        System.out.println("华为 短信");
    }
}
