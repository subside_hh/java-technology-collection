package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:33
 * @description: 华为工厂
 * @since 1.8
 */
public class HuaweiFactory implements ProductFactory{
    @Override
    public PhoneProduct phoneProduct() {
        return new HuaweiPhone();
    }

    @Override
    public RouterProduct routerProduct() {
        return new HuaweiRouter();
    }
}
