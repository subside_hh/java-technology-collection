package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:19
 * @description: 手机产品接口
 * @since 1.8
 */
public interface PhoneProduct {
    void start();
    void showdown();
    void callup();
    void sendSMS();
}
