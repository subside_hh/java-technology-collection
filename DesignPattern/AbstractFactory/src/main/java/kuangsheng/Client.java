package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:36
 * @description: 客户端
 * @since 1.8
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("+++++Mi产品+++++");
        //创建小米工厂
        MiFactory miFactory = new MiFactory();
        PhoneProduct miPhoneProduct = miFactory.phoneProduct();
        miPhoneProduct.start();
        miPhoneProduct.callup();
        RouterProduct miRouterProduct = miFactory.routerProduct();
        miRouterProduct.start();
        miRouterProduct.openWifi();

        System.out.println("+++++Huawei产品+++++");
        //创建华为工厂
        HuaweiFactory huaweiFactory = new HuaweiFactory();
        PhoneProduct huawiePhoneProduct = huaweiFactory.phoneProduct();
        huawiePhoneProduct.start();
        huawiePhoneProduct.callup();
        RouterProduct huawieRouterProduct = miFactory.routerProduct();
        huawieRouterProduct.start();
        huawieRouterProduct.openWifi();
    }
}
