package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:32
 * @description: 小米工厂
 * @since 1.8
 */
public class MiFactory implements ProductFactory{
    @Override
    public PhoneProduct phoneProduct() {
        return new MiPhone();
    }

    @Override
    public RouterProduct routerProduct() {
        return new MiRouter();
    }
}
