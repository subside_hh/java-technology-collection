package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:28
 * @description: 抽象产品工厂类 == 抽象的抽象
 * @since 1.8
 */
public interface ProductFactory {
    /**生产手机*/
    PhoneProduct phoneProduct();

    /**生产路由器*/
    RouterProduct routerProduct();
}
