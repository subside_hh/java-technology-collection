package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:22
 * @description: 小米手机
 * @since 1.8
 */
public class MiPhone implements PhoneProduct{
    @Override
    public void start() {
        System.out.println("MIUI Start");
    }

    @Override
    public void showdown() {
        System.out.println("MIUI Showdown");
    }

    @Override
    public void callup() {
        System.out.println("MIUI 电话");
    }

    @Override
    public void sendSMS() {
        System.out.println("MIUI 短信");
    }
}
