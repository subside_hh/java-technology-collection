package kuangsheng;

/**
 * @author ThreePure
 * @date 2021/10/30 17:20
 * @description: 路由器产品接口
 * @since 1.8
 */
public interface RouterProduct {
    void start();
    void showdown();
    void openWifi();
    void setting();
}
