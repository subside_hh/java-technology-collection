package tuling;

import java.util.ArrayList;
import java.util.List;


/**
 * @author ThreePure
 * @date 2021/11/1 14:18
 * @description: 观察者模式测试
 * @since 1.8
 */
public class ObserverTest {
    public static void main(String[] args) {
        //初始化主题对象
        Subject subject = new Subject();
        //为主题对象添加感兴趣的观察者
        Task1 task1 = new Task1();
        Task2 task2 = new Task2();
        subject.addObserver(task1);
        subject.addObserver(task2);

        //主题对象发生改变，通知所有的观察者主题对象发生改变【触发每一个观察者的更新操作】
        subject.notifyObserver("第一次更新数据");
        System.out.println("=======================>");

        //移除一个观察者
        subject.removeObserver(task1);
        //移除后的观察者不再接收到主题对象发出的通知
        subject.notifyObserver("第二次更新数据");
    }
}

/**
 * 主题对象
 */
class Subject {
    //容器
    private List<Observer> containers = new ArrayList<>();

    //add 添加对其感兴趣的观察者
    public void addObserver(Observer observer) {
        containers.add(observer);
    }

    //remove 移除对其感兴趣的观察者
    public void removeObserver(Observer observer) {
        containers.remove(observer);
    }

    /**
     * @description: 通知观察者进行更改
     * @Param: [object]
     * @Return: void
     */
    public void notifyObserver(Object object) {
        //分别对所有的观察者进行通知，观察者对其进行下一步操作。
        for (Observer item : containers) {
            item.update(object);
        }
    }
}

/**
 * 观察者
 */
interface Observer {
    //主题对象发送改变时进行更新
    void update(Object object);
}

/**定义具体的观察者*/
class Task1 implements Observer{

    @Override
    public void update(Object object) {
        System.out.println("【task1】接收更新数据： "+object+",task1将会进行进一步操作");
    }
}

class Task2 implements Observer{

    @Override
    public void update(Object object) {
        System.out.println("【task2】接收更新数据： "+object+",task2将会进行进一步操作");
    }
}

