package kuangshen.demo2;

import java.util.Date;

/**
 * @author ThreePure
 * @date 2021/10/31 7:53
 * @description: 模仿一个视频客户端：克隆
 * @since 1.8
 */
public class Bilibili {
    public static void main(String[] args) throws CloneNotSupportedException {
        //原型对象
        Date date = new Date();
        Video v1 = new Video("Java", date);
        Video v2 = (Video) v1.clone();

        System.out.println("v1=>"+v1);
        System.out.println("v2=>"+v2);

        System.out.println("===================>");
        date.setTime(22131231);
        System.out.println("v1=>"+v1);
        System.out.println("v2=>"+v2);
    }
}
