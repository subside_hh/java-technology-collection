package kuangshen.demo1;

import java.util.Date;

/**
 * @author ThreePure
 * @date 2021/10/31 7:53
 * @description: 模仿一个视频客户端：克隆   (浅克隆)
 * @since 1.8
 */
public class Bilibili {
    public static void main(String[] args) throws CloneNotSupportedException {
        //原型对象
        Date date = new Date();
        Video v1 = new Video("Java从入门到入土", date);
        System.out.println("v1=>"+v1);
        System.out.println("v1=>hash: "+v1.hashCode());

        //传统方式，如果需要再创建一个对象v2，那么需要再次用new关键字创建一个对象
        Video v2 = new Video("Java从入门到入土", date);
        System.out.println("v2=>"+v2);
        System.out.println("v2=>hash: "+v2.hashCode());

        //通过克隆产生一个对象3
        Video v3 = (Video) v1.clone();
        System.out.println("v3=>"+v3);
        System.out.println("v3=>hash: "+v3.hashCode());

        /*
        * v1=>Video{name='Java从入门到入土', createTime=Sun Oct 31 08:00:25 CST 2021}
        * v1=>hash: 1735600054
        * v2=>Video{name='Java从入门到入土', createTime=Sun Oct 31 08:00:25 CST 2021}
        * v2=>hash: 21685669
        * v3=>Video{name='Java从入门到入土', createTime=Sun Oct 31 08:00:25 CST 2021}
        * v3=>hash: 2133927002
        * 通过打印可以发现clone的确是创造了两个Video对象，但是hash是不同的，这说明这两个对象独立的对象。可以通过set方法进行修改clone出的对象。
        * */

        v3.setName("C语音从入门到进阶");
        System.out.println("v3=>"+v3);
        System.out.println("v3=>modify=>hash: "+v3.hashCode());
        System.out.println("---------------------");

        //以上clone为浅克隆，此时v3是指向v1的地址，v1改变，v3也会改变。
        Date date1 = new Date();
        Video v4 = new Video("Java", date1);
        Video v5 = (Video) v4.clone();

        System.out.println("v4=>"+v4);
        System.out.println("v5=>"+v5);

        System.out.println("===================>");
        date1.setTime(22131231);
        System.out.println("v4=>"+v4);
        System.out.println("v5=>"+v5);
    }
}
