package java.class_diagram.composition;

/**
 * @author ThreePure
 * @date 2021/10/30 11:31
 * @description: 组合关系（Composition）也是整体与部分的关系，但是整体与部分不可以分开。
 * Person与 IDCard、Head，那么Head(头)和 Person（人）【人必须有头，这是不可分的】就是组合，IDCad和Person就是聚合。
 * @since 1.8
 */
public class Person {
    private IDCard idCard;
    /*创建一个Person对象就会创建一个Head对象*/
    private Head head = new Head();
}
