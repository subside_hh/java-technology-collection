package java.class_diagram.dependence;

/**
 * @author ThreePure
 * @date 2021/10/30 10:35
 * @description: 类图依赖关系
 *
 * @since 1.8
 */
public class PersonServiceBean {
    private PersonDao personDao;
    public void save(Person person){}
    public IDCard getCard(Integer personId){
        return new IDCard();
    }
    public void modify(){
        Department department = new Department();
    }

}
