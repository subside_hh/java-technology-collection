package java.class_diagram.generalization;

/**
 * @author ThreePure
 * @date 2021/10/30 10:57
 * @description: 泛化关系
 * @since 1.8
 */
public class PersonServiceBean extends DaoSupport{
    @Override
    public void save(Object entity) {
        super.save(entity);
    }

    @Override
    public void delete(Object id) {
        super.delete(id);
    }
}
