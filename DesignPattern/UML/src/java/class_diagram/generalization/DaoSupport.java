package java.class_diagram.generalization;

/**
 * @author ThreePure
 * @date 2021/10/30 10:56
 * @description: 泛化
 * @since 1.8
 */
public class DaoSupport {
    public void save(Object entity){}
    public void delete(Object id){}
}
