package java.class_diagram.implementation;

/**
 * @author ThreePure
 * @date 2021/10/30 11:03
 * @description: 实现
 * @since 1.8
 */
public interface PersonService {
    public void delete(Integer id);
}
