package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 14:04
 * @description: 模板方法模式
 * @since 1.8
 */
public class TemplateMethodTest {
    public static void main(String[] args) {
        AbstractClass subClass = new SubClass();
        subClass.operation();
        System.out.println("===================");
        AbstractClass subClass1 = new SubClass1();
        subClass1.operation();
    }
}


/**定义一个抽象类【定义一个操作的算法骨架，而将一些步骤延迟到子类中】*/
abstract class AbstractClass{
    public void operation(){
        //原有的一些步骤和方法
        System.out.println(" pre ... ");
        System.out.println(" step1 ");
        System.out.println(" step2 ");

        templateMethod();
    }

    abstract protected void templateMethod();
}

/**子类的实现【完成抽象类的templateMethod（）方法】*/
class SubClass extends AbstractClass{

    @Override
    protected void templateMethod() {
        System.out.println("SubClass 中实现抽象类的templateMethod（）方法");
    }
}

/**子类的实现【完成抽象类的templateMethod（）方法】*/
class SubClass1 extends AbstractClass{

    @Override
    protected void templateMethod() {
        System.out.println("SubClass1 中实现抽象类的templateMethod（）方法AAAA");
    }
}