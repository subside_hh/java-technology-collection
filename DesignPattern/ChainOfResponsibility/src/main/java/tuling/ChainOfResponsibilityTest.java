package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 14:58
 * @description: 责任链模式测试
 * @since 1.8
 */
public class ChainOfResponsibilityTest {
    public static void main(String[] args) {
        Request request = new Request.RequestBuilder().frequentlyOk(true).loggedOn(false).build();

        RequestFrequentHandler requestFrequentHandler = new RequestFrequentHandler(new LoggingHandler(null));

        if (requestFrequentHandler.process(request)){
            System.out.println("正常业务处理");
        }else {
            System.out.println("访问异常");
        }
    }
}

class Request {
    //是否已登录
    private boolean loggedOn;
    //是否频繁访问
    private boolean frequentOk;
    //是否允许访问
    private boolean isPermits;
    //是否为敏感词汇
    private boolean containsSensitiveWords;
    //请求体
    private String requestBody;

    public Request(boolean loggedOn, boolean frequentOk, boolean isPermits, boolean containsSensitiveWords) {
        this.loggedOn = loggedOn;
        this.frequentOk = frequentOk;
        this.isPermits = isPermits;
        this.containsSensitiveWords = containsSensitiveWords;
    }

    static class RequestBuilder {
        //是否已登录
        private boolean loggedOn;
        //是否频繁访问
        private boolean frequentOk;
        //是否允许访问
        private boolean isPermits;
        //是否为敏感词汇
        private boolean containsSensitiveWords;

        RequestBuilder loggedOn(boolean loggedOn) {
            this.loggedOn = loggedOn;
            return this;
        }

        RequestBuilder frequentlyOk(boolean frequentOk) {
            this.frequentOk = frequentOk;
            return this;
        }

        RequestBuilder isPermits(boolean isPermits) {
            this.isPermits = isPermits;
            return this;
        }

        RequestBuilder containsSensitiveWords(boolean containsSensitiveWords) {
            this.containsSensitiveWords = containsSensitiveWords;
            return this;
        }

        public Request build() {
            Request request = new Request(loggedOn, frequentOk, isPermits, containsSensitiveWords);
            return request;
        }
    }


    public boolean isLoggedOn() {
        return loggedOn;
    }

    public boolean isFrequentOk() {
        return frequentOk;
    }

    public boolean isPermits() {
        return isPermits;
    }

    public boolean isContainsSensitiveWords() {
        return containsSensitiveWords;
    }
}

/**
 * 抽象的一个节点
 */
abstract class Handler {
    Handler next;

    public Handler(Handler next) {
        this.next = next;
    }

    public Handler getNext() {
        return next;
    }

    public void setNext(Handler next) {
        this.next = next;
    }

    abstract boolean process(Request request);
}

/**
 * 访问频率控制节点
 */
class RequestFrequentHandler extends Handler {

    public RequestFrequentHandler(Handler next) {
        super(next);
    }

    @Override
    boolean process(Request request) {
        System.out.println("进行访问频率控制");
        if (request.isFrequentOk()) {
            Handler next = getNext();
            if (next == null) {
                return true;
            }
            if (!next.process(request)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}

/**登录判断节点*/
class LoggingHandler extends Handler{

    public LoggingHandler(Handler next) {
        super(next);
    }

    @Override
    boolean process(Request request) {
        System.out.println("进行登录验证控制");
        if (request.isLoggedOn()) {
            Handler next = getNext();
            if (next == null) {
                return true;
            }
            if (!next.process(request)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}