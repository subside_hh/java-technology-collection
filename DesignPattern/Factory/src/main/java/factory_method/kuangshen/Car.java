package factory_method.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:15
 * @description: 汽车抽象类
 * @since 1.8
 */
public interface Car {
    void name();
}
