package factory_method.kuangshen;

import simple_factory.kuangshen.CarFactory;

/**
 * @author ThreePure
 * @date 2021/10/30 16:19
 * @description: 消费者类
 * 工厂方法模式太过繁琐，可以发现其需要创建不同的类，在数量上特别多。
 * @since 1.8
 */
public class Consumer {
    public static void main(String[] args) {
        //1、常规操作(必须知道接口以及实现类)
        Wuling wuling = new Wuling();
        Tesila tesila = new Tesila();
        wuling.name();
        tesila.name();
        System.out.println("====================");

        //2、使用工厂进行获取对象
        Car wuling_factory = new WulingFactory().getCar();
        Car tesila_factory = new TesilaFactory().getCar();
        wuling_factory.name();
        tesila_factory.name();
        //添加新的车后并没有改变原来的代码
        Car byd_factory = new BydFactory().getCar();
        byd_factory.name();
    }
}
