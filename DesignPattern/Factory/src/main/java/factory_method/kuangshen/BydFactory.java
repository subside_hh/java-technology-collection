package factory_method.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:48
 * @description: 比亚迪工厂
 * @since 1.8
 */
public class BydFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new Byd();
    }
}
