package factory_method.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:40
 * @description: 实验工厂方法模式的接口
 * @since 1.8
 */
public interface CarFactory {
    Car getCar();
}
