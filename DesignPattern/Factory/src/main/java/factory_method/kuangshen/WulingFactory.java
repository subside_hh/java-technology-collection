package factory_method.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:42
 * @description: 五菱工厂
 * @since 1.8
 */
public class WulingFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new Wuling();
    }
}
