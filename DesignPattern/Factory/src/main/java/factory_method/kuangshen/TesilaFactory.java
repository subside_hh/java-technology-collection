package factory_method.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:43
 * @description: 特斯拉工厂
 * @since 1.8
 */
public class TesilaFactory implements CarFactory{
    @Override
    public Car getCar() {
        return new Tesila();
    }
}
