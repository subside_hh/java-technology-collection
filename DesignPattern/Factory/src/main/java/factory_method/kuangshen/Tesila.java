package factory_method.kuangshen;


/**
 * @author ThreePure
 * @date 2021/10/30 16:18
 * @description: 特斯拉类
 * @since 1.8
 */
public class Tesila  implements Car {
    @Override
    public void name() {
        System.out.println("刹不住呀！");
    }
}
