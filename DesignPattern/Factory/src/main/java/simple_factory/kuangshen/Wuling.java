package simple_factory.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:17
 * @description: 五菱类
 * @since 1.8
 */
public class Wuling implements Car{
    @Override
    public void name() {
        System.out.println("人民需要说明，五菱就造什么！");
    }
}
