package simple_factory.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:21
 * @description: 车工厂类
 * @since 1.8
 * 问题：方式一和方式二都是静态工厂模式也叫简单工厂模式，弊端是：如果要增加一个产品，无法不修改代码来实现。
 * 解决办法：通过工厂方法模式来创建
 * 当然即便是存在这样的弊端，但是很多时候还是使用简单工厂模式来实现。
 */
public class CarFactory {

    /**
     * @description: 方法一：   ===》产生一个问题，如果还有其他的类型（大众汽车）需要被添加进来，那么这个CarFactory必需被改变，不满足OOP的七大原则的开闭原则
     * @Param: [car]
     * @Return: simple_factory.kuangshen.Car
     */
    public static Car getCar(String car){
        if ("五菱".equals(car)){
            return new Wuling();
        }else if ("特斯拉".equals(car)){
            return new Tesila();
        }else {
            return null;
        }
    }

    /**
     * @description:  方式二：可以解决方式一的问题，但是不够全面
     * @Return: simple_factory.kuangshen.Car
     */
    public static Car getWuling(){
        return new Wuling();
    }
    public static Car getTesila(){
        return new Tesila();
    }

}
