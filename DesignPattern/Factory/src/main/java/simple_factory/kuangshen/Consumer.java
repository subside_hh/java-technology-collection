package simple_factory.kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/30 16:19
 * @description: 消费者类
 * @since 1.8
 */
public class Consumer {
    public static void main(String[] args) {
        //1、常规操作(必须知道接口以及实现类)
        Wuling wuling = new Wuling();
        Tesila tesila = new Tesila();
        wuling.name();
        tesila.name();

        //2、使用工厂进行获取对象
        Car wuling_factory = CarFactory.getCar("五菱");
        Car tesila_factory = CarFactory.getCar("特斯拉");
        wuling_factory.name();
        tesila_factory.name();
    }
}
