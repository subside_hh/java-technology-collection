package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 11:39
 * @description: 植物大战僵尸中分为普通僵尸和棋手僵尸。棋手僵尸拥有平台僵尸的属性外加多携带了一面旗帜
 * @since 1.8
 */
public class ZombieTest {
    public static void main(String[] args) {
        AbstractZombie normalZombie = new NormalZombie();
        AbstractZombie flagZombie = new FlagZombie();

        normalZombie.display();
        normalZombie.move();
        normalZombie.attack();

        System.out.println("____________________");
        flagZombie.display();
        flagZombie.move();
        flagZombie.attack();

    }
}

/**定义僵尸抽象类——公用属性方法*/
abstract class AbstractZombie{
    public abstract void display();

    public void attack(){
        System.out.println("咬");
    }

    public void move(){
        System.out.println("向前移动");
    }
}

/**定义一个普通僵尸类，实现抽象僵尸类*/
class NormalZombie extends AbstractZombie{
    @Override
    public void display() {
        System.out.println("我是普通僵尸");
    }
}

/**定义一个棋手僵尸类，实现抽象僵尸类*/
class FlagZombie extends AbstractZombie{
    @Override
    public void display() {
        System.out.println("我是棋手僵尸");
    }
}

/**业务需求增肌一个大头僵尸：定义一个大头僵尸类，实现抽象僵尸类*/
class BigHeadZombie extends AbstractZombie{
    @Override
    public void display() {
        System.out.println("我是大头僵尸");
    }

    /*这个大头僵尸攻击方式不再是咬，而是用头撞，所以必须重写父类方法*/
    @Override
    public void attack() {
        System.out.println("用头撞");
    }
}

/**增加其他类型的僵尸【通过继承显得很繁琐，结构混乱】*/
class XxxZombie extends BigHeadZombie{
    @Override
    public void move() {
        System.out.println("Xxx");
    }
}




