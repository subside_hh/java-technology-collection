package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 11:52
 * @description: 策略模式测试
 * @since 1.8
 */
public class StrategyTest {
    public static void main(String[] args) {
        System.out.println("______普通僵尸______");
        Zombie normalZombie = new NormalZombie1();
        normalZombie.display();
        normalZombie.move();
        normalZombie.attack();

        //改变普通僵尸的攻击方式【根据传入不同的参数来实现不同的功能】
        System.out.println("================>");
        normalZombie.setAttackable(new HitAttack());
        normalZombie.attack();
    }
}


/**定义移动和攻击的接口*/
interface Moveable{
    void move();
}
interface Attackable{
    void attack();
}

/**移动方式为：一步一步*/
class StepByStepMove implements Moveable{

    @Override
    public void move() {
        System.out.println("一步一步移动");
    }
}

/**攻击方式：咬*/
class BiteAttack implements Attackable{
    @Override
    public void attack() {
        System.out.println("咬");
    }
}

/**攻击方式：咬*/
class HitAttack implements Attackable{
    @Override
    public void attack() {
        System.out.println("敲打");
    }
}

/**定义一个抽象的僵尸类*/
abstract class Zombie{
    Moveable moveable;
    Attackable attackable;

    /**
     * @description:  同各国有参构造方法让僵尸由不同的属性
     * @Param: [moveable, attackable]
     * @Return:
     */
    public Zombie(Moveable moveable, Attackable attackable) {
        this.moveable = moveable;
        this.attackable = attackable;
    }

    abstract public void display();
    abstract void move();
    abstract void attack();

    public Moveable getMoveable() {
        return moveable;
    }

    public void setMoveable(Moveable moveable) {
        this.moveable = moveable;
    }

    public Attackable getAttackable() {
        return attackable;
    }

    public void setAttackable(Attackable attackable) {
        this.attackable = attackable;
    }
}

/**具体的将是类【普通僵尸】*/
class NormalZombie1 extends Zombie{
    public NormalZombie1() {
        super(new StepByStepMove(),new BiteAttack());
    }

    public NormalZombie1(Moveable moveable, Attackable attackable) {
        super(moveable, attackable);
    }

    @Override
    public void display() {
        System.out.println("我是普通僵尸");
    }

    @Override
    void move() {
        moveable.move();
    }

    @Override
    void attack() {
        attackable.attack();
    }
}


/**具体的将是类【棋手僵尸】*/
class FlagZombie1 extends Zombie{
    public FlagZombie1() {
        super(new StepByStepMove(),new BiteAttack());
    }

    public FlagZombie1(Moveable moveable, Attackable attackable) {
        super(moveable, attackable);
    }

    @Override
    public void display() {
        System.out.println("我是棋手僵尸");
    }

    @Override
    void move() {
        moveable.move();
    }

    @Override
    void attack() {
        attackable.attack();
    }
}