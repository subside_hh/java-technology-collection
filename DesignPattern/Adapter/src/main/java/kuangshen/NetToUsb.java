package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:16
 * @description: 转接器的接口
 * @since 1.8
 */
public interface NetToUsb {
    public void handleRequest();
}
