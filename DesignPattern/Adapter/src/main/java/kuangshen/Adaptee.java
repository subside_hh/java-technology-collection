package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:12
 * @description: 要被适配的类   （模拟网线）
 * 模拟现在的笔记本电脑通过转换器实现用网线上网
 * @since 1.8
 */
public class Adaptee {
    public void requestNetwork(){
        System.out.println("连接网线上网");
    }
}
