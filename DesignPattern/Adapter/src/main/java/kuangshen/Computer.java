package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:14
 * @description: 客户端类：（现在的笔记本电脑）
 * 模拟现在的笔记本电脑通过转换器实现用网线上网
 * @since 1.8
 */
public class Computer {
    /*public void net(NetAdapter adapter){
        //上网的具体实现，本身没有网线接口
        adapter.handleRequest();
    }*/

    public void net(NetAdapter2 adapter){
        //上网的具体实现，本身没有网线接口
        adapter.handleRequest();
    }

    public static void main(String[] args) {
        /*//电脑
        Computer computer = new Computer();
        //网线
        Adaptee adaptee = new Adaptee();
        //转接器
        NetAdapter netAdapter = new NetAdapter();
        //电脑可以上网了【并没有使用到adaptee对象】
        computer.net(netAdapter);*/

        //电脑
        Computer computer = new Computer();
        //网线
        Adaptee adaptee = new Adaptee();
        //转接器
        NetAdapter2 netAdapter2 = new NetAdapter2(adaptee);
        //电脑可以上网了【使用到adaptee对象,】
        computer.net(netAdapter2);
    }
}
