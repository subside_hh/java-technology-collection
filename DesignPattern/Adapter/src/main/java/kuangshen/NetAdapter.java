package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:19
 * @description: 适配器【让两个不相关的类产生关联】   ===使用继承方式
 * 适配器实现方式：
 *      1、继承（类适配器：java单继承，存在局限性）
 *      2、组成（对象适配器：常用）
 * @since 1.8
 */
public class NetAdapter extends Adaptee implements NetToUsb {
    @Override
    public void handleRequest() {
        //使用父类的方法实现上网
        super.requestNetwork();
    }
}
