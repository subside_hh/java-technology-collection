package tuling;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ThreePure
 * @date 2021/11/1 10:05
 * @description: 享元模式
 * @since 1.8
 */
public class Flyweight {
    public static void main(String[] args) {
        //通过工厂实现：工厂中有这棵树就直接获得[存在相同的那么就可以直接返回]，如果没有那么工厂会创建这棵这样的树并且返回
        TreeNode treeNode1 = new TreeNode(3, 4, TreeFactory.getTree("天下第一树", "黄山迎客松"));
        TreeNode treeNode2 = new TreeNode(5, 4, TreeFactory.getTree("天下第一树", "黄山迎客松"));

        TreeNode treeNode3 = new TreeNode(6, 3, TreeFactory.getTree("天下第二树", "江西是个好地方"));
        TreeNode treeNode4 = new TreeNode(12, 2, TreeFactory.getTree("天下第二树", "江西是个好地方"));

        /*
        * 只打印两次
        * name:天下第一树tree has created!   【创建一次】
        * name:天下第二树tree has created!   【创建一次】
        * */
    }
}

/**
 * 生成树的一个工厂
 */
class TreeFactory{
    //ConcurrentHashMap:Creates a new, empty map with the default initial table size。 可以创建一个线程安全的map
    private static Map<String, Tree> map =new ConcurrentHashMap<>();

    /**
     * @description:  实现创建一棵树
     * @Param: [name, data]
     * @Return: tuling.Tree
     */
    public static Tree getTree(String name,String data){
        if (map.containsKey(name)){
            //如果已经存在一棵同名的树（相同的树），那么直接返回出去这个树
            return map.get(name);
        }
        //如果这个工厂中还没有这样的一棵树，那么创建一棵这样的树，并且保存起来以及返回出去
        Tree tree = new Tree(name, data);
        map.put(name, tree);
        return tree;
    }
}

/**
 * 树类 通过构造方法创建指定name和data的树对象
 */
class Tree{
    private final String name;
    private final String data;

    public Tree(String name, String data) {
        //被创建的时候打印依据日志
        System.out.println("name:"+name+"tree has created!");
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }
}

/**
 * 森林类   在坐标轴上有一棵树。x，y，tree
 */
class TreeNode{
    private int x;
    private int y;
    private Tree tree;

    public TreeNode(int x, int y, Tree tree) {
        this.x = x;
        this.y = y;
        this.tree = tree;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }
}
