package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 11:12
 * @description: 装饰器模式测试
 * @since 1.8
 */
public class DecoratorTest {
    public static void main(String[] args) {
        //创建一份原始的类（需要后期由装饰器添加其他功能，比如这里指手机原生相机）
        Component component1 = new ConcreteComponent();
        component1.operation();

        System.out.println("++++++++使用装饰器模式（使用一个装饰器）+++++++++");
        //通过添加装饰器增加功能
        Component componen2 = new AddFunction1(new ConcreteComponent());
        componen2.operation();

        System.out.println("++++++++使用装饰器模式（使用两个个装饰器）+++++++++");
        //通过添加装饰器增加功能
        Component componen3 =new AddFunction2(new AddFunction1(new ConcreteComponent()));
        componen3.operation();
    }
}

/**定义一个接口*/
interface Component{
    void operation();
}

/**定义一个具体的类（通过装饰器为其添加功能）比如手机原生相机*/
class ConcreteComponent implements Component{
    @Override
    public void operation() {
        System.out.println("手机相机：拍照");
    }
}

/**定义一个装饰器的抽象类，为其子类提供统一的装饰器接口*/
abstract class Decorator implements Component{
    Component component;
    public Decorator(Component component) {
        this.component = component;
    }
}

/**一个具体的装饰器类，可以添加功能（效果）*/
class AddFunction1 extends Decorator{
    public AddFunction1(Component component) {
        super(component);
    }

    @Override
    public void operation() {
        component.operation();
        System.out.println("手机美颜：添加美颜效果");
    }
}


/**一个具体的装饰器类，可以添加功能（效果）*/
class AddFunction2 extends Decorator{
    public AddFunction2(Component component) {
        super(component);
    }

    @Override
    public void operation() {
        component.operation();
        System.out.println("手机滤镜：添加滤镜效果");
    }
}


