package singleresponsibility;

/**
 * @author ThreePure
 * @date 2021/10/29 10:24
 * @description: 设计模式七大原则之单一职责原则——类级别保持单一职责原则
 * @since 1.8
 */
public class SingleResponsibility2 {
    public static void main(String[] args) {
        RoadVehicle roadVehicle = new RoadVehicle();
        //造成此处（客户端）的修改
        AirVehicle airVehicle = new AirVehicle();
        roadVehicle.run("摩托车");
        roadVehicle.run("汽车");
        airVehicle.run("飞机");
    }
}

/**
 *方式二：将类分解，一个类只负责一项职责
 *问题：改动较大，将类分解，同时需要改动客户端
 *改进：直接修改Vehicle类  =》方式三
*/
class RoadVehicle{
    public void run(String vehicle){
        System.out.println(vehicle+"在公路上运行");
    }
}

class AirVehicle{
    public void run(String vehicle){
        System.out.println(vehicle+"在天上飞");
    }
}