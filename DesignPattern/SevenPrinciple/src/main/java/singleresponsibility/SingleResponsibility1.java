package singleresponsibility;

/**
 * @author ThreePure
 * @date 2021/10/29 10:14
 * @description: 设计模式七大原则之单一职责原则
 * @since 1.8
 */

/**
 * note:
 *  对类来说的，即一个类应该只负责一项职责。如类A负责两个不同职责：职责1，职责2。当职责1需求变更而改变A时，可能造成职责2执行错误，所以需要将类A的粒度分解为A1，A2
 *  单一职责原则注意事项和细节：
 *      1）降低类的复杂度，一个类只负责一项职责
 *      2）提高类的可读性，可维护性
 *      3）降低变更引起的风险
 *      4）通常情况下，我们应当遵守单一职责原则，只有逻辑足够简单，才可以在代码级违反单一职责原则；只有类中方法数量足够少，可以在方法级别保持单一职责原则
 */

public class SingleResponsibility1 {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.run("摩托车");
        vehicle.run("游艇");
        vehicle.run("飞机");
    }
}


/**
 交通工具类，方式一
 问题：方式一的run（）方法中，违反了单一职责原则。
 解决：根据交通工具运行方式不同，分解为不同类    ==>方式二
 */
class Vehicle{
    public void run(String vehicle){
        System.out.println(vehicle+"在公路上运行");
    }
}

