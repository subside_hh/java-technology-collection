package singleresponsibility;

/**
 * @author ThreePure
 * @date 2021/10/29 10:30
 * @description: 设计模式七大原则之单一职责原则——方法级别保持单一职责原则
 * @since 1.8
 */
public class SingleResponsibility3 {
    public static void main(String[] args) {
        Vehicle2 vehicle2 = new Vehicle2();
        vehicle2.run("汽车");
        vehicle2.runAir("飞机");
        vehicle2.runWater("轮船");
    }
}

/**
 * 方式三：在方法上实现单一职责原则（适用于方法较少的类中）
 * 这种修改方法没有对原来的类做大的修改，
 * 只是增加方法这里虽然没有在类这个级别上遵守单一职责原则，但是在方法级别上，仍然是遵守单一职责
 */
class Vehicle2{
    public void run(String vehicle){
        System.out.println(vehicle+"在公路上运行……");
    }

    public void runAir(String vehicle){
        System.out.println(vehicle+"在天上运行……");
    }

    public void runWater(String vehicle){
        System.out.println(vehicle+"在水路上运行……");
    }
}

