package dependence_inversion;

/**
 * @author ThreePure
 * @date 2021/11/3 20:07
 * @description: 依赖倒转原则
 * @since 1.8
 */
public class DependenceInversionTest2 {
    public static void main(String[] args) {
        Person2 person = new Person2();
        person.receive(new Email2());
        person.receive(new WeXin());
    }
}

interface IReceiver{
    String getInfo();
}

class Email2 implements IReceiver{
    @Override
    public String getInfo(){
        return "电子邮件信息：hello! ";
    }
}

class WeXin implements IReceiver{
    @Override
    public String getInfo(){
        return "微信信息：hello! ";
    }
}

/**完成Person接收消息的功能【方式二】
 * */
class Person2{
    public void receive(IReceiver iReceiver){
        System.out.println(iReceiver.getInfo());
    }
}



