package dependence_inversion;

/**
 * @author ThreePure
 * @date 2021/11/3 20:30
 * @description: 依赖关系传递的三种方式
 * 方式一：通过接口传递实现依赖
 * 方式二：通过构造方法传递依赖
 * 方式三：通过setter方法传递
 * @since 1.8
 */
public class DependenceInversionTest3 {
    public static void main(String[] args) {
        //方式一：通过接口传递实现依赖
        MiTV miTV = new MiTV();
        OpenAndClose openAndClose = new OpenAndClose();
        openAndClose.open(miTV);

        //方式二：通过构造方法传递依赖
        TCLTV tcltv = new TCLTV();
        OpenAndClose2 openAndClose2 = new OpenAndClose2(tcltv);
        openAndClose2.open();

        //方式三：通过setter方法传递
        OpenAndClose3 openAndClose3 = new OpenAndClose3();
        HairTV hairTV = new HairTV();
        openAndClose3.setTv3(hairTV);
        openAndClose3.open();

    }
}


/*方式一：通过接口传递实现依赖*/

/**开关的接口*/
interface IOpenAndClose{
    public void open(TTV ttv);
}

/**TTV接口*/
interface TTV{
    public void play();
}

class MiTV implements TTV{
    @Override
    public void play() {
        System.out.println("Mi电视，让每个人都能享受科技带来的乐趣");
    }
}

/**实现接口*/
class OpenAndClose implements IOpenAndClose{

    @Override
    public void open(TTV ttv) {
        ttv.play();
    }
}


/*方式二：通过构造方法传递依赖*/
interface IOpenAndClose2{
    //抽象方法
    public void open();
}

interface TTV2{
    public void play();
}

class OpenAndClose2 implements IOpenAndClose2{
    public TTV2 ttv2;

    public OpenAndClose2(TTV2 ttv2) {
        this.ttv2 = ttv2;
    }

    @Override
    public void open() {
        this.ttv2.play();
    }
}

class TCLTV implements TTV2{
    @Override
    public void play() {
        System.out.println("老牌电视啦");
    }
}


/*方式三：通过setter方法传递*/

interface IOpenAndClase3{
    public void open();
    public void setTv3(TTV3 ttv3);
}

interface TTV3{
    public void play();
}

class OpenAndClose3 implements IOpenAndClase3{
    private TTV3 ttv3;

    @Override
    public void open() {
        this.ttv3.play();
    }

    @Override
    public void setTv3(TTV3 ttv3) {
        this.ttv3 = ttv3;
    }
}

class HairTV implements TTV3{
    @Override
    public void play() {
        System.out.println("服务到位");
    }
}