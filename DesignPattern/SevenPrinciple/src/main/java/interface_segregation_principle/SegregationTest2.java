package interface_segregation_principle;

/**
 * @author ThreePure
 * @date 2021/11/2 20:56
 * @description: 接口隔离原则：满足接口隔离原则
 * @since 1.8
 */
public class SegregationTest2 {
    public static void main(String[] args) {
        A2 a2 = new A2();
        //A2类通过接口依赖（使用）B2类
        a2.depend1(new B2());
        a2.depend2(new B2());
        a2.depend3(new B2());

        System.out.println("--------------------");
        C2 c2 = new C2();
        //C2类通过接口依赖（使用）D2类
        c2.depend1(new D2());
        c2.depend4(new D2());
        c2.depend5(new D2());
    }
}

interface Interface21{
    void operation1();
}

interface Interface22{
    void operation2();
    void operation3();
}

interface Interface23{
    void operation4();
    void operation5();
}

/**A类通过接口Interface11和Interface22依赖（使用）B类，但是只会用放1、2、3方法*/
class A2{
    public void depend1(Interface21 interface1){
        interface1.operation1();
    }
    public void depend2(Interface22 interface1){
        interface1.operation2();
    }
    public void depend3(Interface22 interface1){
        interface1.operation3();
    }
}

class B2 implements Interface21,Interface22{

    @Override
    public void operation1() {
        System.out.println("B类实现了operation1方法");
    }

    @Override
    public void operation2() {
        System.out.println("B类实现了operation2方法");
    }

    @Override
    public void operation3() {
        System.out.println("B类实现了operation3方法");
    }
}

/**C类通过接口Interface21和Interface23依赖（使用）B类，但是只会用放1、4、5方法*/
class C2{
    public void depend1(Interface21 interface1){
        interface1.operation1();
    }
    public void depend4(Interface23 interface1){
        interface1.operation4();
    }
    public void depend5(Interface23 interface1){
        interface1.operation5();
    }
}

class D2 implements Interface21, Interface23{

    @Override
    public void operation1() {
        System.out.println("D类实现了operation1方法");
    }

    @Override
    public void operation4() {
        System.out.println("D类实现了operation4方法");
    }

    @Override
    public void operation5() {
        System.out.println("D类实现了operation5方法");
    }
}

