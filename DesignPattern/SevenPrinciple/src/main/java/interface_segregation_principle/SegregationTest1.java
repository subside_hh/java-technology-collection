package interface_segregation_principle;

/**
 * @author ThreePure
 * @date 2021/11/2 20:44
 * @description: 接口隔离原则：不满足接口隔离原则---》SegregationTest2
 * @since 1.8
 */
public class SegregationTest1 {
    public static void main(String[] args) {

    }
}

/**定义一个接口*/
interface Interface1{
    void operation1();
    void operation2();
    void operation3();
    void operation4();
    void operation5();
}

/**A类通过接口Interface1依赖（使用）B类，但是只会用放1、2、3方法*/
class A{
    public void depend1(Interface1 interface1){
        interface1.operation1();
    }
    public void depend2(Interface1 interface1){
        interface1.operation2();
    }
    public void depend3(Interface1 interface1){
        interface1.operation3();
    }
}

class B implements Interface1{

    @Override
    public void operation1() {
        System.out.println("B类实现了operation1方法");
    }

    @Override
    public void operation2() {
        System.out.println("B类实现了operation2方法");
    }

    @Override
    public void operation3() {
        System.out.println("B类实现了operation3方法");
    }

    @Override
    public void operation4() {
        System.out.println("B类实现了operation4方法");
    }

    @Override
    public void operation5() {
        System.out.println("B类实现了operation5方法");
    }
}

/**C类通过接口Interface1依赖（使用）B类，但是只会用放1、4、5方法*/
class c{
    public void depend1(Interface1 interface1){
        interface1.operation1();
    }
    public void depend4(Interface1 interface1){
        interface1.operation4();
    }
    public void depend5(Interface1 interface1){
        interface1.operation5();
    }
}

class D implements Interface1{

    @Override
    public void operation1() {
        System.out.println("D类实现了operation1方法");
    }

    @Override
    public void operation2() {
        System.out.println("D类实现了operation2方法");
    }

    @Override
    public void operation3() {
        System.out.println("D类实现了operation3方法");
    }

    @Override
    public void operation4() {
        System.out.println("D类实现了operation4方法");
    }

    @Override
    public void operation5() {
        System.out.println("D类实现了operation5方法");
    }
}
