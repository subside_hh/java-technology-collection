package com.bytedance;

import com.bytedance.matter.Matter;

/**
 * @date: 2024-05-11 16:49
 * @author: hjb
 * @description: 定义装修包接⼝
 */
public interface IMenu {

    IMenu appendCeiling(Matter matter); // 吊顶

    IMenu appendCoat(Matter matter); // 涂料
    
    IMenu appendFloor(Matter matter); // 地板
    
    IMenu appendTile(Matter matter); // 地砖
    
    String getDetail(); // 明细
}
