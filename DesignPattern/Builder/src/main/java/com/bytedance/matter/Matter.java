package com.bytedance.matter;

import java.math.BigDecimal;

/**
 * @date: 2024-05-11 16:50
 * @author: hjb
 * @description: 物料接口
 */
public interface Matter {
    
    String scene(); // 场景；地板、地砖、涂料、吊顶
    
    String brand(); // 品牌
    
    String model(); // 型号
    
    BigDecimal price(); // 价格
    
    String desc(); // 描述
    
}
