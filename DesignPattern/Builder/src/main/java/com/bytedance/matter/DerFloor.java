package com.bytedance.matter;

import java.math.BigDecimal;

/**
 * @date: 2024-05-11 17:02
 * @author: hjb
 * @description: 德尔地板
 */
public class DerFloor implements Matter{

    public String scene() {
        return "地板";
    }
    public String brand() {
        return "德尔(Der)";
    }
    public String model() {
        return "A+";
    }
    public BigDecimal price() {
        return new BigDecimal(119);
    }
    public String desc() {
        return "DER德尔集团是全球领先的专业⽊地板制造商，北京2008年奥运会家装和公装 地板供应商";
    }
    
}
