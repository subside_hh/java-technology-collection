package com.bytedance;

import com.bytedance.matter.*;

import java.math.BigDecimal;

/**
 * @date: 2024-05-11 16:56
 * @author: hjb
 * @description: 建造者⽅法
 */
public class Builder {

    public IMenu levelOne(Double area) {
        return new DecorationPackageMenu(new BigDecimal(area), "豪华欧式")
                .appendCeiling(new LevelTwoCeiling()) // 吊顶，⼆级顶
                .appendCoat(new DuluxCoat()) // 涂料，多乐⼠
                .appendFloor(new ShengXiangFloor()); // 地板，圣象
    }
    public IMenu levelTwo(Double area){
        return new DecorationPackageMenu(new BigDecimal(area), "轻奢⽥园")
                .appendCeiling(new LevelTwoCeiling()) // 吊顶，⼆级顶
                .appendCoat(new LiBangCoat()) // 涂料，⽴邦
                .appendTile(new MarcoPoloTile()); // 地砖，马可波罗
    }
    public IMenu levelThree(Double area){
        return new DecorationPackageMenu(new BigDecimal(area), "现代简约")
                .appendCeiling(new LevelOneCeiling()) // 吊顶，⼆级顶
                .appendCoat(new LiBangCoat()) // 涂料，⽴邦
                .appendFloor(new DerFloor())  //地板 德尔
                .appendTile(new DongPengTile()); // 地砖，东鹏
    }
    
}
