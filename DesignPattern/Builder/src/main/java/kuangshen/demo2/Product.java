package kuangshen.demo2;

/**
 * @author ThreePure
 * @date 2021/10/30 19:30
 * @description: 套餐   产品有初始化的属性值
 * @since 1.8
 */
public class Product {
    private String Food1 = "汉堡";
    private String Food2 = "可乐";
    private String Food3 = "薯条";
    private String Food4 = "甜点";

    public String getFood1() {
        return Food1;
    }

    public void setFood1(String food1) {
        Food1 = food1;
    }

    public String getFood2() {
        return Food2;
    }

    public void setFood2(String food2) {
        Food2 = food2;
    }

    public String getFood3() {
        return Food3;
    }

    public void setFood3(String food3) {
        Food3 = food3;
    }

    public String getFood4() {
        return Food4;
    }

    public void setFood4(String food4) {
        Food4 = food4;
    }

    @Override
    public String toString() {
        return "Product{" +
                "Food1='" + Food1 + '\'' +
                ", Food2='" + Food2 + '\'' +
                ", Food3='" + Food3 + '\'' +
                ", Food4='" + Food4 + '\'' +
                '}';
    }
}
