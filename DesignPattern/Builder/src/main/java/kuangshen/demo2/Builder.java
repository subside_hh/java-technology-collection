package kuangshen.demo2;

/**
 * @author ThreePure
 * @date 2021/10/30 19:28
 * @description: 与demo1的区别是在建造者的方法中需要传递不同的参数来实现产品的差异化
 * @since 1.8
 */
public abstract class Builder {
    /**汉堡*/
    abstract Builder buildA(String msg);
    /**可乐*/
    abstract Builder buildB(String msg);
    /**薯条*/
    abstract Builder buildC(String msg);
    /**甜点*/
    abstract Builder buildD(String msg);

    /**获得方法*/
    abstract Product getProduct();
}
