package kuangshen.demo2;

/**
 * @author ThreePure
 * @date 2021/10/30 19:33
 * @description: 具体建造者    ——建造具体的产品，产品的属性由外部传入。
 * @since 1.8
 */
public class Worker extends Builder{

    private Product product;

    public Worker() {
        product = new Product();
    }

    @Override
    Builder buildA(String msg) {
        product.setFood1(msg);
        return this;
    }

    @Override
    Builder buildB(String msg) {
        product.setFood2(msg);
        return this;
    }

    @Override
    Builder buildC(String msg) {
        product.setFood3(msg);
        return this;
    }

    @Override
    Builder buildD(String msg) {
        product.setFood4(msg);
        return this;
    }

    @Override
    Product getProduct() {
        return product;
    }
}
