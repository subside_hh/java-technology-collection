package kuangshen.demo2;

/**
 * @author ThreePure
 * @date 2021/10/30 19:39
 * @description:
 * @since 1.8
 */
public class Test {
    public static void main(String[] args) {
        //服务员
        Worker worker = new Worker();
        //链式编程  在原来的基础上可以自由组合，如果不自由组合，也有默认的值  --》根据传入不同的参数，实现不同的产品
        Product product = worker
                .buildA("全家桶")
                .buildB("雪碧")
                .getProduct();
        System.out.println(product.toString());
    }
}
