package kuangshen.demo1;

/**
 * @author ThreePure
 * @date 2021/10/30 19:19
 * @description: TODO
 * @since 1.8
 */
public class Test {
    public static void main(String[] args) {
        //指挥
        Director director = new Director();
        //指挥具体的工人完成建造【生产产品需要通过具体的建造者，即工人，传入不同的工人可以实现生产不同的产品】
        Product product = director.build(new Worker());
        System.out.println(product.toString());

        System.out.println("++++++++++++sgg++++++++++++");
        Director director1 = new Director(new Worker());
        Product product1 = director1.buildingProduct();
        System.out.println(product1.toString());
        System.out.println("product1.getBuildB():"+product1.getBuildB());

    }
}
