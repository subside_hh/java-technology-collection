package kuangshen.demo1;

/**
 * @author ThreePure
 * @date 2021/10/30 19:06
 * @description: 具体的建造者--工人   ——只是建造具体的产品，产品的属性由建造者自己去顶。
 * @since 1.8
 */
public class Worker extends Builder{
    private Product product;

    public Worker() {
        product = new Product();
    }

    @Override
    void buildA() {
        product.setBuildA("地基");
        System.out.println("地基");
    }

    @Override
    void buildB() {
        product.setBuildB("框架");
        System.out.println("框架");
    }

    @Override
    void buildC() {
        product.setBuildC("墙体");
        System.out.println("墙体");
    }

    @Override
    void buildD() {
        product.setBuildD("粉刷");
        System.out.println("粉刷");
    }

    @Override
    Product getProduct() {
        return product;
    }
}
