package kuangshen.demo1;

/**
 * @author ThreePure
 * @date 2021/10/30 19:16
 * @description: 指挥，核心。负责构建一个工程，制作流程的控制
 * @since 1.8
 */
public class Director {

    Builder builder = null;

    public Director() {
    }


    /**
     * @description:  方式一：通过构造器传入一个具体的builder
     * @Param: [builder]
     */
    public Director(Builder builder) {
        this.builder = builder;
    }

    /**
     * @description:  方式二：通过setter方法传入builder
     * @Param: [builder]
     */
    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public Product buildingProduct(){
        builder.buildA();
        builder.buildD();
        builder.buildC();
        builder.buildB();
        return builder.getProduct();
    }

    /*以上代码为尚硅谷学习。 以下代码为Kuangshen指导。原理相同，只是传递方式不同而已*/

    /**
     * @description:  指挥工人按照顺序建造房子
     * @Param: [builder]
     * @Return: kuangshen.demo1.Product
     */
    public Product build(Builder builder){
        builder.buildA();
        builder.buildB();
        builder.buildC();
        builder.buildD();
        return builder.getProduct();
    }
}
