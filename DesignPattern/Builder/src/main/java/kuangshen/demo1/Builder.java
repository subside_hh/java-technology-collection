package kuangshen.demo1;

/**
 * @author ThreePure
 * @date 2021/10/30 19:01
 * @description: 抽象的建造者 --方法   不需要传递产品的具体参数。只能建造具体的产品
 * @since 1.8
 */
public abstract class Builder {
    /**4个步骤*/
    abstract void buildA();
    abstract void buildB();
    abstract void buildC();
    abstract void buildD();

    /**得到产品*/
    abstract Product getProduct();
}
