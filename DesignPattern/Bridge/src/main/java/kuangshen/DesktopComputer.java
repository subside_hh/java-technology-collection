package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:52
 * @description: 桌面电脑类
 * @since 1.8
 */
public class DesktopComputer extends Computer{
    public DesktopComputer(Brand brand) {
        super(brand);
    }

    @Override
    public void info() {
        super.info();
        System.out.print("台式机");
    }
}
