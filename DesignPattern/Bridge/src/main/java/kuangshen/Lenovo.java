package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:46
 * @description: 具体品牌类（联想）
 * @since 1.8
 */
public class Lenovo implements Brand{
    @Override
    public void info() {
        System.out.print("联想");
    }
}
