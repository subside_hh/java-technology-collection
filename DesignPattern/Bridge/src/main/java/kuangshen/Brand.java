package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:46
 * @description: 品牌
 * @since 1.8
 */
public interface Brand {
    void info();
}
