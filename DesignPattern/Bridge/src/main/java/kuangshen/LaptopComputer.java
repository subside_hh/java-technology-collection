package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:54
 * @description: 笔记本
 * @since 1.8
 */
public class LaptopComputer extends Computer{
    public LaptopComputer(Brand brand) {
        super(brand);
    }

    @Override
    public void info() {
        super.info();
        System.out.print("笔记本");
    }
}
