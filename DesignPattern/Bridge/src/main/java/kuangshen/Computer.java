package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:50
 * @description: 抽象的电脑类型类
 * @since 1.8
 */
public abstract class Computer {
    /**组合，品牌*/
    protected Brand brand;

    public Computer(Brand brand) {
        this.brand = brand;
    }

    public void info(){
        //自带品牌
        brand.info();
    }
}
