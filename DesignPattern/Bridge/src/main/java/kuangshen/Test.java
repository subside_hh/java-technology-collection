package kuangshen;

/**
 * @author ThreePure
 * @date 2021/10/31 16:57
 * @description: 桥接模式
 * @since 1.8
 */
public class Test {
    public static void main(String[] args) {
        //苹果笔记本
        Computer computer1 = new LaptopComputer(new Apple());
        computer1.info();
        System.out.println("");
        //联想台式机
        Computer computer2 = new DesktopComputer(new Lenovo());
        computer2.info();

    }
}
