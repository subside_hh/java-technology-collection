package com.th.singleton;

/**
 * @description: 线程安全的双重检查锁单例模式
 * @author: HuangJiBin
 * @date: 2022/8/11  17:56
 * @since: 1.8
 */
public final class ThreadSafeDoubleCheckLockingSingleton {
    private static volatile ThreadSafeDoubleCheckLockingSingleton instance;

    private ThreadSafeDoubleCheckLockingSingleton(){
        if (instance != null){
            throw new IllegalStateException("已经初始化");
        }
    }

    /**
     * @description: 获取单实例
     * @Return: com.th.singleton.ThreadSafeDoubleCheckLockingSingleton
     */
    public static ThreadSafeDoubleCheckLockingSingleton getInstance(){
        ThreadSafeDoubleCheckLockingSingleton result = instance;
        if (result ==null){
            synchronized (ThreadSafeDoubleCheckLockingSingleton.class){
                result = instance;
                if (result == null){
                    instance = result = new ThreadSafeDoubleCheckLockingSingleton();
                }
            }
        }
        return result;
    }


}
