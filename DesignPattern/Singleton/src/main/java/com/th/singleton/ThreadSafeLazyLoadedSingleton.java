package com.th.singleton;

/**
 * @description: 延迟初始化的单例
 * @author: HuangJiBin
 * @date: 2022/8/11  17:48
 * @since: 1.8
 */
public final class ThreadSafeLazyLoadedSingleton {
    private static volatile ThreadSafeLazyLoadedSingleton instance;

    private ThreadSafeLazyLoadedSingleton(){
        if (instance !=null){
            throw new IllegalStateException("已经初始化");
        }
    }

    /**
     * @description: 获取单实例 直到第一次调用该方法，才创建实例。
     * @Return: com.th.singleton.ThreadSafeLazyLoadedSingleton
     */
    public static synchronized ThreadSafeLazyLoadedSingleton getInstance(){
        if (instance == null){
            synchronized (ThreadSafeLazyLoadedSingleton.class){
                if (instance == null){
                    instance = new ThreadSafeLazyLoadedSingleton();
                }
            }
        }
        return instance;
    }
}
