package com.th.singleton;

/**
 * @description: 用户枚举单例
 * @author: HuangJiBin
 * @date: 2022/8/12  9:05
 * @since: 1.8
 */
public enum EnumUserSingleton {
    /**单例*/
    USER("张三",20220812,"zhangsan@qq.com");


    private final String name;
    private final Integer userCode;
    private final String email;

    EnumUserSingleton(String name, Integer userCode, String email) {
        this.name = name;
        this.userCode = userCode;
        this.email = email;
    }

    @Override
    public String toString() {
        return "EnumUserSingleton{" +
                "name='" + name + '\'' +
                ", userCode=" + userCode +
                ", email='" + email + '\'' +
                '}';
    }
}
