package com.th.singleton;

/**
 * @description: 枚举单例
 * @author: HuangJiBin
 * @date: 2022/8/11  23:47
 * @since: 1.8
 */
public enum EnumSingleton {
    /**单例*/
    INSTANCE;

    /**toString方法*/
    @Override
    public String toString() {
        return getDeclaringClass().getCanonicalName() + "@" + hashCode();
    }
}
