package com.th.singleton;

/**
 * @description: 单例模式 【存在多线程安全问题嘛】
 * @author: HuangJiBin
 * @date: 2022/8/11  16:53
 * @since: 1.8
 * 单例模式实现最基本就是要构造私有，对外暴露唯一一个获取该类对象的唯一接口,而且该接口必须为静态的，因为无法使用new创建对象，而在该类内部保证实例对象的唯一性
 */

public class SingletonOne {
    private static SingletonOne singletonOne = null;

    private SingletonOne() {
    }

    /**
     * @description: 获取SingletonOne单例对象
     * @Return: com.th.singleton.SingletonOne
     * @apiNote: 这种单例模式在多线程中存在问题，在单例判断时，可能有多个线程同时进行，造成产生多个单例，这就不符合要求；
     */
    public SingletonOne getSingletonOneInstance(){

        if (SingletonOne.singletonOne == null){
            singletonOne = new SingletonOne();
        }
        return singletonOne;
    }

}
