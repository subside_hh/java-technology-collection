package com.th.singleton;

/**
 * @description: 初始化随需应变的持有者习惯用法
 * @author: HuangJiBin
 * @date: 2022/8/11  23:39
 * @since: 1.8
 */
public final class InitializingOnDemandHolderIdiomSingleton {

    private InitializingOnDemandHolderIdiomSingleton(){
    }

    /**
     * @description: 使用内部类方式创建单例模式
     * @Return: com.th.singleton.InitializingOnDemandHolderIdiomSingleton
     */
    public static InitializingOnDemandHolderIdiomSingleton getInstance(){
        return HelperHolder.INSTANCE;
    }

    /**提供懒加载的Singleton实例*/
    private static class HelperHolder {
        private static final InitializingOnDemandHolderIdiomSingleton INSTANCE = new InitializingOnDemandHolderIdiomSingleton();
    }

}
