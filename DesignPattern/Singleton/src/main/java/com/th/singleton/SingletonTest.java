package com.th.singleton;

/**
 * @description: 单例测试
 * @author: HuangJiBin
 * @date: 2022/8/11  23:48
 * @since: 1.8
 */
public class SingletonTest {
    public static void main(String[] args) {

        EnumSingleton instance1 = EnumSingleton.INSTANCE;
        EnumSingleton instance2 = EnumSingleton.INSTANCE;
        System.out.println("instance1:" + instance1);
        System.out.println("instance2:" + instance2);
        System.out.println("==================================");

        InitializingOnDemandHolderIdiomSingleton instance3 = InitializingOnDemandHolderIdiomSingleton.getInstance();
        InitializingOnDemandHolderIdiomSingleton instance4 = InitializingOnDemandHolderIdiomSingleton.getInstance();
        System.out.println("instance3:" + instance3.toString());
        System.out.println("instance4:" + instance4.toString());
        System.out.println("==================================");

        ThreadSafeDoubleCheckLockingSingleton instance5 = ThreadSafeDoubleCheckLockingSingleton.getInstance();
        ThreadSafeDoubleCheckLockingSingleton instance6 = ThreadSafeDoubleCheckLockingSingleton.getInstance();
        System.out.println("instance5:" + instance5.toString());
        System.out.println("instance6:" + instance6.toString());
        System.out.println("==================================");

        ThreadSafeLazyLoadedSingleton instance7 = ThreadSafeLazyLoadedSingleton.getInstance();
        ThreadSafeLazyLoadedSingleton instance8 = ThreadSafeLazyLoadedSingleton.getInstance();
        System.out.println("instance7:" + instance7.toString());
        System.out.println("instance8:" + instance8.toString());
        System.out.println("==================================");

        EnumUserSingleton user1 = EnumUserSingleton.USER;
        EnumUserSingleton user2 = EnumUserSingleton.USER;
        System.out.println("user1:" + user1);
        System.out.println("user2:" + user2);
        System.out.println("==================================");

    }
}
