package com.th.singleton;

/**
 * @description: 通用单例模式 （解决多线程安全问题，加锁）
 * @author: HuangJiBin
 * @date: 2022/8/11  17:24
 * @since: 1.8
 * 构造私有，接口加锁
 */
public class SingletonTwo {
    private static final SingletonTwo SINGLETONTWO = new SingletonTwo();

    private SingletonTwo() {
    }

    public synchronized static SingletonTwo getSingletonTwoInstance(){
        return SINGLETONTWO;
    }
}
