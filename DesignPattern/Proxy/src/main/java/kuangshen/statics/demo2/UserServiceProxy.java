package kuangshen.statics.demo2;

/**
 * @author ThreePure
 * @date 2021/10/31 17:53
 * @description: 【代理】  模拟为已写好代码的应用添加对数据库操作的日志功能
 * @since 1.8
 */
public class UserServiceProxy implements UserService{
    private UserServiceImpl userService;

    public void setUserService(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public void add() {
        log("添加");
        userService.add();
    }

    @Override
    public void delete() {
        log("删除");
        userService.delete();
    }

    @Override
    public void update() {
        log("更新");
        userService.update();
    }

    @Override
    public void query() {
        log("查询");
        userService.query();
    }

    /**
     * @description:  代理附属方法，可以实现添加日志功能
     * @param: msg
     * @Return: void
     */
    public void log(String msg){
        System.out.println(msg+"了一条数据");
    }
}
