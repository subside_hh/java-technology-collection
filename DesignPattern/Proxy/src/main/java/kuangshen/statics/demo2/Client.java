package kuangshen.statics.demo2;

/**
 * @author ThreePure
 * @date 2021/10/31 17:52
 * @description: 模拟为已写好代码的应用添加对数据库操作的日志功能
 * @since 1.8
 */
public class Client {
    public static void main(String[] args) {
        //传统一般方式
        UserServiceImpl userService = new UserServiceImpl();
        userService.add();

        System.out.println("++++++++++++++++++++");
        //使用代理进行操作
        UserServiceProxy userServiceProxy = new UserServiceProxy();
        userServiceProxy.setUserService(userService);
        userServiceProxy.delete();
    }
}
