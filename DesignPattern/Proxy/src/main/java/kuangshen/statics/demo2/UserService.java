package kuangshen.statics.demo2;

/**
 * @author ThreePure
 * @date 2021/10/31 17:48
 * @description: 模拟为已写好代码的应用添加对数据库操作的日志功能
 * @since 1.8
 */
public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
