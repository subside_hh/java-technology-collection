package kuangshen.statics.demo2;

/**
 * @author ThreePure
 * @date 2021/10/31 17:49
 * @description: 模拟为已写好代码的应用添加对数据库操作的日志功能
 * 原有的数据库操作实现类【现在要对其操作前进行记录日志的功能】  为了符合面向对象的7大原则，直接在原有代码中修改是不符合开闭原则的。现添加一个代理角色
 * @since 1.8
 */
public class UserServiceImpl implements UserService{
    @Override
    public void add() {
        System.out.println("添加用户");
    }

    @Override
    public void delete() {
        System.out.println("删除用户");
    }

    @Override
    public void update() {
        System.out.println("更新用户");
    }

    @Override
    public void query() {
        System.out.println("查询用户");
    }
}
