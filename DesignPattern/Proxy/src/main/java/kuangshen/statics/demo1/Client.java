package kuangshen.statics.demo1;

/**
 * @author ThreePure
 * @date 2021/10/31 17:27
 * @description: 房客
 * @since 1.8
 */
public class Client {
    public static void main(String[] args) {
        //传统方式直接找房东租房子
        Hoster hoster = new Hoster();
        hoster.rent();
        System.out.println("+++++++++++++++++");

        //使用代理进行租房【往往中介是会有其他的附属操作，否则是无意义的】
        Procy procy = new Procy(hoster);
        procy.rent();

    }
}
