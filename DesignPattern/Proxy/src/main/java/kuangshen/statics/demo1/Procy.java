package kuangshen.statics.demo1;

/**
 * @author ThreePure
 * @date 2021/10/31 17:27
 * @description: 中介
 * @since 1.8
 */
public class Procy implements Rent {
    private Hoster hoster;

    public Procy() {
    }

    public Procy(Hoster hoster) {
        this.hoster = hoster;
    }


    @Override
    public void rent() {
        seeHouse();
        hoster.rent();
        hetong();
        charge();
    }

    /**
    中介特有的方法
    */
    public void seeHouse() {
        System.out.println("中介带你看房");
    }

    public void charge() {
        System.out.println("中介收中介费");
    }

    public void hetong() {
        System.out.println("中介签合同");
    }
}
