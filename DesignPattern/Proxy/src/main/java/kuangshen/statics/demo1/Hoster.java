package kuangshen.statics.demo1;

/**
 * @author ThreePure
 * @date 2021/10/31 17:26
 * @description: 房东
 * @since 1.8
 */
public class Hoster implements Rent{
    @Override
    public void rent() {
        System.out.println("房东要出租房子");
    }
}
