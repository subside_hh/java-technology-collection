package kuangshen.dynamics.demo2;

/**
 * @author ThreePure
 * @date 2021/11/1 8:56
 * @description: 使用动态代理
 * @since 1.8
 */
public class Client {
    public static void main(String[] args) {
        //真实角色
        UserServiceImpl userService = new UserServiceImpl();
        //代理角色【不存在】
        ProxyInvocationHandler handler = new ProxyInvocationHandler();
        //设置要动态代理的对象
        handler.setTarget(userService);
        //动态生成代理类
        UserService proxy = (UserService) handler.getProxy();
        proxy.delete();
    }
}
