package kuangshen.dynamics.demo2;

import kuangshen.dynamics.demo1.Rent;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author ThreePure
 * @date 2021/11/1 8:37
 * @description: 用于自动生成代理类怕【万能工具类】
 * Proxy:              用于生成动态代理实例
 * InvocationHandler： 调用处理程序并返回结果
 * @since 1.8
 */
public class ProxyInvocationHandler implements InvocationHandler {
    /**被代理的接口*/
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    /**
     * @description:  生成代理类
     * @Return: java.lang.Object
     */
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), target.getClass().getInterfaces(),this);
    }

    /**
     * @description:  处理代理实例，并返回结果
     * @Param: [proxy, method, args]
     * @Return: java.lang.Object
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log(method.getName());
        //动态代理的本质就是使用反射机制实现；
        Object result = method.invoke(target, args);
        return result;
    }

    public void log(String msg){
        System.out.println("执行了"+msg+"方法");
    }
}
