package kuangshen.dynamics.demo1;

/**
 * @author ThreePure
 * @date 2021/10/31 17:25
 * @description: 租房
 * @since 1.8
 */
public interface Rent {
    public void rent();
}
