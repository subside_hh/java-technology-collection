package kuangshen.dynamics.demo1;

/**
 * @author ThreePure
 * @date 2021/11/1 8:44
 * @description: 动态代理客户端
 * @since 1.8
 */
public class Client {
    public static void main(String[] args) {
        //真实角色
        Hoster hoster = new Hoster();
        //代理角色
        ProxyInvocationHandler handler = new ProxyInvocationHandler();
        //通过调用程序处理角色来处理我们要调用的接口对象
        handler.setRent(hoster);
        //proxy这里就是动态生成的代理
        Rent proxy = (Rent) handler.getProxy();
        proxy.rent();
    }
}
