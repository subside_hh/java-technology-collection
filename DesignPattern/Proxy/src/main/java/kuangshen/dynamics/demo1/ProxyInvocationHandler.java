package kuangshen.dynamics.demo1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author ThreePure
 * @date 2021/11/1 8:37
 * @description: 用于自动生成代理类
 * @since 1.8
 */
public class ProxyInvocationHandler implements InvocationHandler {
    /**被代理的接口*/
    private Rent rent;

    public void setRent(Rent rent) {
        this.rent = rent;
    }

    /**
     * @description:  生成代理类
     * @Return: java.lang.Object
     */
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), rent.getClass().getInterfaces(),this);
    }

    /**
     * @description:  处理代理实例，并返回结果
     * @Param: [proxy, method, args]
     * @Return: java.lang.Object
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        seeHouse();
        //动态代理的本质就是使用反射机制实现；
        Object result = method.invoke(rent, args);
        change();
        return result;
    }

    public void seeHouse(){
        System.out.println("中介带看房子");
    }
    public void change(){
        System.out.println("中介收中介费");
    }

}
