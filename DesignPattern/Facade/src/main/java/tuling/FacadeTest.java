package tuling;

/**
 * @author ThreePure
 * @date 2021/11/1 10:48
 * @description: 外观模式测试
 * 客户端不需要具体去实现子系统，而通过外观模式对外提供一个统一的接口，对于客户端来说，使用这个统一的接口即可。
 * @since 1.8
 */
public class FacadeTest {
    public static void main(String[] args) {
        Client1 client1 = new Client1();
        Client2 client2 = new Client2();
        client1.doSomething1();
        System.out.println("_________________");
        client2.doSomething2();
    }
}


/**
 * 定义两个客户端程序，传统的使用子系统必须分别创建各个子系统的对象，调用子系统的方法
 */
class Client1 {
    SubSystem1 subSystem1 = new SubSystem1();
    SubSystem2 subSystem2 = new SubSystem2();
    SubSystem3 subSystem3 = new SubSystem3();

    public void doSomething1() {
        subSystem1.method1();
        subSystem2.method2();
        subSystem3.method3();
    }
}

/**客户端类 使用外观模式，而不需要创建具体的子对象来实现对子系统进行使用*/
class Client2 {
    FacadeHandle facadeHandle = new FacadeHandle();
    public void doSomething2() {
        facadeHandle.doSomethingFacade();
    }
}

/**
 * 引入Facade模式来处理
 */
class FacadeHandle {
    SubSystem1 subSystem1 = new SubSystem1();
    SubSystem2 subSystem2 = new SubSystem2();
    SubSystem3 subSystem3 = new SubSystem3();

    public void doSomethingFacade() {
        subSystem1.method1();
        subSystem2.method2();
        subSystem3.method3();
    }
}

/**
 * 定义三个子系统SubSystem1，SubSystem2，SubSystem3
 */
class SubSystem1 {
    public void method1() {
        System.out.println("subsystem1.method1 is executed。");
    }
}

class SubSystem2 {
    public void method2() {
        System.out.println("subsystem2.method2 is executed。");
    }
}

class SubSystem3 {
    public void method3() {
        System.out.println("subsystem3.method3 is executed。");
    }
}