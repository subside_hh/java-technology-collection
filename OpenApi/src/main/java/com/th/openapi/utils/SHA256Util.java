/*
 * aspire-tech.com Inc.
 * Copyright (c) 2000-2023 All Rights Reserved.
 */
package com.th.openapi.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * SHA256加密工具类
 *
 * @author WuFan
 * @version $id:SHA256Util.java, v 1.0.0.0 2023/10/6 11:46:04, WuFan Exp $
 */
public class SHA256Util {

    private SHA256Util() {
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SHA256Util.class);

    private static final String SHA256 = "SHA-256";
    private static final String ZREO = "0";

    /**
     * 使用SHA-256算法生成签名
     *
     * @param encryptString 需要签名的数据
     * @return 生成的签名
     */
    public static String encrypt(String encryptString) {
        MessageDigest messageDigest;
        String encodestr = "";
        try {
            messageDigest = MessageDigest.getInstance(SHA256);
            messageDigest.update(encryptString.getBytes(StandardCharsets.UTF_8));
            encodestr = byte2Hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("SHA256Util Error NoSuchAlgorithmException" + e.getMessage(), e);
        }
        return encodestr;
    }

    /**
     * 使用SHA-256算法生成签名并进行Base64处理
     *
     * @param encryptString 需要签名的数据
     * @return 生成的签名
     */
    public static String base64Encrypt(String encryptString) {
        return Base64.getEncoder().encodeToString(encrypt(encryptString).getBytes());
    }

    /**
     * 生成最终摘要.将byte转为16进制
     *
     * @param bytes 字节
     * @return 结果
     */
    private static String byte2Hex(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        String temp;
        for (int i = 0; i < bytes.length; i++) {
            temp = Integer.toHexString(bytes[i] & 0xFF);
            if (temp.length() == 1) {
                stringBuilder.append(ZREO);
            }
            stringBuilder.append(temp);
        }
        stringBuilder.trimToSize();
        return stringBuilder.toString();
    }

    /**
     * 验证签名是否正确
     *
     * @param data         需要验证的数据
     * @param signature    原始签名
     * @param newSignature 生成的签名
     * @return 验证结果
     */
    public static boolean verifySign(String data, String signature, String newSignature) {
        String encryptData = base64Encrypt(data);
        LOGGER.info("verifySign,encryptData={}", encryptData);
        return encryptData.equals(signature) && encryptData.equals(newSignature);
    }

    public static void main(String[] args) {
        System.out.println(encrypt("JX-CG20200404180001165CG-9f60d47bcf634b3db5b15bcb51105a8dD8x3bNNG2cfK3Q29qO47Nt0uS5Hcu4Nx"));
    }

}
