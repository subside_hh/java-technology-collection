package com.th.openapi.sample;

import com.aliyun.devops20210625.models.*;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.models.RuntimeOptions;
import com.google.gson.Gson;

/**
 * @date: 2024-07-15 9:42
 * @author: hjb
 * @description: 云效平台测试案例
 */
public class YunXiaoSample {

    /**
     * <b>description</b> :
     * <p>使用AK&amp;SK初始化账号Client</p>
     * @return Client
     *
     * @throws Exception
     */
    public static com.aliyun.devops20210625.Client createClient() throws Exception {
        // 工程代码泄露可能会导致 AccessKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考。
        // 建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html。
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_ID。
                .setAccessKeyId("LTAI5t7RcMa7HRGnZQ77....")
                // 必填，请确保代码运行环境设置了环境变量 ALIBABA_CLOUD_ACCESS_KEY_SECRET。
                .setAccessKeySecret("2K5OTRoWvZf5hDj5fqz....")
                // .setSecurityToken("pt-U33bYpdQjgWoFna4EQft7ugn_248fd9db-e69c-433c-ae41-1ef5cba73e8d")
                ;
        // Endpoint 请参考 https://api.aliyun.com/product/devops
        config.endpoint = "devops.cn-hangzhou.aliyuncs.com";
        return new com.aliyun.devops20210625.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.devops20210625.Client client = YunXiaoSample.createClient();
        com.aliyun.devops20210625.models.ListProjectsRequest listProjectsRequest = new com.aliyun.devops20210625.models.ListProjectsRequest()
                .setCategory("Project");
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        java.util.Map<String, String> headers = new java.util.HashMap<>();
        try {
            // 复制代码运行请自行打印 API 的返回值
            //项目查询
            ListProjectsResponse listProjectsResponse = client.listProjectsWithOptions("6627872756bd1343facca195", listProjectsRequest, headers, runtime);
            ListProjectsResponseBody body = listProjectsResponse.getBody();
            System.out.println("项目：" + new Gson().toJson(body));
            //仓库查询
            com.aliyun.devops20210625.models.ListRepositoriesRequest listRepositoriesRequest = new com.aliyun.devops20210625.models.ListRepositoriesRequest()
                    .setOrganizationId("6627872756bd1343facca195");
            com.aliyun.teautil.models.RuntimeOptions runtime1 = new com.aliyun.teautil.models.RuntimeOptions();
            java.util.Map<String, String> headers1 = new java.util.HashMap<>();
            ListRepositoriesResponse listRepositoriesResponse = client.listRepositoriesWithOptions(listRepositoriesRequest, headers, runtime);
            ListRepositoriesResponseBody body1 = listRepositoriesResponse.getBody();
            System.out.println("仓库：" + new Gson().toJson(body1));
        } catch (TeaException error) {
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }
    
}
