package com.th.openapi.entrance.web;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @date: 2024-05-15 15:23
 * @author: hjb
 * @description: 测试控制器
 */
@RestController
public class EntranceDemoController {

    @PostMapping(value = "/api/v1/frontlineEvaluation/addCompanyEvaluation")
    public HashMap<String, Object> hello(@RequestBody JSONObject obj, HttpServletRequest request) {
        String jsonString = com.alibaba.fastjson.JSON.toJSONString(obj, SerializerFeature.WriteMapNullValue);
        System.out.println("接收到参数" + jsonString);

        String clientId = request.getHeader("X-Client-Id");
        String timeStr = request.getHeader("X-Req-Time");
        String transId = request.getHeader("X-Req-Trans-Id");
        String sign = request.getHeader("X-Sign");


        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put("status", "000000");
        resultMap.put("message", "请求成功！");
        resultMap.put("data", null);
        //将这个resultMap转换成json对象返回

        return resultMap;
    }
}
