package com.th.delay_order.task;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @description: 十五分钟未支付取消订单
 * @author: HuangJiBin
 * @date: 2022/7/16  0:01
 * @since: 1.8
 */
public class CancelOrder implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        String orderId = (String) jobDataMap.get("orderId");

        //查询订单是否未支付,
        /*OrderModel orderModel = orderService.selectOrder(orderId);
        if (orderModel.getStatus().equals(OrderEnums.NOT_PAY)){
            //修改订单状态
            UpdateWrapper wrapper = new UpdateWrapper<OrderModel>();
            wrapper.eq("number", orderId);
            wrapper.set("status", OrderEnums.CANCEL_DEAL);
            orderService.update(wrapper);
        }*/
    }
}
