package com.th.delay_order.task;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.DateBuilder.futureDate;

/**
 * @description: 创建Quartz所需要的触发器、调度器以及工作详情
 * @author: HuangJiBin
 * @date: 2022/7/16  0:06
 * @since: 1.8
 */
public class QuartzWork {

    /**
     * @description: 订单延时任务的处理
     * @Param: [orderId, userId]
     * @Return: void
     */
    public void delayTask (String orderId, Integer userId) throws SchedulerException, InterruptedException {
        // 创建Scheduler对象进行任务与Trigger的协调，并开启任务调度
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();

        //创建任务
        JobDetail jobDetail = JobBuilder.newJob(CancelOrder.class)
                .withDescription("取消超时订单")
                .withIdentity(orderId, userId.toString())//任务id a+b
                .usingJobData("orderId",orderId)
                .build();

        //创建触发器
        SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule())
                .withDescription("取消超时订单触发器")
                .withIdentity(orderId.toString()+"_Trigger", userId.toString())
                .startAt(futureDate(15, DateBuilder.IntervalUnit.SECOND))
                .build();

        //5.将触发器和任务绑定到哪里去？ 调度器
        scheduler.scheduleJob(jobDetail,trigger);

        //6.启动调度器
        scheduler.start();

        // 7.关闭调度器
        /*Thread.sleep(1000 * 60 * 15);//用线程睡眠来操作调度器运行的时间
        scheduler.shutdown();*/
    }

}
