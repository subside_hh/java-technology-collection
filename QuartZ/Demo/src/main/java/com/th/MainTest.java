package com.th;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-14
 * @Description: 主测试类
 */
public class MainTest {
    public static void main(String[] args) throws SchedulerException, InterruptedException {
        // 创建任务对象，绑定任务类并为任务命名及分组
        JobDetail jobDetail = JobBuilder.newJob()
                .ofType(MyJob.class)
                .withIdentity("job1", "group1")
                .build();

        // 创建触发器对象，简单触发器每隔 3 秒执行一次任务
        SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger1", "group1")
                .withSchedule(SimpleScheduleBuilder
                        .simpleSchedule()
                        .withIntervalInSeconds(3)
                        .repeatForever())
                .build();

        // 创建调度器
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        // 绑定任务和触发器
        scheduler.scheduleJob(jobDetail, trigger);

        // 启动调度器
        scheduler.start();

        // 注意：Quartz会开启子线程，而为了防止主线程结束，我们为主线程设置下休眠
        Thread.sleep(60000);

    }
}
