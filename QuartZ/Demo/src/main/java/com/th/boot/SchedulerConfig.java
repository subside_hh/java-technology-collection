package com.th.boot;

import com.th.MyJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-14
 * @Description: TODO
 */
@Configuration
public class SchedulerConfig {

    // 任务bean
    @Bean
    public JobDetail simpleJobBean() {
        return JobBuilder.newJob(MyJob.class)
                .withIdentity("job1", "group1")
                // Jobs added with no trigger must be durable.
                .storeDurably()
                .build();
    }

    // 触发器bean
    @Bean
    public Trigger simpleTrigger() {
        return TriggerBuilder.newTrigger()
                .withIdentity("trigger1", "group1")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(3)
                        .repeatForever())
                // 指定具体任务
                .forJob(simpleJobBean())
                .build();
    }

}