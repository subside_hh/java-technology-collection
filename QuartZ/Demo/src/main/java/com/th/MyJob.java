package com.th;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: ThreePure
 * @CreateTime: 2023-03-14
 * @Description: 创建Job类：Job类是实际要执行的任务。它需要实现org.quartz.Job接口，并实现execute方法
 * @note Job（作业/任务）： 需要执行的具体工作任务。
 */
public class MyJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //需要执行的方法
        // 输出当前线程和时间
        System.out.println("SimpleJob:::" + Thread.currentThread().getName() +
                ":::" + SimpleDateFormat.getDateTimeInstance().format(new Date()));
    }
}
