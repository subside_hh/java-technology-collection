package com.th.review.simple;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @date: 2024-05-10 15:24
 * @author: hjb
 * @description:
 */
public class MyScheduler {

    public static void main(String[] args) {
        //进一步将Job包装成 JobDetail
        JobDetail jobDetail = JobBuilder.newJob(MyJob1.class)
                //指定JobName和groupName，两个合起来是唯一的标识
                .withIdentity("job1", "group1")
                // 用于拓展属性，在运行时可以从context中取到此处设置的参数
                .usingJobData("param1", "参数测试1")
                .usingJobData("param2", "参数测试2")
                .build();
        
        // Trigger
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("trigger1", "group1")
                .startNow()
                .withSchedule(SimpleScheduleBuilder.simpleSchedule() //简单触发器
                        //两秒执行一次
                        .withIntervalInSeconds(2)
                        //持续不断执行
                        .repeatForever())
                .build();

        StdSchedulerFactory factory =  new StdSchedulerFactory();
        Scheduler scheduler;
        try {
            //获取调度器 Scheduler
            scheduler = factory.getScheduler();
            //绑定关系 把JobDetail和Trigger绑定，注册到容器中
            scheduler.scheduleJob(jobDetail, trigger);
            //启动
            scheduler.start();
        } catch (SchedulerException e) {
            throw new RuntimeException(e);
        }
        

    }
    
}
