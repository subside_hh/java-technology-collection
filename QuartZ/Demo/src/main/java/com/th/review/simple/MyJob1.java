package com.th.review.simple;

import org.quartz.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @date: 2024-05-10 15:12
 * @author: hjb
 * @description: 自定义工作的 Job
 */
public class MyJob1 implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        System.out.println(" " + dateFormat.format(date) + "任务1执行了，" + jobDataMap.getString("param1"));

    }
}
