package com.th.logaspect;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan(basePackages = "com.th.logaspect.mapper")
public class LogAspectApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogAspectApplication.class, args);

    }

}
