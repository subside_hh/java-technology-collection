package com.th.logaspect.annotation;

import java.lang.annotation.*;

/**
 * @description: 自定义操作日志(用户端)注解
 * @author: HuangJiBin
 * @date: 2022/9/24  21:11
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserLogger {
    /**
     * 操作模块
     */
    String value() default "";

    /**
     * 操作类型
     */
    String type() default "";

    /**
     * 操作说明
     */
    String desc() default "";

    /**
     * 是否将当前日志记录到数据库中
     */
    boolean save() default true;
}
