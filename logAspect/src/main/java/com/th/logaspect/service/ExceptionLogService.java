package com.th.logaspect.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.ExceptionLog;

import java.util.List;

/**
 * @description: 异常日志接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:15
 */
public interface ExceptionLogService extends IService<ExceptionLog> {

    /**
     * @description 查询异常日志列表
     * @return com.th.logaspect.common.ResponseResult
     */
    ResponseResult listExceptionLog();

    /**
     * @description 批量删除异常日志信息
     * @param ids 日志id列表
     * @return com.th.logaspect.common.ResponseResult
     */
    ResponseResult deleteBatch(List<Long> ids);
}
