package com.th.logaspect.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.UserLog;
import com.th.logaspect.mapper.UserLogMapper;
import com.th.logaspect.service.UserLogService;
import com.th.logaspect.utils.PageUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: 用户端日志接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:20
 */
@Service
public class UserLogServiceImpl extends ServiceImpl<UserLogMapper, UserLog> implements UserLogService {
    /**
     * 用户日志列表
     */
    @Override
    public ResponseResult listUserLog() {
        QueryWrapper<UserLog> queryWrapper = new QueryWrapper<UserLog>()
                .orderByDesc("create_time");
        Page<UserLog> sysLogPage = baseMapper.selectPage(new Page<>(PageUtils.getPageNo(), PageUtils.getPageSize()), queryWrapper);

        return ResponseResult.success(sysLogPage);
    }

    /**
     * 批量删除用户日志
     * @param ids 用户日志id列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult deleteBatch(List<Long> ids) {
        int rows = baseMapper.deleteBatchIds(ids);
        return rows > 0 ? ResponseResult.success(): ResponseResult.error("批量删除用户日志失败");
    }
}
