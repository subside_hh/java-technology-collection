package com.th.logaspect.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.User;

import java.util.List;

/**
 * @description: 用户信息接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:24
 */
public interface UserService extends IService<User> {

    ResponseResult insertUser(User user);

    ResponseResult deleteBatch(List<Integer> ids);
}
