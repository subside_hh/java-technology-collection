package com.th.logaspect.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.ExceptionLog;
import com.th.logaspect.entity.log.UserLog;
import com.th.logaspect.mapper.ExceptionLogMapper;
import com.th.logaspect.service.ExceptionLogService;
import com.th.logaspect.utils.PageUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: 异常日志实现类
 * @author: HuangJiBin
 * @date: 2022/9/24  23:17
 */
@Service
public class ExceptionLogServiceImpl extends ServiceImpl<ExceptionLogMapper,ExceptionLog> implements ExceptionLogService {
    @Override
    public ResponseResult listExceptionLog() {
        QueryWrapper<ExceptionLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        Page<ExceptionLog> pg = new Page<>(PageUtils.getPageNo(), PageUtils.getPageSize());
        Page<ExceptionLog> sysLogPage = baseMapper.selectPage(pg, queryWrapper);
        return ResponseResult.success(sysLogPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult deleteBatch(List<Long> ids) {
        int rows = baseMapper.deleteBatchIds(ids);
        return rows > 0 ? ResponseResult.success(): ResponseResult.error("批量删除操作日志失败");
    }
}
