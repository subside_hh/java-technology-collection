package com.th.logaspect.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.UserLog;

import java.util.List;

/**
 * @description: 用户端日志接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:19
 */
public interface UserLogService extends IService<UserLog> {

    /**
     * @description 用户日志列表
     * @return com.th.logaspect.common.ResponseResult
     */
    ResponseResult listUserLog();

    /**
     * @description 批量删除用户日志
     * @return com.th.logaspect.common.ResponseResult
     */
    ResponseResult deleteBatch(List<Long> ids);
}
