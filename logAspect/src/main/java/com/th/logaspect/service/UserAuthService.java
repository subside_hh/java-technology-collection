package com.th.logaspect.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.UserAuth;

/**
 * @description: 用户信息表接口
 * @author: HuangJiBin
 * @date: 2022/9/25  15:06
 */
public interface UserAuthService extends IService<UserAuth> {

    ResponseResult updatePassword(String email);
}
