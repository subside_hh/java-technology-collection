package com.th.logaspect.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.AdminLog;

import java.util.List;

/**
 * @description: 管理端日志接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:11
 */
public interface AdminLogService extends IService<AdminLog> {

    /**
     * 分页查询操作日志
     */
    ResponseResult listAdminLog();

    /**
     * 批量删除操作日志
     * @param ids 操作日志id集合
     */
    ResponseResult deleteAdminLog(List<Long> ids);

}
