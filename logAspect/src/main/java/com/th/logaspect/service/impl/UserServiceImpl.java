package com.th.logaspect.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.User;
import com.th.logaspect.mapper.UserAuthMapper;
import com.th.logaspect.mapper.UserMapper;
import com.th.logaspect.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @description: 用户表实现类
 * @author: HuangJiBin
 * @date: 2022/9/24  23:25
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserAuthMapper userAuthMapper;

    /**
     *  添加用户
     * @param user User实体类
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult insertUser(User user) {
        /*user.setPassword(user.getPassword());
        user.setStatus(1);
        baseMapper.insert(user);
        // roleMapper.insertToUserId(user.getId(),user.getRoleId());*/
        int a = 10/0;
        return ResponseResult.success(user);
    }

    /**
     * 删除用户
     * @param ids 用户id列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult deleteBatch(List<Integer> ids) {
        /*userAuthMapper.deleteByUserIds(ids);
        int rows = baseMapper.deleteBatchIds(ids);
        return rows > 0? ResponseResult.success(): ResponseResult.error("删除失败");*/
        return  ResponseResult.success();
    }

}
