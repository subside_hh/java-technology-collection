package com.th.logaspect.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.UserAuth;
import com.th.logaspect.mapper.UserAuthMapper;
import com.th.logaspect.service.UserAuthService;
import org.springframework.stereotype.Service;

/**
 * @description: 用户信息实现类
 * @author: HuangJiBin
 * @date: 2022/9/25  15:07
 */
@Service
public class UserAuthServiceImpl extends ServiceImpl<UserAuthMapper, UserAuth> implements UserAuthService {
    @Override
    public ResponseResult updatePassword(String email) {
        System.out.println("email:" + email);
        return ResponseResult.success("修改成功！！！");
    }
}
