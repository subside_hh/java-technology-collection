package com.th.logaspect.common;

/**
 * @description: 全局变量
 * @author: HuangJiBin
 * @date: 2022/9/24  23:34
 */
public class Constants {
    /**
     * 未知的
     */
    public static final String UNKNOWN = "未知";

}
