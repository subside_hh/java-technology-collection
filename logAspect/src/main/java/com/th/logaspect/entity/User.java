package com.th.logaspect.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/*
 * CREATE TABLE `b_user` (
 *   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
 *   `username` varchar(100) DEFAULT NULL COMMENT '账号',
 *   `password` varchar(100) DEFAULT NULL COMMENT '登录密码',
 *   `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 *   `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
 *   `status` int(10) DEFAULT '1' COMMENT '状态 0:禁用 1:正常',
 *   `login_type` int(10) DEFAULT NULL COMMENT '登录方式',
 *   `user_auth_id` bigint(10) DEFAULT NULL COMMENT '用户详情id',
 *   `role_id` bigint(20) DEFAULT NULL COMMENT '角色id',
 *   `ip_address` varchar(255) DEFAULT NULL COMMENT 'ip地址',
 *   `ip_source` varchar(255) DEFAULT NULL COMMENT 'ip来源',
 *   `os` varchar(100) DEFAULT NULL COMMENT '登录系统',
 *   `last_login_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '最后登录时间',
 *   `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
 *   PRIMARY KEY (`id`) USING BTREE,
 *   UNIQUE KEY `username` (`username`) USING BTREE
 * ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统管理-用户基础信息表'
 * */

/**
 * @description: 用户实体类
 * @author: HuangJiBin
 * @date: 2022/9/24  21:27
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("b_user")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "User对象", description = "系统管理-用户基础信息表")
public class User extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账号")
    private String username;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "状态")
    private Integer status;


    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "最后更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "最后登录时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date lastLoginTime;

    @ApiModelProperty(value = "角色ID")
    private Integer roleId;

    @ApiModelProperty(value = "IP地址")
    private String ipAddress;

    @ApiModelProperty(value = "IP来源")
    private String ipSource;

    @ApiModelProperty(value = "登录系统")
    private String os;

    @ApiModelProperty(value = "浏览器")
    private String browser;

    @ApiModelProperty(value = "用户信息id")
    private Integer userAuthId;

    @ApiModelProperty(value = "登录类型")
    private Integer loginType;

}
