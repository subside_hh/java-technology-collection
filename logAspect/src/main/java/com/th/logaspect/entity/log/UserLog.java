package com.th.logaspect.entity.log;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.th.logaspect.entity.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/*
 * CREATE TABLE `b_user_log` (
 *   `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
 *   `user_id` bigint(20) DEFAULT NULL COMMENT '操作用户ID',
 *   `ip` varchar(50) DEFAULT NULL COMMENT 'ip地址',
 *   `address` varchar(255) DEFAULT NULL COMMENT '操作地址',
 *   `type` varchar(100) DEFAULT NULL COMMENT '操作类型',
 *   `description` varchar(255) DEFAULT NULL COMMENT '操作日志',
 *   `model` varchar(255) DEFAULT NULL COMMENT '操作模块',
 *   `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
 *   `result` varchar(255) DEFAULT NULL COMMENT '操作结果',
 *   `access_os` varchar(255) DEFAULT NULL COMMENT '操作系统',
 *   `browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
 *   `client_type` varchar(255) DEFAULT NULL COMMENT '客户端类型',
 *   PRIMARY KEY (`id`) USING BTREE
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户日志表'
 * */

/**
 * @description: 用户日志实体类
 * @author: HuangJiBin
 * @date: 2022/9/24  21:01
 */
@Data
@Builder
@TableName("b_user_log")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户日志对象", description = "用户操作日志实体类,'b_user_log'")
public class UserLog extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ip地址")
    private String ip;

    @ApiModelProperty(value = "操作地址")
    private String address;

    @ApiModelProperty(value = "操作类型")
    private String type;

    @ApiModelProperty(value = "操作日志")
    private String description;

    @ApiModelProperty(value = "操作模块")
    private String model;

    @ApiModelProperty(value = "操作系统")
    private String accessOs;
    @ApiModelProperty(value = "客户端类型")
    private String clientType;

    @ApiModelProperty(value = "浏览器")
    private String browser;

    @ApiModelProperty(value = "操作时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "操作结果")
    private String result;
}
