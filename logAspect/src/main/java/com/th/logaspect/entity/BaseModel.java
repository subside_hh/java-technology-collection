package com.th.logaspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @description: 基础实体类
 * @author: HuangJiBin
 * @date: 2022/9/24  20:41
 */
@Data
@Accessors(chain = true)
public class BaseModel {
    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

}
