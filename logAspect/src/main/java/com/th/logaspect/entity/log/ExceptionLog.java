package com.th.logaspect.entity.log;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.th.logaspect.entity.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/*
 * CREATE TABLE `b_exception_log` (
 *   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
 *   `username` varchar(255) DEFAULT NULL COMMENT '用户名',
 *   `ip` varchar(255) DEFAULT NULL COMMENT 'IP',
 *   `ip_source` varchar(255) DEFAULT NULL COMMENT 'ip来源',
 *   `method` varchar(255) DEFAULT NULL COMMENT '请求方法',
 *   `operation` mediumtext COMMENT '描述',
 *   `params` mediumtext COMMENT '参数',
 *   `exception_json` mediumtext COMMENT '异常对象json格式',
 *   `exception_message` mediumtext COMMENT '异常简单信息,等同于e.getMessage',
 *   `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '发生时间',
 *   PRIMARY KEY (`id`) USING BTREE
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='异常日志表'
 * */

/**
 * @description: 异常日志实体类
 * @author: HuangJiBin
 * @date: 2022/9/24  21:05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("b_exception_log")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "异常日志对象", description = "系统异常日志实体类，表'b_exception_log'")
public class ExceptionLog extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "ip")
    private String ip;

    @ApiModelProperty(value = "ip来源")
    private String ipSource;

    @ApiModelProperty(value = "请求方法")
    private String method;

    @ApiModelProperty(value = "描述")
    private String operation;

    @ApiModelProperty(value = "参数")
    private String params;

    @ApiModelProperty(value = "异常对象json格式")
    private String exceptionJson;

    @ApiModelProperty(value = "异常简单信息,等同于e.getMessage")
    private String exceptionMessage;

    @ApiModelProperty(value = "发生时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
