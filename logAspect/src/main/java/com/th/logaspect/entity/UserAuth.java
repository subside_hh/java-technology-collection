package com.th.logaspect.entity;

/*
 * CREATE TABLE `b_user_auth` (
 *   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
 *   `email` varchar(50) DEFAULT NULL COMMENT '邮箱号',
 *   `nickname` varchar(50) NOT NULL COMMENT '用户昵称',
 *   `avatar` varchar(1024) NOT NULL DEFAULT '' COMMENT '用户头像',
 *   `intro` varchar(255) DEFAULT NULL COMMENT '用户简介',
 *   `web_site` varchar(255) DEFAULT NULL COMMENT '个人网站',
 *   `is_disable` int(1) NOT NULL DEFAULT '1' COMMENT '是否禁用',
 *   `create_time` datetime DEFAULT NULL COMMENT '创建时间',
 *   `update_time` datetime DEFAULT NULL COMMENT '更新时间',
 *   PRIMARY KEY (`id`) USING BTREE
 * ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户信息表'
 * */

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: 用户信息表实体类
 * @author: HuangJiBin
 * @date: 2022/9/25  9:52
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("b_user_auth")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "用户信息对象", description = "系统管理-用户信息表")
public class UserAuth extends BaseModel implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "邮箱号")
    private String email;

    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "用户简介")
    private String intro;

    @ApiModelProperty(value = "个人网站")
    private String webSite;

    @ApiModelProperty(value = "是否禁用")
    @TableField(value = "is_disable")
    private Boolean isDisable;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

