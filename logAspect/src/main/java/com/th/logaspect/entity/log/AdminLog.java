package com.th.logaspect.entity.log;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.th.logaspect.entity.BaseModel;
import com.th.logaspect.utils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/*
  CREATE TABLE `b_admin_log` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `username` varchar(255) DEFAULT NULL COMMENT '操作用户',
    `request_url` varchar(255) DEFAULT NULL COMMENT '请求接口',
    `type` varchar(255) DEFAULT NULL COMMENT '请求方式',
    `operation_name` varchar(255) DEFAULT NULL COMMENT '操作名称',
    `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
    `source` varchar(255) DEFAULT NULL COMMENT 'ip来源',
    `spend_time` bigint(20) DEFAULT NULL COMMENT '请求接口耗时',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `params_json` mediumtext COMMENT '请求参数',
    `class_path` varchar(255) DEFAULT NULL COMMENT '类地址',
    `method_name` varchar(255) DEFAULT NULL COMMENT '方法名',
    PRIMARY KEY (`id`) USING BTREE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='管理端日志表'
  */

/**
 * @description: 管理端日志实体类
 * @author: HuangJiBin
 * @date: 2022/9/24  20:32
 */
@Data
@Builder
@TableName("b_admin_log")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "管理端日志对象", description = "管理端日志实体类,'b_admin_log'")
public class AdminLog extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "操作用户")
    private String username;

    @ApiModelProperty(value = "请求接口")
    private String requestUrl;

    @ApiModelProperty(value = "请求方式")
    private String type;

    @ApiModelProperty(value = "操作名称")
    private String operationName;

    @ApiModelProperty(value = "ip")
    private String ip;

    @ApiModelProperty(value = "ip来源")
    private String source;

    @ApiModelProperty(value = "请求参数")
    private String paramsJson;

    @ApiModelProperty(value = "类地址")
    private String classPath;

    @ApiModelProperty(value = "方法名")
    private String methodName;

    @ApiModelProperty(value = "请求接口耗时")
    private Long spendTime;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = DateUtils.FORMAT_STRING, timezone = "GMT+8")
    private Date createTime;

    public AdminLog(

            String username,
            String requestUrl,
            String type,
            String operationName,
            String ip,
            String source,
            String paramsJson,
            String classPath,
            String methodName,
            Long spendTime,
            Date createTime) {
        this.username = username;
        this.requestUrl = requestUrl;
        this.type = type;
        this.operationName = operationName;
        this.ip = ip;
        this.source = StringUtils.isBlank(source) ? "未知" : source;
        this.paramsJson = paramsJson;
        this.classPath = classPath;
        this.methodName = methodName;
        this.spendTime = spendTime;
        this.createTime = createTime;
    }
}

