package com.th.logaspect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.logaspect.entity.log.UserLog;
import org.springframework.stereotype.Repository;

/**
 * @description: 用户日志表 Mapper 接口
 * @author: HuangJiBin
 * @date: 2022/9/24  22:24
 */
@Repository
public interface UserLogMapper extends BaseMapper<UserLog> {

}
