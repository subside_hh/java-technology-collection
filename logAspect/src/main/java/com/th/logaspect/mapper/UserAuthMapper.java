package com.th.logaspect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.logaspect.entity.UserAuth;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description: 用户信息Mapper接口
 * @author: HuangJiBin
 * @date: 2022/9/25  15:08
 */
@Repository
public interface UserAuthMapper extends BaseMapper<UserAuth> {
    void deleteByUserIds(@Param("ids") List<Integer> ids);
}
