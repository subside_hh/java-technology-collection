package com.th.logaspect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.logaspect.entity.log.ExceptionLog;
import org.springframework.stereotype.Repository;

/**
 * @description: 异常日志表 Mapper 接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:02
 */
@Repository
public interface ExceptionLogMapper extends BaseMapper<ExceptionLog> {

}
