package com.th.logaspect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.logaspect.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @description: 用户表 Mapper接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:26
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
