package com.th.logaspect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.th.logaspect.entity.log.AdminLog;
import org.springframework.stereotype.Repository;

/**
 * @description: 管理端日志表 Mapper 接口
 * @author: HuangJiBin
 * @date: 2022/9/24  23:05
 */
@Repository
public interface AdminLogMapper extends BaseMapper<AdminLog> {
}
