package com.th.logaspect.aspect;

import com.th.logaspect.annotation.UserLogger;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.log.UserLog;
import com.th.logaspect.mapper.UserLogMapper;
import com.th.logaspect.utils.DateUtils;
import com.th.logaspect.utils.IpUtils;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @description: 自定义用户日志切面
 * @author: HuangJiBin
 * @date: 2022/9/24  21:31
 */
@Aspect
@Component
@RequiredArgsConstructor
public class UserLoggerAspect {

    private static final Logger logger = LoggerFactory.getLogger(UserLoggerAspect.class);

    private final UserLogMapper userLogMapper;

    /**
     * 设置操作日志切入点   在注解的位置切入代码
     */
    @Pointcut("@annotation(userLogger)")
    public void pointcut(UserLogger userLogger) {
    }

    /*
    //该定义的切入点等效于上面
    @Pointcut("@annotation(com.th.logaspect.annotation.UserLogger)")
    public void pointcut() {
    }
    */

    @Around(value = "pointcut(userLogger)")
    public Object doAround(ProceedingJoinPoint joinPoint, UserLogger userLogger) throws Throwable {

        //先执行业务
        Object result = joinPoint.proceed();

        try {
            // 日志收集
            handle(joinPoint, (ResponseResult) result);

        } catch (Exception e) {
            logger.error("日志记录出错!", e);
        }

        return result;
    }

    /**
     * 记录操作日志
     *
     * @param joinPoint 方法的执行点
     * @param result    方法返回值
     */
    public void handle(ProceedingJoinPoint joinPoint, ResponseResult result) throws Throwable {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            //获取切入点所在的方法
            Method method = signature.getMethod();
            //获取操作
            UserLogger annotation = method.getAnnotation(UserLogger.class);
            if (!annotation.save()) {
                return;
            }

            //获取用户操作相关的设备信息
            String ip = IpUtils.getIp(request);
            //UserAgent类源于IP管理jar-->
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
            String clientType = userAgent.getOperatingSystem().getDeviceType().toString();
            String os = userAgent.getOperatingSystem().getName();
            String browser = userAgent.getBrowser().toString();

            UserLog userLog = UserLog.builder()
                    .model(annotation.value())
                    .type(annotation.type())
                    .description(annotation.desc())
                    .createTime(DateUtils.getNowDate())
                    .ip(ip)
                    .address(IpUtils.getCityInfo(ip))
                    .clientType(clientType)
                    .accessOs(os)
                    .browser(browser)
                    .result(result.getMessage())
                    .build();
            userLogMapper.insert(userLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
