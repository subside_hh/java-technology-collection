package com.th.logaspect.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.th.logaspect.annotation.AdminLogger;
import com.th.logaspect.entity.UserAuth;
import com.th.logaspect.entity.log.AdminLog;
import com.th.logaspect.entity.log.ExceptionLog;
import com.th.logaspect.entity.vo.SystemUserVO;
import com.th.logaspect.mapper.AdminLogMapper;
import com.th.logaspect.mapper.ExceptionLogMapper;
import com.th.logaspect.utils.AspectUtils;
import com.th.logaspect.utils.DateUtils;
import com.th.logaspect.utils.IpUtils;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;

/**
 * @description: 自定义管理端日志切面
 * @author: HuangJiBin
 * @date: 2022/9/24  21:30
 */
@Aspect
@Component
@RequiredArgsConstructor
public class AdminLoggerAspect  {

    private static final Logger logger = LoggerFactory.getLogger(AdminLoggerAspect.class);

    private final AdminLogMapper adminLogMapper;

    private final ExceptionLogMapper exceptionLogMapper;

    /**
     * 开始时间
     */
    Date startTime;

    @Pointcut(value = "@annotation(adminLogger)")
    public void pointcut(AdminLogger adminLogger) {

    }

    @Around(value = "pointcut(adminLogger)")
    public Object doAround(ProceedingJoinPoint joinPoint, AdminLogger adminLogger) throws Throwable {
        //因给了演示账号所有权限以供用户观看，所以执行业务前需判断是否是管理员操作
        //Assert.isTrue(StpUtil.hasRole("admin"),"演示账号不允许操作!!");

        startTime = new Date();

        //先执行业务
        Object result = joinPoint.proceed();
        try {
            // 日志收集
            handle(joinPoint,getHttpServletRequest());

        } catch (Exception e) {
            logger.error("日志记录出错!", e);
        }

        return result;
    }


    @AfterThrowing(value = "pointcut(adminLogger)", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, AdminLogger adminLogger, Throwable e) throws Exception {
        //此异常不做保存日志的处理 因为此异常基本都是业务中Assert返回的异常
        if (e.toString().contains("IllegalArgumentException")) {
            return;
        }

        HttpServletRequest request = getHttpServletRequest();
        String ip = IpUtils.getIp(request);
        String operationName = AspectUtils.INSTANCE.parseParams(joinPoint.getArgs(), adminLogger.value());
        // 获取参数名称字符串
        String paramsJson = getParamsJson((ProceedingJoinPoint) joinPoint);
        //SystemUserVO user = (SystemUserVO) StpUtil.getSession().get(CURRENT_USER);

        ExceptionLog exception = ExceptionLog.builder()
                .ip(ip)
                .ipSource(IpUtils.getCityInfo(ip))
                .params(paramsJson)
                //.username(user.getUsername())
                .username("测试用户")
                .method(joinPoint.getSignature().getName())
                .exceptionJson(JSON.toJSONString(e))
                .exceptionMessage(e.getMessage())
                .operation(operationName)
                .createTime(DateUtils.getNowDate())
                .build();
        exceptionLogMapper.insert(exception);
    }

    /**
     * 管理员日志收集
     *
     * @param point
     * @throws Exception
     */
    private void handle(ProceedingJoinPoint point,HttpServletRequest request) throws Exception {

        Method currentMethod = AspectUtils.INSTANCE.getMethod(point);

        //获取操作名称
        AdminLogger annotation = currentMethod.getAnnotation(AdminLogger.class);

        boolean save = annotation.save();

        String operationName = AspectUtils.INSTANCE.parseParams(point.getArgs(), annotation.value());
        if (!save) {
            return;
        }
        // 获取参数名称字符串
        String paramsJson = getParamsJson(point) ;

        // 当前操作用户
        //SystemUserVO user = (SystemUserVO) StpUtil.getSession().get(CURRENT_USER);
        //本案例未引用Sa-token，以下数据为测试使用，真实业务需要重session中获取，
        SystemUserVO user = new SystemUserVO();
        user.setNickname("测试用户");

        String type = request.getMethod();
        String ip = IpUtils.getIp(request);
        String url = request.getRequestURI();

        // 存储日志
        Date endTime = new Date();
        Long spendTime = endTime.getTime() - startTime.getTime();

        AdminLog adminLog = AdminLog.builder()
                .ip(ip)
                .source(IpUtils.getCityInfo(ip))
                .type(type)
                .requestUrl(url)
                .username(user.getNickname())
                .paramsJson(paramsJson)
                .classPath(point.getTarget().getClass().getName())
                .methodName(point.getSignature().getName())
                .operationName(operationName)
                .spendTime(spendTime)
                .build();
        adminLogMapper.insert(adminLog);
    }

    private String getParamsJson(ProceedingJoinPoint joinPoint) throws ClassNotFoundException, NoSuchMethodException {
        // 参数值
        Object[] args = joinPoint.getArgs();
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] parameterNames = methodSignature.getParameterNames();

        // 通过map封装参数和参数值
        HashMap<String, Object> paramMap = new HashMap();
        for (int i = 0; i < parameterNames.length; i++) {
            paramMap.put(parameterNames[i], args[i]);
        }

        boolean isContains = paramMap.containsKey("request");
        if (isContains) {
            paramMap.remove("request");
        }
        String paramsJson = JSONObject.toJSONString(paramMap);
        return paramsJson;
    }

    private HttpServletRequest getHttpServletRequest() {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        return (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
    }

}
