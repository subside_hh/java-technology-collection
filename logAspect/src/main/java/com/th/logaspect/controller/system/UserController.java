package com.th.logaspect.controller.system;

import com.th.logaspect.annotation.AdminLogger;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.entity.User;
import com.th.logaspect.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 客户端对用户的操作
 * @author: HuangJiBin
 * @date: 2022/9/25  15:51
 */
@RestController(value = "sysUserController")
@RequiredArgsConstructor
@Api(tags = "系统用户管理")
@RequestMapping("/system/user")
public class UserController {
    private final UserService userService;

    @GetMapping(value = "/hello")
    @ApiOperation(value = "hello测试", httpMethod = "GET", response = String.class, notes = "hello测试")
    public String  hello() {
        //业务层定义了 异常
        return "hello";
    }

    /**
     * @description 用于异常日志
     */
    @PostMapping(value = "/create")
    @ApiOperation(value = "添加用户", httpMethod = "POST", response = ResponseResult.class, notes = "添加用户")
    @AdminLogger(value = "添加用户")
    public ResponseResult insert(@RequestBody User user) {
        //业务层定义了 异常
        return userService.insertUser(user);
    }

    /**
     * @description 用于测试管理员日志
     */
    @RequestMapping(value = "/remove/{id}",method = RequestMethod.DELETE)
    @ApiOperation(value = "删除用户", httpMethod = "DELETE", response = ResponseResult.class, notes = "删除用户")
    @AdminLogger(value = "删除用户")
    public ResponseResult deleteBatch(@PathVariable("id") Integer id) {
        System.out.println("id:"+id);
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(id);
        return userService.deleteBatch(ids);
    }
}
