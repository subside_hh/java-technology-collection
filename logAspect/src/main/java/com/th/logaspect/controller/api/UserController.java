package com.th.logaspect.controller.api;

import com.th.logaspect.annotation.UserLogger;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.service.UserAuthService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @description: 用户Controller
 * @author: HuangJiBin
 * @date: 2022/9/24  23:56
 */
@RestController(value = "apiUserController")
@Api(tags = "用户相关接口")
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserAuthService userAuthService;


    /**
     * @description 用于测试用户日志
     */
    @RequestMapping(value = "/updatePassword",method = RequestMethod.POST)
    @UserLogger(value = "个人中心模块-邮箱账号修改密码",type = "修改",desc = "邮箱账号修改密码")
    public ResponseResult updatePassword(String email){
        return userAuthService.updatePassword(email);
    }

}
