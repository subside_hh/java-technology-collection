package com.th.logaspect.controller.log;

import com.th.logaspect.annotation.AdminLogger;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.service.UserLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 用户日志Controller
 * @author: HuangJiBin
 * @date: 2022/9/25  15:39
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "用户日志管理")
@RequestMapping("/system/userLog")
public class UserLogController {

    private final UserLogService userLogService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "用户日志列表", httpMethod = "GET", response = ResponseResult.class, notes = "用户日志列表")
    public ResponseResult list() {
        return userLogService.listUserLog();
    }

    @DeleteMapping(value = "/delete")
    @AdminLogger(value = "删除用户日志")
    @ApiOperation(value = "删除用户日志", httpMethod = "DELETE", response = ResponseResult.class, notes = "删除用户日志")
    public ResponseResult deleteBatch(@RequestBody List<Long> ids) {
        return userLogService.deleteBatch(ids);
    }
}
