package com.th.logaspect.controller.log;

import com.th.logaspect.annotation.AdminLogger;
import com.th.logaspect.common.ResponseResult;
import com.th.logaspect.service.AdminLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 管理端日志Controller
 * @author: HuangJiBin
 * @date: 2022/9/24  23:55
 */
@RestController
@RequiredArgsConstructor
@Api(tags = "管理端日志管理")
@RequestMapping("/system/adminLog")

public class AdminLogController {

    private final AdminLogService adminLogService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "操作日志列表", httpMethod = "GET", response = ResponseResult.class, notes = "操作日志列表")
    public ResponseResult list() {
        return adminLogService.listAdminLog();
    }

    @DeleteMapping(value = "/delete")
    @AdminLogger(value = "删除操作日志")
    @ApiOperation(value = "删除操作日志", httpMethod = "DELETE", response = ResponseResult.class, notes = "删除操作日志")
    public ResponseResult delete(@RequestBody List<Long> ids) {
        return adminLogService.deleteAdminLog(ids);
    }
}