![image-logo](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220721175045996.png)
[TOC]

## 一、Docker简介

> 通俗易懂

软件安装的时候实现带环境和配置安装，解决开发环境时能正常运行而生产环境无法正常运行，为了解决这一问题，在安装的时候把原始的环境“复制”下来；实现**一次构建、随处运行**；

> 原理

Docker是基于**Go**语言实现的云开源项目。Docker的主要目标是“**Build，Ship and Run Any App,Anywhere**”；通过对应用组件的封装、分发、部署、运行等生命周期的管理，使用户的APP（可以是一个WEB应用或数据库应用等等）及其运行环境能够做到“一次镜像，处处运行”。Docker容器在任何操作系统上都是一致的，这就实现了**跨平台**、**跨服务器**。

> 发展

![image-20220721214356209](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220721214356209.png)

+ 传统虚拟机

  在一种操作系统里面运行另一种操作系统，对于底层系统来说，虚拟机就是一个普通文件，不需要了就删掉，对其他部分毫无影响。传统虚拟机技术基于安装在主操作系统上的虚拟机管理系统（如：VirtualBox和WMWare等），创建虚拟机（虚拟出各种硬件），在虚拟机上安装从操作系统，在从操作系统中部署各种运用；

  > 缺点

  1   资源占用多        2  冗余步骤多         3  启动慢

+ 容器虚拟化技术

  Linux容器(Linux Containers，缩写为 LXC)是与系统其他部分隔离开的一系列进程，从另一个镜像运行，并由该镜像提供支持进程所需的全部文件。**容器与虚拟机不同，不需要捆绑一整套操作系统，只需要软件工作所需的库资源和设置**。系统因此而变得**高效轻量**并保证部署在任何环境中的软件都能始终如一地运行。

> 三大要素

+ 镜像文件

  将应用程序和配置依赖打包好形成一个可交付的运行环境，这个打包好的运行环境就是image镜像文件。

+ 容器实例

   一个容器运行一种服务，当我们需要的时候，就可以通过docker客户端创建一个对应的运行实例，也就是我们的容器，也就是镜像的具体表现；

+ 仓库

  放一堆镜像的地方，我们可以把镜像发布到仓库中，需要的时候再从仓库中拉下来就可以了。



## 二、安装

Docker并非是应用通用的容器工具，它依赖于已存在的Linux内核环境，Docker实质上是在已运行的Linux下制造一个隔离的文件环境，因此Docker必须部署在Linux内核系统上，如果Windows上要部署Docker，必须要安装一个虚拟Linux环境，可以借助默认的WSL实现；

>  CentOS安装Docker

+ 前提条件：CentOS 仅发行版本中的内核支持 Docker。运行在CentOS 7 (64-bit)上；系统为64位、内核版本为 3.8以上；
+ [官网教程](https://docs.docker.com/engine/install/centos/)

1. 卸载旧版本

   ```bash
   sudo yum remove docker \
                     docker-client \
                     docker-client-latest \
                     docker-common \
                     docker-latest \
                     docker-latest-logrotate \
                     docker-logrotate \
                     docker-engine
   ```

2. yum安装gcc相关

   ```bash
   yum -y install gcc
   yum -y install gcc-c++
   ```

3. 安装需要的软件包

   ```bash
   sudo yum install -y yum-utils
   ```

4. 配置阿里云镜像,否则很难下载成功

   ```bash
   yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   ```

   如果阿里云镜像无法下载，可使用官网的仓库：

   ```bash
   sudo yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo
   ```

5. 更新yun软件包索引

   ```bash
   yum makecache fast
   ```

6. 安装DOCKER CE

   ```bash
   yum -y install docker-ce docker-ce-cli containerd.io
   ```

7. 启动Docker

   ```bash
   systemctl start docker
   ```

   ```bash
   net start com.docker.service  # Windows下启动
   ```

8. 测试

   ```bash
   docker version
       
   docker run hello-world
   ```

9. 卸载

   ```bash
   yum remove docker-ce docker-ce-cli containerd.io
   rm -rf /var/lib/docker
   rm -rf /var/lib/containerd
   ```


10. 配置镜像加速器（[阿里云](https://cr.console.aliyun.com/cn-qingdao/instances/mirrors)），解决下载镜像慢的问题

    注册一个属于自己的阿里云账户,登录控制台选择容器服务-->容器镜像服务-->获得加速器地址连接

    ```bash
    sudo mkdir -p /etc/docker
    sudo tee /etc/docker/daemon.json <<-'EOF'
    {
      "registry-mirrors": ["https://......aliyuncs.com"]
    }
    EOF
    ```

    配置完后重新启动：

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl restart docker
    ```



> Docker中软件安装
>
>  > 搜索镜像
>
> > > 拉取镜像
>
> > > > 查看镜像
>
> > > > > 启动镜像(服务端口映射)
>
> > > > 停止容器
>
> > > 移除容器
>
> > 移除镜像
>
> 生命周期结束



## 三、常用命令

> 1、启动类命令

| 含义                         | 命令                     |
| ---------------------------- | ------------------------ |
| 启动docker                   | systemctl start docker   |
| 停止docker                   | systemctl stop    docker |
| 重启docker                   | systemctl restart docker |
| 查看docker状态               | systemctl status docker  |
| 开机启动                     | systemctl enable docker  |
| 查看docker概要信息           | docker info              |
| 查看docker总体帮助文档       | docker --help            |
| 查看docker的状态  CPU、I/O等 | docker stats             |



> 2、镜像命令

 1、**`docker images`**   列出本地主机上的镜像


关于镜像相关的信息：

+ REPOSITORY：表示镜像的**仓库源**
+ TAG：镜像的**标签版本号**
+ IMAGE ID：**镜像ID**
+ CREATED：镜像创建时间
+ SIZE：镜像大小

同一仓库源可以有多个 TAG版本，代表这个仓库源的不同个版本，我们使用 `REPOSITORY:TAG` 来定义不同的镜像。

参数：

+ -a :列出本地所有的镜像（含历史映像层）
+ -q :只显示镜像ID

2、`docker search` 某个XXX镜像名字     查询某个镜像   [官方网站](https://hub.docker.com/)

+ --limit : 只列出N个镜像，默认25个

3、`docker pull 某个XXX镜像名字`  下载镜像

+ `docker pull 镜像名字[:TAG] `   
+ `docker pull 镜像名字 `   没有TAG就是最新版、等价于 docker pull 镜像名字:latest

4、`docker system df` 查看镜像/容器/数据卷所占的空间

5、`docker rmi 某个XXX镜像名字ID`  删除镜像

+ -f : 强制删除

+ 删除单个 `docker rmi -f 镜像ID`
+ 删除多个 `docker rmi -f 镜像名1:TAG 镜像名2:TAG`
+ 删除全部 `docker rmi -f $(docker images -qa)`

其他命令见 [四、镜像]()

6、谈谈**docker虚悬镜像**是什么？

+ **仓库名、标签都是<none>的镜像，俗称虚悬镜像dangling image**

+ `docker image ls -f dangling=true`：查看所有的虚悬镜像；
+ `docker image prune`：删除所有的虚悬镜像；



> 3、容器命令

1、` docker run [OPTIONS] IMAGE [COMMAND] [ARG...]`新建并启动容器

+ `--name="容器新名字"`    为容器指定一个名称；

+ -d: 后台运行容器并返回容器ID，也即启动守护式容器(后台运行)；

+ -i：以交互模式运行容器，通常与 -t 同时使用；

+ -t：为容器重新分配一个伪输入终端，通常与 -i 同时使用；也即启动交互式容器(前台有伪终端，等待交互)；

+ -P: 随机端口映射，大写P

+ -p: 指定端口映射，小写p

  ![image-20220822093727401](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822093727401.png)

2、`docker ps [OPTIONS]` 列出当前所有正在运行的容器

+ -a :列出当前所有正在运行的容器 + 历史上运行过的

+ -l :显示最近创建的容器。

+ -n 3：显示最近3个创建的容器。

+ -q :静默模式，只显示容器编号。
+ -s : 显示文件大小

3、退出|启动|停止容器

+ `exit方式`  run进去容器，exit退出，**容器停止**。

+ `ctrl+p+q方式` run进去容器，ctrl+p+q退出，**容器不停止**。

+ `docker start 容器ID或者容器名`  启动已停止运行的容器

+ `docker restart 容器ID或者容器名`   重启容器

+ `docker stop 容器ID或者容器名`   停止容器

+ `docker kill 容器ID或容器名`    强制停止容器

+ `docker rm 容器ID`  删除已停止的容器

+ `docker rm -f $(docker ps -a -q)   `     一次性删除多个容器实例

+ `docker ps -a -q | xargs docker rm` 一次性删除多个容器实例

4、容器内部命令

+ `docker logs 容器ID` 查看容器日志
  + -f  ：跟踪日志输出
+ **`docker top 容器ID`**  查看容器内运行的进程
+ `docker inspect 容器ID` 查看容器内部细节
+ `docker exec -it 容器ID /bin/bash` 进入正在运行的容器并以命令行交互 ，exit后不会退出
  + `docker exec -it 容器ID redis-cli`  :使用redis-cli进行交互式打开
  + 最佳实例：一般用`-d`后台启动的程序，再用`exec`进入对应容器实例
+ `docker attach 容器ID` 重新进入容器，exit后会退出

5、文件拷贝命令

+ 容器→主机   docker cp 容器ID:容器内路径 目的主机路径

6、导入和导出命令

+ `export `导出容器的内容留作为一个tar归档文件[对应import命令]                   `docker export 容器ID > 文件名.tar`
+ `import` 从tar包中的内容创建一个新的文件系统再导入为镜像  **`cat 文件名.tar | docker import - 镜像用户/镜像名:镜像版本号`**



## 四、镜像



> 镜像是什么

一种**轻量级、可执行的独立软件包**，它**包含运行某个软件所需的所有内容**，我们把应用程序和配置依赖打包好形成一个可交付的运行环境(包括**代码**、**运行时需要的库**、**环境变量**和**配置文件等**)，这个打包好的运行环境就是IMAGE镜像文件。

只有通过镜像才能生成Docker实例（容器）。

镜像是分层的，以我们的pull为例，在下载的过程中我们可以看到docker的镜像好像是在一层一层的在下载；

> **UnionFS（联合文件系统）**

是一种**分层、轻量级**并且**高性能**的文件系统，它支持<u>对文件系统的修改作为一次提交来一层层的叠加</u>，同时可以将<u>不同目录挂载到同一个虚拟文件系统下</u>(unite several directories into a single virtual filesystem)。**Union** **文件系统是** **Docker** **镜像的基础**。镜像可以通过分层来进行继承，基于基础镜像（没有父镜像），可以制作各种具体的应用镜像。

特性：一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统叠加起来，这样最终的文件系统会包含所有底层的文件和目录。

> Docker镜像加载原理

Docker的镜像实际上由一层一层的文件系统组成，这种层级的文件系统UnionFS。

**bootfs**(boot file system)主要包含`bootloader`和`kernel`,` bootloader`主要是引导加载`kernel`, Linux刚启动时会加载`bootfs`文件系统，在Docker镜像的最底层是引导文件系统`bootfs`。这一层与我们典型的Linux/Unix系统是一样的，包含`boot`加载器和内核。当`boot`加载完成之后整个内核就都在内存中了，此时内存的使用权已由bootfs转交给内核，此时系统也会卸载bootfs。

**rootfs** (root file system) ，在`bootfs`之上。包含的就是典型 Linux 系统中的` /dev`,` /proc`, `/bin`, `/etc `等标准目录和文件。`rootfs`就是各种不同的操作系统发行版，比如Ubuntu，Centos等等。

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822105406328.png" alt="image-20220822105406328" style="zoom:67%;" />

镜像分层最大的一个好处就是**共享资源**，方便复制迁移，就是为了复用。

当容器启动时，一个新的可写层被加载到镜像的顶部。这一层通常被称作“容器层”，“容器层”之下的都叫“镜像层”。所有对容器的改动 - 无论添加、删除、还是修改文件都只会发生在容器层中。只有容器层是可写的，容器层下面的所有镜像层都是只读的。

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822105546053.png" alt="image-20220822105546053" style="zoom:50%;" />

> 创建镜像命令

1、**docker commit提交容器副本使之成为一个新的镜像**  【生成容器】

`docker commit -m="提交的描述信息" -a="作者" 容器ID 要创建的目标镜像名:[标签名]`

2、发布本地镜像到阿里云镜像以及私有云镜像仓库

+ `docker tag`命令重命名镜像，并将它通过专有网络地址推送至Registry：
  + `docker tag [ImageId] registry.cn-hangzhou.aliyuncs.com/com_th/redis:[镜像版本号]`
+ `docker push`命令将该镜像推送至远程。
  + `docker push registry.cn-hangzhou.aliyuncs.com/com_th/redis:[镜像版本号]`
+ `docker pull `命令拉去镜像
  + `docker pull registry.cn-hangzhou.aliyuncs.com/com_th/redis:[镜像版本号]`

## 五、数据卷

> 前言

Docker挂载数据卷的注意事项：

Docker挂载主机目录访问如果出现**cannot open directory .: Permission denied**异常==》

解决办法：在挂载目录后多加一个--privileged=true参数即可；

如果是CentOS7安全模块会比之前系统版本加强，不安全的会先禁止，所以目录挂载的情况被默认为不安全的行为， 在SELinux里面挂载目录被禁止掉了，如果要开启，我们一般使用`--privileged=true`命令，扩大容器的权限解决挂载目录没有权限的问题，也即使用该参数，`container`内的`root`拥有**真正的root权限**，否则，`container`内的`root`只是外部的一个普通用户权限。

> 数据卷是什么

卷就是**目录或文件**，存在于一个或多个容器中，由docker挂载到容器，但**不属于联合文件系统**，因此能够绕过Union File System提供一些用于持续存储或共享数据的特性; 

卷的设计目的就是**数据的持久化**，完全独立于容器的生存周期，因此Docker不会在容器删除时删除其挂载的数据卷；本质上相当于备份文件；

> 用途

将运用与运行的环境打包镜像，run后形成容器实例运行 ，但是我们对数据的要求希望是持久化的；容器实例删除后，容器内的数据能保存在docker数据卷中。

特点：

+ 数据卷可在容器之间共享或重用数据。

+ 卷中的更改可以直接实时生效（相比于export命令）。

+ 数据卷中的更改不会包含在镜像的更新中。

+ 数据卷的生命周期一直持续到没有容器使用它为止。

> 容器卷案例

1. 宿主机与容器之间隐射添加容器卷

   `docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录 镜像名 /bin/bash`

   容器和宿主机之间数据共享，Docker修改，主机同步获得 ；主机修改，Docker同步获得；Docker容器stop，主机修改，docker容器重启据依旧同步。

   查看数据卷是否挂载成功可以使用`docker inspect容器ID`查看`Mounts`字段；

2. 读写规则映射

   `		docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:rw 镜像名`表示容器可以对数据卷进行读写操作，读写是默认的，":rw"可以不写；

   `docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:ro 镜像名`表示容器只能读取数据卷中的数据；宿主机写入内容，可以同步给容器内；

3. 卷的继承与共享

   `docker run -it --privileged=true -v /mydocker/u:/tmp --name u1 ubuntu`    父容器配置数据卷；

   **`docker run -it --privileged=true --volumes-from 父类 --name u2 ubuntu`**  子容器继承父容器；

## 六、[Dockerfile](https://docs.docker.com/engine/reference/builder/)解析

**Dockerfile**是用来构建Docker镜像的**文本文件**，是由一条条构建镜像所需的**指令**和**参数**构成的脚本。

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822154429586.png" alt="image-20220822154429586" style="zoom:67%;" />

构建镜像三步骤：

+ 编写Dockerfile文件

+ docker build 命令构建镜像

+ docker run 镜像运行容器实例

> **Dockerfile构建过程解析**
>
> > Dockerfile内容基础知识：
> >
> > > + 每条保留字指令都必须为**大写字母**且后面要跟随至少一个参数;
> > > + 指令按照**从上到下，顺序执行**;
> > > + **#**表示注释;
> > > + 每条指令都会创建一个新的镜像层并对镜像进行提交;
> >
> > Docker执行Dockerfile的大致流程：
> >
> > > + docker从基础镜像运行一个容器；
> > > + 执行一条指令并对容器作出修改；
> > > + 执行类似docker commit的操作提交一个新的镜像层；
> > > + docker再基于刚提交的镜像运行一个新容器；
> > > + 执行dockerfile中的下一条指令直到所有指令都执行完成；



> **DockerFile常用保留字指令**

+ `FROM`：基础镜像，当前新镜像是基于哪个镜像的，指定一个已经存在的镜像作为模板，第一条必须是FROM；

+ `MAINTAINER`：镜像维护者的姓名和邮箱地址；

+ `WORKDIR`：指定在创建容器后，终端默认登陆的进来工作目录，一个落脚点；

+ `USER`：指定该镜像以什么样的用户去执行，如果都不指定，默认是root；

+ `ENV`：用来在构建镜像过程中设置环境变量

+ `VOLUME`：容器数据卷，用于数据保存和持久化工作

+ `ADD`：将宿主机目录下的文件拷贝进镜像且会自动处理URL和解压tar压缩包；copy+解压

+ `COPY`：类似ADD，拷贝文件和目录到镜像中。 将从构建上下文目录中 <源路径> 的文件/目录复制到新的一层的镜像内的 <目标路径> 位置

  + COPY src dest                <src源路径>：源文件或者源目录
  + COPY ["src", "dest"]      <dest目标路径>：容器内的指定路径，该路径不用事先建好，路径不存在的话，会自动创建。

+ `EXPOSE`：当前容器对外暴露出的端口；

+ `RUN`：容器构建时需要运行的命令

  + shell格式   eg：`RUN yum -y install vim`

    ![image-20220822165224800](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822165224800.png)

  +  exec格式

    ![image-20220822165251462](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822165251462.png)

  + **RUN是在` docker build`时运行**

+ `CMD`：指定容器启动后的要干的事情

  ![image-20220822165441437](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220822165441437.png)

  + Dockerfile 中可以有多个 CMD 指令，但只有最后一个生效，CMD 会被 `docker run(命令行启动容器时) `之后的参数替换；
  + **CMD是在docker run 时运行。**

+ `ENTRYPOINT`：也是用来指定一个容器启动时要运行的命令

  + 类似于 CMD 指令，但是ENTRYPOINT不会被`docker run`后面的命令覆盖， 而且这些命令行参数会被当作参数送给 ENTRYPOINT 指令指定的程序；
  + ENTRYPOINT可以和CMD一起用，一般是变参才会使用 CMD ，这里的 **CMD 等于是在给 ENTRYPOINT 传参**。
  + 优点：在执行docker run的时候可以指定 ENTRYPOINT 运行所需的参数。
  + 如果 Dockerfile 中如果存在多个 ENTRYPOINT 指令，仅**最后一个生效**。

  

> 案例一    CentOS 安装Vim ifconfig命令以及添加本地jdk，安装jdk；

```java
# Dockerfile
FROM centos
MAINTAINER zzyy<zzyybs@126.com>
 
ENV MYPATH /usr/local
WORKDIR $MYPATH
 
#安装vim编辑器
RUN yum -y install vim
#安装ifconfig命令查看网络IP
RUN yum -y install net-tools
#安装java8及lib库
RUN yum -y install glibc.i686
RUN mkdir /usr/local/java
#ADD 是相对路径jar,把jdk-8u171-linux-x64.tar.gz添加到容器中,安装包必须要和Dockerfile文件在同一位置
ADD jdk-8u171-linux-x64.tar.gz /usr/local/java/
#配置java环境变量
ENV JAVA_HOME /usr/local/java/jdk1.8.0_171
ENV JRE_HOME $JAVA_HOME/jre
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
ENV PATH $JAVA_HOME/bin:$PATH
 
EXPOSE 80
 
CMD echo $MYPATH
CMD echo "success--------------ok"
CMD /bin/bash
```

> 案例二  微服务 多jar包

```java
# Dockerfile
FROM openjdk:11
ADD app-1.0.0.jar app-1.0.0.jar
ADD goods-1.0-0.jar goods-1.0-0.jar
ADD order-1.0-0.jar order-1.0-0.jar
ADD scenic-1.0-0.jar scenic-1.0-0.jar
ADD strategy-1.0-0.jar strategy-1.0-0.jar
ADD system-1.0.0.jar system-1.0.0.jar
ADD user-1.0-0.jar user-1.0-0.jar
ADD user-gateway-1.0.0.jar user-gateway-1.0.0.jar
ADD gateway-1.0.0.jar gateway-1.0.0.jar
ADD hotel-1.0-0.jar hotel-1.0-0.jar
ADD start.sh start.sh
ENTRYPOINT ["sh","-c","./start.sh"]    
```

```sh
#!/bin/bash
nohup java -jar app-1.0.0.jar &
nohup java -jar goods-1.0-0.jar &
nohup java -jar order-1.0-0.jar &
nohup java -jar scenic-1.0-0.jar &
nohup java -jar strategy-1.0-0.jar &
nohup java -jar system-1.0.0.jar &
nohup java -jar user-1.0-0.jar &
nohup java -jar user-gateway-1.0.0.jar &
nohup java -jar gateway-1.0.0.jar &
nohup java -jar hotel-1.0-0.jar &

while [[ true ]]; do
    sleep 1
done
```



## 七、Docker网络

`docker network [COMMAND]`查看并管理Docker网络；

```bash
Commands:
  connect     Connect a container to a network
  create      Create a network
  disconnect  Disconnect a container from a network
  inspect     Display detailed information on one or more networks
  ls          List networks
  prune       Remove all unused networks
  rm          Remove one or more networks
```

> Docker 网络情况

+ 未启动状态

  <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823094803047.png" alt="image-20220823094803047" style="zoom:60%;" />

+ 启动状态

  **会产生一个名为docker0的虚拟网桥**

  <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823094847030.png" alt="image-20220823094847030" style="zoom:60%;" />

> Docker网络能做什么

+ 容器间的互联和通信以及端口映射

+ 容器IP变动时候可以通过服务名直接网络通信而不受到影响

> 网络模式

![image-20220823095336798](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823095336798.png)

+ bridge模式：使用`--network bridge`指定，默认使用docker0

  + Docker 服务默认会创建一个 **`docker0 `**网桥（其上有一个 `docker0 `内部接口），该桥接网络的名称为`docker0`，它在内核层连通了其他的物理或虚拟网卡，这就将所有容器和本地主机都放到**同一个物理网络**。Docker 默认指定了` docker0 `接口 的 IP 地址和子网掩码，让主机和容器之间可以通过网桥相互通信。

  + `docker run `的时候，没有指定`network`的话默认使用的网桥模式就是`bridge`，使用的就是`docker0`。

  + 网桥`docker0`创建一对对等虚拟设备接口一个叫`veth`，另一个叫`eth0`，成对匹配。每个容器实例内部也有一块网卡，每个接口叫eth0；

  + 将宿主机上的所有容器都连接到这个内部网络上，两个容器在同一个网络下,会从这个网关下各自拿到分配的ip，此时两个容器的网络是互通的。

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823101926265.png" alt="image-20220823101926265" style="zoom:70%;" />

  + 案例：

    ```bash
    # 启动两个tomcat容器
    docker run -d -p 8081:8080   --name tomcat81 billygoo/tomcat8-jdk8
    docker run -d -p 8082:8080   --name tomcat82 billygoo/tomcat8-jdk8
    ```

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823102110472.png" alt="image-20220823102110472" style="zoom:80%;" />

+ host模式：使用`--network host`指定

  + 容器将不会获得一个独立的Network Namespace， 而是和宿主机共用一个Network Namespace。容器将不会虚拟出自己的网卡而是使用宿主机的IP和端口。

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823102741517.png" alt="image-20220823102741517" style="zoom:80%;" />

  + ```jade
    # 使用host模式启动tomcat，因为使用了宿主机的网络，使用不需要设置端口映射；
    docker run -d --network host --name tomcat83 billygoo/tomcat8-jdk8
    ```

  + 没有设置-p的端口映射了，访问tomcat需要直接1使用：`http://宿主机IP:8080/`

    容器共享宿主机网络IP，这样的好处是**外部主机与容器可以直接通信**。

+ none模式：使用`--network none`指定

  + 在none模式下，并不为Docker容器进行任何网络配置。 也就是说，这个Docker容器**没有网卡、IP、路由等信息**，只有一个`lo`，需要我们自己为Docker容器添加网卡、配置IP等。

  + ```bas
    docker run -d -p 8084:8080 --network none --name tomcat84 billygoo/tomcat8-jdk8
    ```

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823104209422.png" alt="image-20220823104209422" style="zoom:70%;" />

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823104242613.png" alt="image-20220823104242613" style="zoom:70%;" />

+ container模式：使用`--network container:NAME或者容器ID`指定

  + 新建的容器和已经存在的一个容器共享一个网络ip配置而不是和宿主机共享。新创建的容器不会创建自己的网卡，配置自己的IP，而是和一个指定的容器共享IP、端口范围等。同样，两个容器除了网络方面，其他的如文件系统、进程列表等还是隔离的。

    <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823104426333.png" alt="image-20220823104426333" style="zoom:60%;" />

  + <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823105206099.png" alt="image-20220823105206099" style="zoom:80%;" />

  + alpine1关闭后alpine2也就只有一个`lo`了。

+ 自定义网络

  + 使用默认的bridge方式创建的多个容器直接可以通过**IP地址**`ping`通，但是无法使用**服务名**`ping`通，但是IP地址是会发生改变的；

  + 步骤：

    + docker network create 网络名

    + 新建容器加入上一步新建的自定义网络

      ```bash
      docker run -d -p 8081:8080 --network zzyy_network  --name tomcat81 billygoo/tomcat8-jdk8
      docker run -d -p 8082:8080 --network zzyy_network  --name tomcat82 billygoo/tomcat8-jdk8
      ```

    + 互相ping测试

      <img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823110716116.png" alt="image-20220823110716116" style="zoom:80%;" />

    + **自定义网络本身就维护好了主机名和ip的对应关系（ip和域名都能通）**



## 八、Compose容器编排

> 介绍

**Docker-Compose**是Docker官方的开源项目， 负责实现对Docker**容器集群的快速编排**。

如果同时部署好多个服务,每个服务单独写Dockerfile然后再构建镜像，构建容器，这样无疑是增加了工作量。所以docker官方给我们提供了docker-compose多服务部署的工具。

可以实现管理多个 Docker 容器组成一个应用。你需要定义一个 **YAML** 格式的配置文件`docker-compose.yml`，**写好多个容器之间的调用关系，定义一组相关联的应用容器为一个项目（project）。然后，只要一个命令，就能同时启动/关闭这些容器**。

> 安装

[文档](https://docs.docker.com/compose/compose-file/compose-file-v3/)    [安装](https://docs.docker.com/compose/install/)   

详见官网...

>  Compose使用的三个步骤

+ 编写Dockerfile定义各个微服务应用并构建出对应的镜像文件

+ 使用 docker-compose.yml 定义一个完整业务单元，安排好整体应用中的各个容器服务。

+ 最后，执行docker-compose up命令 来启动并运行整个应用程序，完成一键部署上线

>  Compose常用命令

| 命令                                 | 含义                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| docker-compose -h                    | 查看帮助                                                     |
| docker-compose up                    | 启动所有docker-compose服务                                   |
| docker-compose up -d                 | 启动所有docker-compose服务并后台运行                         |
| docker-compose down                  | 停止并删除容器、网络、卷、镜像。                             |
| docker-compose exec  yml里面的服务id | 进入容器实例内部 docker-compose exec docker-compose.yml文件中写的服务id /bin/bash |
| docker-compose ps                    | 展示当前docker-compose编排过的运行的所有容器                 |
| docker-compose top                   | 展示当前docker-compose编排过的容器进程                       |
| docker-compose logs  yml里面的服务id | 查看容器输出日志                                             |
| docker-compose config                | 检查配置                                                     |
| docker-compose config -q             | 检查配置，有问题才有输出 （当前目录下存在`docker-compose.yml`文件） |
| docker-compose restart               | 重启服务                                                     |
| docker-compose start                 | 启动服务                                                     |
| docker-compose stop                  | 停止服务                                                     |

> 使用步骤

1. 拥有一套个微服务jar包；

2. 编写`docker-compose.yml`文件

   ```yaml
   version: "3"
    
   services:
     microService:                 # 设置的微服务名
       image: zzyy_docker:1.6      # 镜像名
       container_name: ms01        # 容器名（显示设置的话docker就会用这个名字，未设置的话docker将会使用当前jar路径下目                                   录_微服务名作为容器名）
       ports:                      # 端口映射
         - "6001:6001"
       volumes:                    # 容器卷配置
         - /app/microService:/data
       networks:                   # 网络配置
         - atguigu_net 
       depends_on:                 # 依赖于以下微服务
         - redis
         - mysql
    
     redis:                        # 设置的微服务名
       image: redis:6.0.8          # 镜像名
       ports:
         - "6379:6379"
       volumes:
         - /app/redis/redis.conf:/etc/redis/redis.conf
         - /app/redis/data:/data
       networks: 
         - atguigu_net
       command: redis-server /etc/redis/redis.conf     # 按照该目录下的redis.conf文件启动
    
     mysql:
       image: mysql:5.7
       environment:                                    # 变量传递
         MYSQL_ROOT_PASSWORD: '123456'
         MYSQL_ALLOW_EMPTY_PASSWORD: 'no'
         MYSQL_DATABASE: 'db2021'
         MYSQL_USER: 'zzyy'
         MYSQL_PASSWORD: 'zzyy123'
       ports:
          - "3306:3306"
       volumes:
          - /app/mysql/db:/var/lib/mysql
          - /app/mysql/conf/my.cnf:/etc/my.cnf
          - /app/mysql/init:/docker-entrypoint-initdb.d
       networks:
         - atguigu_net
       command: --default-authentication-plugin=mysql_native_password   #解决外部无法访问
    
   networks:                # 新建网络模式，实现按服务访问而不是IP地址                                           
      atguigu_net: 
   ```

3. 修改工程中的配置文件（非必须）

   在例如使用了redis或者MySQL中，需要配置账号密码等；尤其是MySQL的`url`参数可以使用服务名去调用，而不是继续使用IP地址；

   ```yaml
   spring.datasource.url=jdbc:mysql://mysql:3306/db2021?useUnicode=true&characterEncoding=utf-8&useSSL=false
   ```

   如上`mysql`字段即为第二步设置的MySQL的微服务名。当然如果MySQL服务器的IP地址不会随意发生改变，使用IP地址也可；Redis相关主机配置也类似；

4. 将`docker-compose.yml`文件与最新的服务jar包上传至服务器统一目录下；

5. 编写Dockerfile、按照构建镜像步骤构造出微服务镜像；

6. 执行 docker-compose up 或者 执行 docker-compose up -d；   (在使用该命令前可以使用`docker-compose config -q`检查配置)

   ![image-20220823150638347](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823150638347.png)

7. 其他前置操作，如进入MySQL容器内部创建数据库数据表等......

8. 项目运行。

9. 一键关闭项目

   ```bash
   docker-compose stop
   ```



## 九、CIG监控

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823153012793.png" alt="image-20220823153012793" style="zoom:67%;" />

**CAdvisor 监控收集+InfluxDB 存储数据+Granfana 展示图表**

> 步骤

1. 使用`compose`容器编排实现CIG

   ```yaml
   version: '3.1'
    
   volumes:
     grafana_data: {}
    
   services:
    influxdb:
     image: tutum/influxdb:0.9
     restart: always
     environment:
       - PRE_CREATE_DB=cadvisor
     ports:
       - "8083:8083"
       - "8086:8086"
     volumes:
       - ./data/influxdb:/data
    
    cadvisor:
     image: google/cadvisor
     links:
       - influxdb:influxsrv
     command: -storage_driver=influxdb -storage_driver_db=cadvisor -storage_driver_host=influxsrv:8086
     restart: always
     ports:
       - "8080:8080"
     volumes:
       - /:/rootfs:ro
       - /var/run:/var/run:rw
       - /sys:/sys:ro
       - /var/lib/docker/:/var/lib/docker:ro
    
    grafana:
     user: "104"
     image: grafana/grafana
     user: "104"
     restart: always
     links:
       - influxdb:influxsrv
     ports:
       - "3000:3000"
     volumes:
       - grafana_data:/var/lib/grafana
     environment:
       - HTTP_USER=admin
       - HTTP_PASS=admin
       - INFLUXDB_HOST=influxsrv
       - INFLUXDB_PORT=8086
       - INFLUXDB_NAME=cadvisor
       - INFLUXDB_USER=root
       - INFLUXDB_PASS=root
   ```

2. 启动`docker-compose`文件;  `docker-compose up`

3. 测试

   1. 浏览cAdvisor收集服务，http://ip:8080/
   2. 浏览influxdb存储服务，http://ip:8083/
   3. 浏览grafana展现服务，http://ip:3000

4. grafana配置数据源



## 十、Docker平台架构

Docker 是一个 C/S 模式的架构，后端是一个松耦合架构，众多模块各司其职。

1. 用户是使用 Docker Client 与 Docker Daemon 建立通信，并发送请求给后者。

2. Docker Daemon 作为 Docker 架构中的主体部分，首先提供 Docker Server 的功能使其可以接受 Docker Client 的请求。

3. Docker Engine 执行 Docker 内部的一系列工作，每一项工作都是以一个 Job 的形式的存在。

4. Job 的运行过程中，当需要容器镜像时，则从 Docker Registry 中下载镜像，并通过镜像管理驱动 Graph driver将下载镜像以Graph的形式存储。

5. 当需要为 Docker 创建网络环境时，通过网络管理驱动 Network driver 创建并配置 Docker 容器网络环境。

6. 当需要限制 Docker 容器运行资源或执行用户指令等操作时，则通过 Execdriver 来完成。

7. Libcontainer是一项独立的容器管理包，Network driver以及Exec driver都是通过Libcontainer来实现具体对容器进行的操作。

   ![image-20220823111010231](E:\IDEA_Projects\JavaTechnologyCollection\Docker\note\Docker.assets\image-20220823111010231.png)



**更多学习**

1. Docker轻量级可视化工具Portainer。 [官网](https://www.portainer.io/)  [文档](https://docs.portainer.io/start/install/server/docker/linux)
1. Docker复杂安装MySQL、Redis集群；