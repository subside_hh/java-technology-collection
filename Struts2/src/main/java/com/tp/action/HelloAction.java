package com.tp.action;

import com.opensymphony.xwork2.Action;

/**
 * @date: 2023-08-11 7:21
 * @author: ThreePure
 * @description: Struts2 Hello world
 */
public class HelloAction implements Action {

    /**
     * Struts2 默认执行的方法
     * @return String.class
     * @throws Exception
     */
    @Override
    public String execute() throws Exception {
        System.out.println("Hello World Struts2");
        return SUCCESS;
    }
}
