package com.th.stream;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author ThreePure
 * @createDate 2022/11/21
 * @description 主方法
 */
public class StreamMain {

    private static List<Sku> cartSkuList = CartService.getCartSkuList();

    @Test
    public void show() {
        List<Integer> collect = cartSkuList.stream()
                // 方法引用
                .map(Sku::getSkuId)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        collect.stream().forEach(skuId -> {
            System.out.println(skuId.toString());
        });
    }

    @Test
    public void sortTest() {
        cartSkuList.sort(new Comparator<Sku>() {
            @Override
            public int compare(Sku o1, Sku o2) {
                //从大到小
                if (o1.getTotalPrice() < o2.getTotalPrice()) {
                    return 1;
                }
                if (o1.getTotalPrice() > o2.getTotalPrice()) {
                    return -1;
                }
                return 0;
            }
        });

        cartSkuList.forEach((item) -> System.out.println(item.getSkuName() + ":" + item.getTotalPrice()));
    }

    @Test
    public void streamWay() {
        AtomicReference<Double> money = new AtomicReference<>(0.0);

        List<String> resultSkuNameList = CartService.getCartSkuList()
                        // 获取集合流
                        .stream()
                        //1. 打印商品信息*/
                        .peek(sku ->
                                System.out.println(JSON.toJSONString(sku)))
                        //2. 过滤掉所有的图书类商品*/
                        .filter(sku ->
                                !SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory()))
                        //3. 价格进行排序，默认是从小到大，调用 reversed 进行翻转排序即从大到小*/
                        .sorted(Comparator.comparing(Sku::getTotalPrice).reversed())
                        //4. 取 top2*/
                        .limit(2)
                        //累加金额*/
                        .peek(sku -> money.set(money.get() +sku.getTotalPrice()))
                        //获取商品名称*/
                        .map(Sku::getSkuName)
                        .collect(Collectors.toList());
        System.out.println("商品总价:" + money.get());
        System.out.println("商品名列表:" + JSON.toJSONString(resultSkuNameList));
    }



}
