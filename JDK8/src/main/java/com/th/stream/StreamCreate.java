package com.th.stream;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author ThreePure
 * @createDate 2022/11/22
 * @description 流的创建
 */
public class StreamCreate {

    /**
     * 由值创建流
     */
    @Test
    public void streamFromValue() {
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        integerStream.forEach(System.out::println);
    }

    /**
     * 由数组创建流
     */
    @Test
    public void streamFromArray() {
        int[] numbers = {1, 2, 3, 4, 5};
        IntStream stream = Arrays.stream(numbers);
        stream.forEach(System.out::println);
    }

    /**
     * 由文件创建流
     *
     * @throws IOException
     */
    @Test
    public void streamFromFile() throws IOException {
        // 返回指定路径下文件中每一行够成的字符串流
        Stream<String> stream =
                Files.lines(Paths.get("src/main/java/stream/package- info.java"));
        stream.forEach(System.out::println);
    }

    /**
     * 由函数生成流（无限流）
     */
    @Test
    public void streamFromFunction() {
        Stream<Double> generate = Stream.generate(Math::random);
        // 通过 limit 限制数量
        //由函数生成的流为无限流，也就是如果我们没有做其他的限制（如：指定 limit），对其做迭代操作，会一直运行
        generate.limit(10)
                .forEach(System.out::println);
    }

}
