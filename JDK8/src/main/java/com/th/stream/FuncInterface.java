package com.th.stream;

/**
 * @author ThreePure
 * @createDate 2022/11/22
 * @description 函数式接口
 *方法引用的格式
 * 格式: 目标引用 双冒号分隔符 方法名
 * eg: String :: valueO
 */
@FunctionalInterface
public interface FuncInterface {

    abstract void print2Cons();

}
