package com.th.stream;

import com.alibaba.fastjson.JSON;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ThreePure
 * @createDate 2022/11/22
 * @description 流的相关操作 <br>
 * 中间操作(无状态)  ---不需要建立在所有的数据上，只需要针对单个数据进行判断即可 <br>
 * 过滤 filter <br>
 * 映射 map   <br>
 * 扁平化 flatMap <br>
 * 遍历 peek <br>
 * 中间操作(有状态)  ---需要针对所有数据基础上 <br>
 * 去重 distinct <br>
 * 跳过 skip <br>
 * 截断 limit <br>
 * 排序 sorted <br>
 * 终端操作(非短路) ---所有数据均执行 <br>
 * 遍历 forEach <br>
 * 规约 reduce   将 Stream 流中元素转换成一个值 <br>
 * 最大值 max <br>
 * 聚合 collect <br>
 * 最小值 min <br>
 * 计数 count <br>
 * 终端操作(短路)  ---找到符合条件的数据就不再继续执行了 <br>
 * 所有匹配 allMatch <br>
 * 任意匹配 anyMatch <br>
 * 不匹配 noneMatch <br>
 * 查找首个 findFirst <br>
 * 查找任意 findAny <br>
 */
public class StreamOperation {

    List<Sku> list;

    @Before
    public void init() {
        list = CartService.getCartSkuList();
    }

    /**
     * filter 使用：过滤不符合断言判断的数据
     */
    @Test
    public void filterTest() {
        list.stream()
                // 判断是否符合一个断言（商品类型是否为书籍），不符合则过滤元素
                .filter(sku ->
                        SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory()))
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * map 使用：将一个元素转换为另一个元素
     */
    @Test
    public void mapTest() {
        list.stream()
                // 将一个元素转换为另一个数据类型的元素
                .map(Sku::getSkuName)
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * flatMap（扁平化）使用：将一个对象转换为一个流的操作
     */
    @Test
    public void flatMap() {
        list.stream()
                // 这里将商品名称切分为一个字符流，最终输出
                .flatMap(sku ->
                        Arrays.stream(sku.getSkuName().split("")))
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * peek: 对流中元素进行遍历操作，与 forEach 类似，但不会销毁流元素
     */
    @Test
    public void peekTest() {
        list.stream()
                // 对一个流中的所有元素进行处理，但与 forEach 不同之处在于，peek 是一个中间操作，操作完成还能被后续使用。forEach 是一个终端操作，处理完之后流就不能被操作了。
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * sort：对流中元素进行排序，可选择自然排序或指定排序规则
     */
    @Test
    public void sortTest() {
        list.stream()
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                // 根据 Sku 的价格进行升序排列
                .sorted(Comparator.comparing(Sku::getTotalPrice))
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * distinct：对流元素进行去重，有状态操作（针对所有元素进行排序）
     */
    @Test
    public void distinctTest() {
        list.stream()
                .map(Sku::getSkuCategory)
                .distinct()
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * skip：跳过前 N 条记录，有状态操作
     */
    @Test
    public void skipTest() {
        list.stream()
                // 对商品的价格进行排序，跳过前三个（从小到大排序，跳过最小的三个）
                .sorted(Comparator.comparing(Sku::getTotalPrice))
                // 对价格排序之后跳过前三条
                .skip(3)
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * limit：截断前 N 条记录，有状态操作
     */
    @Test
    public void limitTests() {
        list.stream()
                // 对商品价格进行排序，取前三个（从小到大，取 top3）
                .sorted(Comparator.comparing(Sku::getTotalPrice))
                // 对价格排序之后取前三条
                .limit(3)
                // 打印终端操作结果
                .forEach(item ->
                        System.out.println(JSON.toJSONString(item, true)));
    }

    /**
     * allMatch: 检测所有元素是否满足断言，如果都满足返回 true，有一个不满足返回false
     */
    @Test
    public void allMatchTest() {
        boolean isMatch = list.stream()
                // 打印出部门商品名称即结束，参考打印结果
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                // allMatch 是短路操作  匹配到价格小于100的就会退出
                .allMatch(sku -> sku.getTotalPrice() > 100);
        System.out.println(isMatch);
    }

    /**
     * anyMatch: 只有有元素满足断言判断就返回 true，否则返回 false (存在至少一个满足条件的元素即是 true)
     */
    @Test
    public void anyMatchTest() {
        boolean isMatch = list.stream()
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                // 匹配到价格小于100的就会退出
                .anyMatch(sku -> sku.getTotalPrice() > 100);
        System.out.println(isMatch);
    }

    /**
     * noneMatch: 只有所有元素都不满足断言判断才返回 true，否则返回 false
     */
    @Test
    public void noneMatchTest() {
        boolean isMatch = list.stream()
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                .noneMatch(sku -> sku.getTotalPrice() > 10_000);
        System.out.println(isMatch);
    }

    /**
     * findFirst：找到第一个元素
     */
    @Test
    public void findFirstTest() {
        Optional<Sku> first = list.stream()
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                .findFirst();
        System.out.println(JSON.toJSONString(first.get()));
    }

    /**
     * findAny：找到任意一个元素，只要有元素就返回
     */
    @Test
    public void findAnyTest() {
        Optional<Sku> optional = list.stream()
                .peek(sku ->
                        System.out.println(sku.getSkuName()))
                .findAny();
        System.out.println(JSON.toJSONString(optional.get()));
    }

    /**
     * max:获取流中某条件下最大值
     */
    @Test
    public void maxTest() {
        OptionalDouble optionalDouble = list.stream()
                // 提取出价格 数据类型为 double mapToDouble 将元素映射为 double 元素，返回一个 doubleStream 流
                .mapToDouble(Sku::getTotalPrice)
                // 提取最大值
                .max();
        System.out.println(optionalDouble.getAsDouble());
    }

    /**
     * min：获取流中某条件下最小值
     */
    @Test
    public void minTest() {
        OptionalDouble optionalDouble = list.stream()
                // 提取出价格 数据类型为 double mapToDouble 将元素映射为 double 元素，返回一个 doubleStream 流
                .mapToDouble(Sku::getTotalPrice)
                // 提取最小值
                .min();
        System.out.println(optionalDouble.getAsDouble());
    }

    /**
     * count：获取流中元素的个数
     */
    @Test
    public void countTest() {
        long count = list.stream()
                // 提取出价格 数据类型为 double mapToDouble 将元素映射为 double 元素，返回一个 doubleStream 流
                .mapToDouble(Sku::getTotalPrice)
                // 提取总数 不会去重
                .count();
        System.out.println(count);
    }

    /**
     * collect :收集器（Collector）的作用为：将流中元素累积成一个结果作用于终端操作 collect()上关于收集器
     * [集合收集器]
     */
    @Test
    public void toList() {
        List<Sku> list = CartService.getCartSkuList();
        List<Sku> result = list.stream()
                .filter(sku -> sku.getTotalPrice() > 100)
                .collect(Collectors.toList());
        System.out.println(JSON.toJSONString(result, true));
    }

    /**
     * [集合分组]
     */
    @Test
    public void group() {
        List<Sku> list = CartService.getCartSkuList();
        // key=分组条件 value=元素集合 即 Map<分组条件，结果集合>
        Map<Enum, List<Sku>> result = list.stream()
                .collect(Collectors.groupingBy(Sku::getSkuCategory));
        System.out.println(JSON.toJSONString(result, true));
    }

    /**
     * [集合分区]
     * 集合分区是分组的一种特例：分区是由一个谓词作为分区函数，分区函数返回一个
     * boolean 值最终将分区结果分为两组，一组为 boolean=true 的 一组为 boolean=false
     * 的通俗的说也就是满足条件的分为一组，不满足条件的为一组
     */
    @Test
    public void partition() {
        List<Sku> list = CartService.getCartSkuList();
        Map<Boolean, List<Sku>> partition = list.stream()
                .collect(Collectors.partitioningBy(sku ->
                        sku.getTotalPrice() > 100));
        System.out.println(JSON.toJSONString(partition, true));
    }

    /**
     * 【规约】 <br>
     * Optional<T> reduce(BinaryOperator<T> accumulator);<br>
     * 对Stream中的数据通过累加器accumulator迭代计算，最终得到一个Optional对象
     */
    @Test
    public void reduceTest() {
        Optional accResult = Stream.of(1, 2, 3, 4)
                .reduce((acc, item) -> {
                    System.out.println("acc : " + acc);
                    acc += item;
                    System.out.println("item: " + item);
                    System.out.println("acc+ : " + acc);
                    System.out.println("--------");
                    return acc;
                });
        System.out.println(accResult);
    }

    /**
     * 【规约】 <br>
     * T reduce(T identity, BinaryOperator<T> accumulator);<br>
     * 提供一个跟Stream中数据同类型的初始值identity，通过累加器accumulator迭代计算Stream中的数据，得到一个跟Stream中数据相同类型的最终结果，
     */
    @Test
    public void reduceTest1() {
        int accResult = Stream.of(1, 2, 3, 4)
                .reduce(100, (acc, item) -> {
                    System.out.println("acc : "  + acc);
                    acc += item;
                    System.out.println("item: " + item);
                    System.out.println("acc+ : "  + acc);
                    System.out.println("--------");
                    return acc;
                });
        System.out.println(accResult);
    }

    /**
     * 【规约】 <br>
     * <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator</U> combiner);
     * <br>
     * 提供一个不同于Stream中数据类型的初始值，通过累加器规则迭代计算Stream中的数据，最终得到一个同初始值同类型的结果
     */
    @Test
    public void reduceTest2() {
        ArrayList<Integer> accResult_ = Stream.of(2, 3, 4)
                .reduce(Lists.newArrayList(1),
                        (acc, item) -> {
                            acc.add(item);
                            System.out.println("item: " + item);
                            System.out.println("acc+ : " + acc);
                            System.out.println("BiFunction");
                            return acc;
                        }, (acc, item) -> {
                            System.out.println("BinaryOperator");
                            acc.addAll(item);
                            System.out.println("item: " + item);
                            System.out.println("acc+ : " + acc);
                            System.out.println("--------");
                            return acc;
                        }
                );
        System.out.println("accResult_: " + accResult_);
    }


}
