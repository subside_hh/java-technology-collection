package com.th;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.MaxAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

/**
 * @author ThreePure
 * @createDate 2022/12/7
 * @description 高级搜索
 */
public class EsDocAdvancedSearch {

    /**
     * 查询索引所有数据 【search() <- SearchRequest <- SearchSourceBuilder <- QueryBuilders】
     */
    @Test
    public void testSearchAllRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询所有数据
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * term 查询，查询条件为关键字 【search() <- SearchRequest <- SearchSourceBuilder <- QueryBuilders】
     * @throws IOException
     */
    @Test
    public void testSearchTermRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询所有数据
        searchSourceBuilder.query(QueryBuilders.termQuery("name", "zhangsan"));
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 分页查询
     * @throws IOException
     */
    @Test
    public void testSearchPageRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询所有数据
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        // 分页查询
        // 当前页其实索引(第一条数据的顺序号)，from
        searchSourceBuilder.from(0);
        // 每页显示多少条 size
        searchSourceBuilder.size(4);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 数据排序
     * @throws IOException
     */
    @Test
    public void testSearchSortRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询所有数据
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        // 排序 (倒叙)
        searchSourceBuilder.sort("age", SortOrder.DESC);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 过滤字段
     * @throws IOException
     */
    @Test
    public void testSearchFetchRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询所有数据
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        //查询字段过滤
        String[] excludes = {};
        String[] includes = {"age", "sex"};
        searchSourceBuilder.fetchSource(includes, excludes);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * Bool 查询
     * @throws IOException
     */
    @Test
    public void testSearchBoolRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //必须包含
        boolQueryBuilder.must(QueryBuilders.matchQuery("age", "38"));
        //一定不含
        boolQueryBuilder.mustNot(QueryBuilders.matchQuery("name", "zhangsan"));
        //可能包含
        boolQueryBuilder.should(QueryBuilders.matchQuery("sex", "女"));

        searchSourceBuilder.query(boolQueryBuilder);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 范围 查询
     * @throws IOException
     */
    @Test
    public void testSearchRangeRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("age");
        // 大于等于
        rangeQueryBuilder.gte(35);
        // 小于等于
        rangeQueryBuilder.gte(38);

        searchSourceBuilder.query(rangeQueryBuilder);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 模糊 查询
     * @throws IOException
     */
    @Test
    public void testSearchFuzzyRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        FuzzyQueryBuilder fuzziness = QueryBuilders.fuzzyQuery("name", "zhangsan").fuzziness(Fuzziness.ONE);

        searchSourceBuilder.query(fuzziness);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 高亮 查询
     * @throws IOException
     */
    @Test
    public void testSearchHighLighterRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        // 查询
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "zhangsan");
        searchSourceBuilder.query(termQueryBuilder);

        //高亮设置
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.preTags("<font color='red'>");
        highlightBuilder.postTags("</font>");
        highlightBuilder.field("name");
        searchSourceBuilder.highlighter(highlightBuilder);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        System.out.println("Took: " + searchResponse.getTook());
        System.out.println("TimeOut: " + searchResponse.isTimedOut());
        System.out.println("total: " + hits.getTotalHits());
        System.out.println("MaxScore: " + hits.getMaxScore());
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
            //打印高亮结果
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            System.out.println(highlightFields);
        }
    }

    /*==================================聚合查询==================================*/

    /**
     * 聚合 查询（最大值）
     * @throws IOException
     */
    @Test
    public void testSearchMaxRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        MaxAggregationBuilder maxAggregationBuilder = AggregationBuilders.max("maxAge").field("age");
        searchSourceBuilder.aggregation(maxAggregationBuilder);

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        //打印响应结果
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        System.out.println("=============");
        System.out.println("searchSearchResponse" + searchResponse);
    }

    /**
     * 聚合 查询（分组）
     * @throws IOException
     */
    @Test
    public void testSearchGroupRequest() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("user");
        // 构建查询的请求体
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.aggregation(AggregationBuilders.terms("maxAge").field("age"));

        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        //打印响应结果
        SearchHits hits = searchResponse.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        System.out.println("=============");
        System.out.println("searchSearchResponse" + searchResponse);
    }

}
