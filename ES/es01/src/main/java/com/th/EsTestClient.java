package com.th;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

/**
 * @author ThreePure
 * @createDate 2022/12/6
 * @description Es测试类
 */
public class EsTestClient {

    public static void main(String[] args) throws IOException {
        //创建ES连接客户端
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));

        System.out.println("restHighLevelClient: " + restHighLevelClient.toString());
        //关闭连接
        restHighLevelClient.close();
    }

}
