package com.th;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.th.pojo.User;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author ThreePure
 * @createDate 2022/12/6
 * @description ES 文档操作
 */
public class EsDoc {

    /**
     * 创建文档  【index() <- IndexRequest】
     */
    @Test
    public void testCreateDoc() throws IOException {
        // 新增文档 - 请求对象
        IndexRequest indexRequest = new IndexRequest();
        //  设置索引及唯一性标识
        indexRequest.index("user").id("1004");
        //创建数据对象
        User user = new User();
        user.setName("huangba");
        user.setAge(40);
        user.setSex("女");
        ObjectMapper objectMapper = new ObjectMapper();
        String stringUser = objectMapper.writeValueAsString(user);
        System.out.println("stringUser" + stringUser);
        // 添加文档数据，数据格式为 JSON 格式
        indexRequest.source(stringUser, XContentType.JSON);
        // 客户端发送请求，获取响应对象
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println("_index：" + response.getIndex());
        System.out.println("_id：" + response.getId());
        System.out.println("_result：" + response.getResult());
    }

    /**
     * 查询文档 【get() <- GetRequest】
     */
    @Test
    public void testSearchDoc() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        GetRequest getRequest = new GetRequest("user", "1001");
        GetResponse response = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
        System.out.println("index:" + response.getIndex());
        System.out.println("id:" + response.getId());
        System.out.println("type:" + response.getType());
        System.out.println("source:" + response.getSourceAsString());
    }

    /**
     * 更新文档  【update() <- UpdateRequest】
     * @throws IOException
     */
    @Test
    public void testUpdateDoc() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("user").id("1001");
        // 设置请求体，对数据进行修改
        updateRequest.doc(XContentType.JSON,"sex","女");
        UpdateResponse response = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println("_index：" + response.getIndex());
        System.out.println("_id：" + response.getId());
        System.out.println("_result：" + response.getResult());
    }

    /**
     * 删除文档 【delete() <- DeleteRequest】
     * @throws IOException
     */
    @Test
    public void testDeleteDoc() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        DeleteRequest deleteRequest = new DeleteRequest("user", "_doc", "1001");
        DeleteResponse deleteResponse = restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println("index:" + deleteResponse.getIndex());
        System.out.println("id:" + deleteResponse.getId());
        System.out.println("result:" + deleteResponse.getResult());
    }

    /**
     * 批量新增  【bulk() <- BulkRequest】
     */
    @Test
    public void testBulkCreateDoc() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.add(new IndexRequest("user").id("001").source(XContentType.JSON, "name", "zhangsan"));
        bulkRequest.add(new IndexRequest("user").id("002").source(XContentType.JSON, "name", "lisi"));
        bulkRequest.add(new IndexRequest("user").id("003").source(XContentType.JSON, "name", "wangwu"));
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println("Took: " + bulk.getTook());
        System.out.println("Items: " + Arrays.toString(bulk.getItems()));
    }

    /**
     * 批量删除  【bulk() <- BulkRequest】
     * @throws IOException
     */
    @Test
    public void testBulkDeleteDoc() throws IOException {
        RestHighLevelClient restHighLevelClient = EsIndex.getRestHighLevelClient();
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.add(new DeleteRequest("user", "001"));
        bulkRequest.add(new DeleteRequest("user", "002"));
        bulkRequest.add(new DeleteRequest("user", "003"));
        BulkResponse response = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println("Took: " + response.getTook());
        System.out.println("Items: " + response.getItems());
    }

}
