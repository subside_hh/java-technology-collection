package com.th;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.junit.Test;

import java.io.IOException;

/**
 * @author ThreePure
 * @createDate 2022/12/6
 * @description Es中索引相关操作
 */
public class EsIndex {

    public static RestHighLevelClient getRestHighLevelClient() {
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
    }

    /**
     * 创建索引
     */
    @Test
    public void testCreateIndex() throws IOException {
        RestHighLevelClient restHighLevelClient = getRestHighLevelClient();
        CreateIndexRequest indexRequest = new CreateIndexRequest("user");
        CreateIndexResponse createIndexResponse = restHighLevelClient
                .indices()
                .create(indexRequest, RequestOptions.DEFAULT);
        System.out.println("isAcknowledged:" + createIndexResponse.isAcknowledged());
    }

    /**
     * 查询单个索引
     */
    @Test
    public void testSearchIndex() throws IOException {
        RestHighLevelClient restHighLevelClient = getRestHighLevelClient();
        GetIndexRequest getIndexRequest = new GetIndexRequest("user");
        GetIndexResponse indexResponse = restHighLevelClient
                .indices()
                .get(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println("Aliases" + indexResponse.getAliases());
        System.out.println("Aliases" + indexResponse.getMappings());
        System.out.println("Aliases" + indexResponse.getSettings());
    }

    /**
     * 删除索引
     */
    @Test
    public void testDeleteIndex() throws IOException {
        RestHighLevelClient restHighLevelClient = getRestHighLevelClient();
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("user");
        AcknowledgedResponse response = restHighLevelClient.indices()
                .delete(deleteIndexRequest, RequestOptions.DEFAULT);
        System.out.println("isAcknowledged:" + response.isAcknowledged());
    }

}
