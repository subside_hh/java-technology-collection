package com.th.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: SprinBoot项目启动类
 * @author: HuangJiBin
 * @date: 2022/8/8  8:11
 * @since: OpenJDK 11
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
