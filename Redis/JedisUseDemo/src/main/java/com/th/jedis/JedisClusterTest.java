package com.th.jedis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * @description: 集群方式使用Jedis
 * @author: HuangJiBin
 * @date: 2022/8/13  17:24
 * @since: OpenJDK 11
 */
public class JedisClusterTest {
    public static void main(String[] args) {
        Set<HostAndPort> set =new HashSet<HostAndPort>();
        set.add(new HostAndPort("192.168.31.211",6379));
        JedisCluster jedisCluster=new JedisCluster(set);
        jedisCluster.set("k1", "v1");
        System.out.println(jedisCluster.get("k1"));
    }
}
