package com.th.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @description: Redis哨兵模式配置Jedis
 * @author: HuangJiBin
 * @date: 2022/8/13  13:47
 * @since: 1.8
 */
public class JedisSentinel {
    private static JedisSentinelPool jedisSentinelPool=null;

    public static Jedis getJedisFromSentinel(){
        if(jedisSentinelPool==null){
            Set<String> sentinelSet=new HashSet<>();
            /**哨兵的地址与端口*/
            sentinelSet.add("192.168.11.103:26379");

            JedisPoolConfig jedisPoolConfig =new JedisPoolConfig();
            //最大可用连接数
            jedisPoolConfig.setMaxTotal(10);
            //最大闲置连接数
            jedisPoolConfig.setMaxIdle(5);
            //最小闲置连接数
            jedisPoolConfig.setMinIdle(5);
            //连接耗尽是否等待
            jedisPoolConfig.setBlockWhenExhausted(true);
            //等待时间
            jedisPoolConfig.setMaxWaitMillis(2000);
            //取连接的时候进行一下测试 ping pong
            jedisPoolConfig.setTestOnBorrow(true);

            jedisSentinelPool=new JedisSentinelPool("mymaster",sentinelSet,jedisPoolConfig);
            return jedisSentinelPool.getResource();
        }else{
            return jedisSentinelPool.getResource();
        }
    }

}
