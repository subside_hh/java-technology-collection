package com.th.jedis;

import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * @description: 手机验证码
 * @author: HuangJiBin
 * @date: 2022/8/7  22:31
 * @since: OpenJDK 11
 */
public class VerificationCode {
    public static void main(String[] args) {
        vodeToRedis("18370665542");
        //verify("18370665542", "187713");
    }

    /**
     * @description: 校验验证码是否正确
     * @Param: [phone 手机号码, code 验证码]
     * @Return: boolean
     */
    public static boolean verify(String phone, String code){
        Jedis jedis = new Jedis("120.26.223.85", 6379);
        String codeKey = "VerifyCode" + phone + ":code";
        String redisCode = jedis.get(codeKey);
        if (redisCode.equals(code)){
            System.out.println("校验成功");
            jedis.close();
            return true;
        }else {
            System.out.println("校验失败");
            jedis.close();
            return false;
        }
    }


    /**
     * @description: 参数6位随机验证码
     * @Return: java.lang.String
     */
    public static String getCode(){
        Random random = new Random();
        String code = "";
        for (int i = 0; i < 6; i++) {
            int rand = random.nextInt(10);
            code += rand;
        }
        return code;
    }

    /**
     * @description: 将验证码信息存储到redis中
     * @Param: [phone 手机号码, code 验证码]
     * @Return: void
     */
    public static void vodeToRedis(String phone){
        Jedis jedis = new Jedis("120.26.223.85", 6379);
        //拼接存储验证码次数以及验证码本身的Key
        String countKey = "VerifyCode" + phone + ":count";
        String codeKey = "VerifyCode" + phone + ":code";

        //每个手机只能发送三次
        String count = jedis.get(countKey);
        if (count == null){
            jedis.setex(countKey, 24*60*60, "1");
        } else if (Integer.parseInt(count) <=2 ) {
            jedis.incr(countKey);
        } else if (Integer.parseInt(count)>2) {
            System.out.println("今天次数已用尽，请明天再试");
            jedis.close();
            return;
        }

        //将验证码放在redis中
        jedis.setex(codeKey, 120, getCode());
        jedis.close();
    }
}
