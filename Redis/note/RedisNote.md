##  一、常用命令

| 命令              | 含义                                                         |
| ----------------- | ------------------------------------------------------------ |
| `keys *`          | 查看当前库所有key  (匹配：keys *1)                           |
| `exists key`      | 判断某个key是否存在                                          |
| `type key `       | 查看你的key是什么类型                                        |
| `del key   `      | 删除指定的key数据                                            |
| `unlink key  `    | 根据value选择非阻塞删除 （异步删除）仅将keys从`keyspace`元数据中删除，真正的删除会在后续异步操作。 |
| `expire key 10  ` | 为给定的key设置过期时间                                      |
| `ttl key `        | 查看还有多少秒过期，-1表示永不过期，-2表示已过期             |
| `select`          | 命令切换数据库                                               |
| `dbsize`          | 查看当前数据库的key的数量                                    |
| `flushdb`         | 清空当前库                                                   |
|                   | [【更多命令】](http://www.redis.cn/commands.html)            |

**Redis的启动相关命令：**

| 命令                                       | 含义                                     |
| ------------------------------------------ | ---------------------------------------- |
| `ps aux|grep redis`                        | 查看Redis是否在运行                      |
| `redis-cli shutdown`                       | 关闭redis                                |
| `redis-server /redis.conf`                 | redis以配置文件/redis.conf中配置方式启动 |
| `systemctl stop/disable firewalld.service` | 关闭防火墙                               |

## 二、数据类型

### 1、String类型

String是`Redis`最基本的类型，由一个key对应一个value。String类型是**二进制安全**的（图片、视频）。意味着`Redis`的string可以包含任何数据。比如`jpg`图片或者序列化的对象。一个`Redis`中字符串value最多可以是**`512M`**。

| 常用命令                  | 含义                                                         |
| ------------------------- | ------------------------------------------------------------ |
| `set ` <key> <value> 参数 | 添加键值对<br />NX：当数据库中key不存在时，可以将key-value添加数据库<br />XX：当数据库中key存在时，可以将key-value添加数据库，与NX参数互斥<br />EX：key的超时秒数<br />PX：key的超时毫秒数，与EX互斥 |
| `get ` <key>              | 查询对应键值                                                 |
| `append `<key> <value>    | 将给定的<value> 追加到原值的末尾                             |
| `strlen` <key>            | 获得值的长度                                                 |
| `setnx` <key> <value>     | 只有在 key 不存在时  设置 key 的值                           |
| `incr` <key>              | 将 key 中储存的数字值增1,只能对数字值操作，如果为空，新增值为1 |
| `decr` <key>              | 将 key 中储存的数字值减1,只能对数字值操作，如果为空，新增值为-1 |
| `incrby`  <key> <步长>    | 将 key 中储存的数字值增加。自定义步长。                      |
| `decrby`  <key> <步长>    | 将 key 中储存的数字值减少。自定义步长。                      |

**原子性：** 在单线程中， 能够在单条指令中完成的操作都可以认为是"原子操作"，而`Redis`**单命令的原子性主要得益于`Redis`的单线程。**以下命令基于其原子性，多个命令要么全部执行，要么全部不执行：

| 常用命令                                            | 含义                                                         |
| --------------------------------------------------- | ------------------------------------------------------------ |
| `mset` <key1> <value1> <key2> <value2> .....        | 同时设置一个或多个 key-value对                               |
| `mget `<key1><key2><key3> .....                     | 同时获取一个或多个 value                                     |
| `msetnx `<key1><value1><key2><value2> .....         | 同时设置一个或多个 key-value 对，当且仅当所有给定 key 都不存在。 |
| 注意`msetnx`，只要存在一个已存在的key，就会全部失败 |                                                              |
| `getrange` <key> <起始位置> <结束位置>              | 获得值的范围，类似Java中的substring，**前包，后包**          |
| `setrange` <key> <起始位置> <value>                 | 用 <value> 覆写<key>所储存的字符串值，从<起始位置>开始(**索引从0**开始)。 |
| `setex  `   <key> <**过期时间**> <value>            | 设置键值的同时，设置过期时间，单位秒。                       |
| `getset ` <key> <value>                             | 以新换旧，设置了新值同时获得旧值。                           |

**String数据结构:**

String的数据结构为简单动态字符串(Simple Dynamic String,缩写SDS)。是可以修改的字符串，内部结构实现上类似于Java的ArrayList，采用预分配冗余空间的方式来减少内存的频繁分配.

![image-20220724175915194](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220724175915194.png)

如图中所示，内部为当前字符串实际分配的空间capacity一般要高于实际字符串长度len。当字符串长度小于1M时，扩容都是**加倍**现有的空间，如果超过1M，扩容时一次只会多扩1M的空间。需要注意的是字符串最大长度为512M。

### 2、List类型

**List数据结构：**

`Redis` 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）。

它的底层实际是个双向链表，对两端的操作性能很高，通过索引下标的操作中间的节点性能会较差。

![image-20220724215300285](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220724215300285.png)

**常用命令：**

| 常用命令                                            | 含义                                                         |
| --------------------------------------------------- | ------------------------------------------------------------ |
| `lpush`/`rpush` <key> <value1> <value2> <value3> .. | 从左边/右边插入一个或多个值。                                |
| `lpop`/`rpop` <key>                                 | 从左边/右边吐出(取一个少一个)一个值。**值在键在，值光键亡**。 |
| `rpoplpush` <key1> <key2>                           | 从<key1>列表右边吐出一个值，插到<key2>列表左边。             |
| `lrange` <key> <start> <stop>                       | 按照索引下标获得元素(从左到右)<br />`lrange` key 0 -1  0左边第一个，-1右边第一个，（0-1表示获取所有） |
| `lindex` <key> <index>                              | 按照索引下标获得元素(从左到右)                               |
| `llen` <key>                                        | 获得列表长度                                                 |
| `linsert `<key> before <value> <newvalue>           | 在<value>的后面插入<newvalue>插入值                          |
| `lrem` <key> <n> <value>                            | 从左边删除n个value(从左到右)                                 |
| `lset`<key> <index> <value>                         | 将列表key下标为index的值替换成value                          |

### 3、Set类型

Redis set对外提供的功能与list类似是一个列表的功能，特殊之处在于set是可以**自动排重**，同时还是string类型的**无序集合无重复数据**。它底层其实是一个value为null的hash表，所以添加，删除，查找的**复杂度都是**O(1)。

Set数据结构是`dict字典`，字典是用哈希表实现的。Java中`HashSet`的内部实现使用的是`HashMap`，只不过所有的value都指向同一个对象。`Redis`的set结构也是一样，它的内部也使用hash结构，所有的value都指向同一个内部值。

| 常用命令                             | 含义                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| `sadd` <key> <value1> <value2> ..... | 将一个或多个 member 元素加入到集合 key 中，已经存在的 member 元素将被忽略 |
| `smembers` <key>                     | 取出该集合的所有值。                                         |
| `sismember` <key> <value>            | 判断集合<key>是否为含有该<value>值，有1，没有0               |
| `scard` <key>                        | 返回该集合的元素个数。                                       |
| `srem` <key> <value1> <value2> ....  | 删除集合中的某个元素。                                       |
| `spop` <key>                         | **随机从该集合中吐出一个值。**                               |
| `srandmember` <key> <n>              | 随机从该集合中取出n个值。不会从集合中删除 。                 |
| `smove` <source> <destination> value | 把集合中一个值从一个集合移动到另一个集合                     |
| `sinter` <key1> <key2>               | 返回两个集合的交集元素。                                     |
| `sunion` <key1> <key2>               | 返回两个集合的并集元素。                                     |
| `sdiff` <key1> <key2>                | 返回两个集合的**差集**元素(key1中的，不包含key2中的)         |


###  4、Hash

`Redis hash`是一个String类型的 field 和 value 的映射表，hash特别适合用于存储对象。类似Java里面的`Map<String,Object>`,如：用户ID为查找的key，存储的value用户对象包含姓名，年龄，生日等信息，如果用普通的key/value结构来存储：

![image-20220730164132997](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220730164132997.png)

| 常用命令                                              | 含义                                                         |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| `hset` <key> <field> <value>                          | 给<key>集合中的<field>键赋值<value>                          |
| `hget` <key1><field>                                  | 从<key1>集合<field>取出 value                                |
| `hmset `<key1> <field1> <value1> <field2> <value2>... | 批量设置hash的值                                             |
| `hexists` <key1> <field>                              | 查看哈希表 key 中，给定域 field 是否存在。                   |
| `hkeys` <key>                                         | 列出该hash集合的所有field                                    |
| `hvals `<key>                                         | 列出该hash集合的所有value                                    |
| `hincrby `<key> <field> <increment>                   | 为哈希表 key 中的域 field 的值加上增量 1  -1                 |
| `hsetnx `<key> <field> <value>                        | 将哈希表 key中的域 field 的值设置为 value ，当且仅当域 field 不存在 |

**Hash类型**对应的数据结构是两种：`ziplist`（压缩列表），`hashtable`（哈希表）。当field-value长度较短且个数较少时，使用`ziplist`，否则使用`hashtable`。

### 5、Zset有序集合

Redis有序集合zset与普通集合set非常相似，是一个**没有重复元素**的字符串集合。不同之处是有序集合的每个成员都关联了一个**评分**（score）,这个评分（score）被用来按照从最低分到最高分的方式排序集合中的成员。集合的成员是唯一的，但是评分可以是重复了 。

| 常用命令                                                     | 含义                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `zadd` <key> <score1> <value1> <score2> <value2>…            | 将一个或多个 member 元素及其 score 值加入到有序集 key 当中。 |
| **`zrange` <key> <start> <stop> [WITHSCORES]**               | 返回有序集 key 中，下标在<start><stop>之间的元素<br />带WITHSCORES，可以让分数一起和值返回到结果集。 |
| `zrangebyscore` key min max [withscores] [limit offset count] | 返回有序集 key 中，所有 score 值介于 min 和 max 之间(包括等于)的成员。从小到大顺序 |
| `zrevrangebyscore` key max min [withscores] [limit offset count] | 同上，改为从大到小排列。                                     |
| `zincrby` <key> <increment> <value>                          | 为元素的score加上增量                                        |
| `zrem` <key> <value>                                         | 删除该集合下，指定值的元素                                   |
| `zcount` <key> <min> <max>                                   | 统计该集合，分数区间内的元素个数                             |
| `zrank` <key> <value>                                        | 返回该值在集合中的排名，从0开始。                            |

zset底层使用了两个数据结构

（1）hash，hash的作用就是关联元素value和权重score，保障元素value的唯一性，可以通过元素value找到相应的score值。

（2）**跳跃表**，跳跃表的目的在于给元素value排序，根据score的范围获取元素列表。

**跳跃表**

（1）  有序链表

![image-20220730174047792](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220730174047792.png)                               

要查找值为51的元素，需要从第一个元素开始依次查找、比较才能找到。共需要`6次`比较。

（2）  跳跃表

 ![image-20220730174055917](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220730174055917.png)

从第2层开始，1节点比51节点小，向后比较。21节点比51节点小，继续向后比较，后面就是NULL了，所以从21节点向下到第1层

在第1层，41节点比51节点小，继续向后，61节点比51节点大，所以从41向下

在第0层，51节点为要查找的节点，节点被找到，共查找4次。从此可以看出跳跃表比有序链表效率要高



**Resdi新数据类型**

<hr/>

### 6、 BitMaps

 **Bitmaps本身不是一种数据类型， 实际上它就是字符串（key-value） ， 但是它可以对字符串的位进行操作。最佳实例是在大量访问量中统计用户访问量。**

合理地使用操作位能够有效地提高内存使用率和开发效率。 Bitmaps单独提供了一套命令， 所以在Redis中使用Bitmaps和使用字符串的方法不太相同。 可以把Bitmaps想象成一个以位为单位的数组， 数组的每个单元只能存储0和1， 数组的下标在Bitmaps中叫做偏移量。

![image-20220807120217520](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220807120217520.png)

| 常用命令                                     | 含义                                                         |
| -------------------------------------------- | ------------------------------------------------------------ |
| `setbit` <key> <offset> <value>              | 设置Bitmaps中某个偏移量的值（0或1、offset:偏移量从0开始）    |
| `getbit` <key> <offset>                      | 获取Bitmaps中某个偏移量的值                                  |
| `bitcount` <key> [start end]                 | 统计字符串从start字节到end字节比特值为1的数量                |
| `bitop` **and(or/not/xor)** <destkey> [key…] | 多个Bitmaps的and(交集)、or(并集) 、not(非)、 xor(异或) 操作并将结果保存在destkey中 |

### 7、**HyperLogLog**

用于解决要去重和计数的基数问题的解决。解决基数问题有很多种方案：

（1）数据存储在MySQL表中，使用distinct count计算不重复个数

（2）使用Redis提供的hash、set、bitmaps等数据结构来处理

以上的方案结果精确，但随着数据不断增加，导致占用空间越来越大，对于非常大的数据集是不切实际的。能否能够降低一定的精度来平衡存储空间？Redis推出了HyperLogLog来实现。**Redis HyperLogLog 是用来做基数统计的算法**，HyperLogLog 的优点是，在输入元素的数量或者体积非常非常大时，计算基数所需的空间总是固定的、并且是很小的。在 Redis 里面，**每个 HyperLogLog 键只需要花费 12 KB 内存**，就可以计算接近 **2^64 个不同元素的基数**。

**HyperLogLog 只会根据输入元素来计算基数，而不会储存输入元素本身，所以 HyperLogLog 不能像集合那样，返回输入的各个元素。**

> 什么是基数?

比如数据集 {1, 3, 5, 7, 5, 7, 8}， 那么这个数据集的基数集为 {1, 3, 5 ,7, 8}, 基数(不重复元素)为5。 基数估计就是在误差可接受的范围内，快速计算基数。

| 常用命令                                         | 含义                                         |
| ------------------------------------------------ | -------------------------------------------- |
| `pfadd ` <key> < element> [element ...]          | 添加指定元素到 HyperLogLog 中                |
| `pfcount` <key> [key ...]                        | 计算HLL的近似基数、计算多个HLL               |
| `pfmerge` <destkey> <sourcekey>  [sourcekey ...] | 将一个或多个HLL合并后的结果存储在另一个HLL中 |
|                                                  |                                              |

### 8、 **Geospatial**

Redis 3.2 中增加了对**GEO**类型的支持。GEO，**Geographic**，地理信息的缩写。该类型，就是元素的2维坐标，在地图上就是经纬度。redis基于该类型，提供了**经纬度设置，查询，范围查询，距离查询，经纬度Hash**等常见操作。

| 常用命令                                                     | 含义                                       |
| ------------------------------------------------------------ | ------------------------------------------ |
| `geoadd` <key> < longitude> <latitude> <member> [longitude latitude member...]  ） | 添加地理位置（经度，纬度，名称             |
| `geopos` <key><member> [member...]                           | 获得指定地区的坐标值                       |
| `geodist` <key> <member1> <member2>  [m/km/ft/mi]            | 获取两个位置之间的直线距离                 |
| `georadius` <key> < longitude> <latitude>  [m/km/ft/mi]      | 以给定的经纬度为中心，找出某一半径内的元素 |

## 三、配置文件

> 单位

配置大小单位,开头定义了一些基本的度量单位，只支持**bytes**，不支持bit,大小写不敏感

```bash
# Note on units: when memory size is needed, it is possible to specify
# it in the usual form of 1k 5GB 4M and so forth:
#
# 1k => 1000 bytes
# 1kb => 1024 bytes
# 1m => 1000000 bytes
# 1mb => 1024*1024 bytes
# 1g => 1000000000 bytes
# 1gb => 1024*1024*1024 bytes
#
# units are case insensitive so 1GB 1Gb 1gB are all the same.
```

> 包含**INCLUDES**

多实例的情况可以把公用的配置文件提取出来;

```bash
################################## INCLUDES ###################################

# Include one or more other config files here.  This is useful if you
# have a standard template that goes to all Redis servers but also need
# to customize a few per-server settings.  Include files can include
# other files, so use this wisely.
#
# Note that option "include" won't be rewritten by command "CONFIG REWRITE"
# from admin or Redis Sentinel. Since Redis always uses the last processed
# line as value of a configuration directive, you'd better put includes
# at the beginning of this file to avoid overwriting config change at runtime.
#
# If instead you are interested in using includes to override configuration
# options, it is better to use include as the last line.
#
# include /path/to/local.conf
# include /path/to/other.conf
```

> 网络相关

1. bind

   默认情况bind=127.0.0.1只能接受本机的访问请求，不写的情况下，无限制接受任何ip地址的访问；

   生产环境需要邪恶应用服务器的地址；服务器是需要远程访问的，所以需要将其注释掉；

   ```bash
   ################################## NETWORK #####################################
   
   # By default, if no "bind" configuration directive is specified, Redis listens
   # for connections from all available network interfaces on the host machine.
   # It is possible to listen to just one or multiple selected interfaces using
   # the "bind" configuration directive, followed by one or more IP addresses.
   # Each address can be prefixed by "-", which means that redis will not fail to
   # start if the address is not available. Being not available only refers to
   # addresses that does not correspond to any network interfece. Addresses that
   # are already in use will always fail, and unsupported protocols will always BE
   # silently skipped.
   ………………
   bind 127.0.0.1 -::1
   ```

2. **protected- mode**

   将本机访问保护模式设置为no，这样才能被远程主机访问

   ```bash
   # By default protected mode is enabled. You should disable it only if
   # you are sure you want clients from other hosts to connect to Redis
   # even if no authentication is configured, nor a specific set of interfaces
   # are explicitly listed using the "bind" directive.
   protected-mode yes
   ```

3. Port 端口号

   ```bash
   # Accept connections on the specified port, default is 6379 (IANA #815344).
   # If port 0 is specified Redis will not listen on a TCP socket.
   port 6379
   ```

4. cp-backlog

   ​	设置tcp的backlog，backlog其实是一个连接队列，backlog队列总和=未完成三次握手队列 + 已经完成三次握手队列。在高并发环境下你需要一个高backlog值来避免慢客户端连接问题。

   ```bash
   # TCP listen() backlog.
   #
   # In high requests-per-second environments you need a high backlog in order
   # to avoid slow clients connection issues. Note that the Linux kernel
   # will silently truncate it to the value of /proc/sys/net/core/somaxconn so
   # make sure to raise both the value of somaxconn and tcp_max_syn_backlog
   # in order to get the desired effect.
   tcp-backlog 511
   ```

5. **timeout**

   一个空闲的客户端维持多少秒会关闭，0表示关闭该功能。即永不关闭。

   ```bash
   # Close the connection after a client is idle for N seconds (0 to disable)
   timeout 0
   ```

6. **tcp-keepalive**

   对访问客户端的一种心跳检测，每个n秒检测一次。单位为秒，如果设置为0，则不会进行Keepalive检测，建议设置成60;

   ```bash
   # TCP keepalive.
   #
   # If non-zero, use SO_KEEPALIVE to send TCP ACKs to clients in absence
   # of communication. This is useful for two reasons:
   #
   # 1) Detect dead peers.
   # 2) Force network equipment in the middle to consider the connection to be
   #    alive.
   #
   # On Linux, the specified value (in seconds) is the period used to send ACKs.
   # Note that to close the connection the double of the time is needed.
   # On other kernels the period depends on the kernel configuration.
   #
   # A reasonable value for this option is 300 seconds, which is the new
   # Redis default starting with Redis 3.2.1.
   tcp-keepalive 300
   ```

> 通用

1. daemonize 后台启动

   是否为后台进程，设置为yes,守护进程，后台启动;

   ```bash
   # By default Redis does not run as a daemon. Use 'yes' if you need it.
   # Note that Redis will write a pid file in /var/run/redis.pid when daemonized.
   # When Redis is supervised by upstart or systemd, this parameter has no impact.
   daemonize yes
   ```

2.  **pidfile** 

   存放pid文件的位置，每个实例会产生一个不同的pid文件

   ```bash
   # Note that on modern Linux systems "/run/redis.pid" is more conforming
   # and should be used instead.
   pidfile /var/run/redis_6379.pid
   ```

3. **loglevel**  日志级别

   指定日志记录级别，Redis总共支持四个级别：**debug、verbose、notice、warning**，默认为**notice**；

   ```bash
   # Specify the server verbosity level.
   # This can be one of:
   # debug (a lot of information, useful for development/testing)
   # verbose (many rarely useful info, but not a mess like the debug level)
   # notice (moderately verbose, what you want in production probably)
   # warning (only very important / critical messages are logged)
   loglevel notice
   ```

4.  **logfile** 日志文件\

   日志文件名称，可以自定义日志文件路径；

   ```bash
   # Specify the log file name. Also the empty string can be used to force
   # Redis to log on the standard output. Note that if you use standard
   # output for logging but daemonize, logs will be sent to /dev/null
   logfile ""
   ```

5. **databases** 数据库数

   设定库的数量 默认16，默认数据库为0，可以使用SELECT <dbid>命令在连接上指定数据库id

   ```bash
   # Set the number of databases. The default database is DB 0, you can select
   # a different one on a per-connection basis using SELECT <dbid> where
   # dbid is a number between 0 and 'databases'-1
   databases 16
   ```

> **SECURITY安全**

设置密码：访问密码的查看、设置和取消;在命令中设置密码，只是临时的。重启redis服务器，密码就还原了。永久设置，需要在配置文件中进行设置。

```bash
# The requirepass is not compatable with aclfile option and the ACL LOAD
# command, these will cause requirepass to be ignored.
#
# requirepass foobared
```

![image-20220801232754747](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220801232754747.png)

> **CLIENTS 客户端**

1. **maxclients**

   Ø 设置redis同时可以与多少个客户端进行连接。

   Ø 默认情况下为10000个客户端。

   Ø 如果达到了此限制，redis则会拒绝新的连接请求，并且向这些连接请求方发出“max number of clients reached”以作回应。

   ```bash
   # Set the max number of connected clients at the same time. By default
   # this limit is set to 10000 clients, however if the Redis server is not
   # able to configure the process file limit to allow for the specified limit
   # the max number of allowed clients is set to the current file limit
   # minus 32 (as Redis reserves a few file descriptors for internal uses).
   #
   # Once the limit is reached Redis will close all the new connections sending
   # an error 'max number of clients reached'.
   #
   # IMPORTANT: When Redis Cluster is used, the max number of connections is also
   # shared with the cluster bus: every node in the cluster will use two
   # connections, one incoming and another outgoing. It is important to size the
   # limit accordingly in case of very large clusters.
   #
   # maxclients 10000
   ```

2. maxmemory

   Ø 建议**必须设置**，否则，将内存占满，造成服务器宕机

   Ø 设置redis可以使用的内存量。一旦到达内存使用上限，redis将会试图移除内部数据，移除规则可以通过maxmemory-policy来指定。

   Ø 如果redis无法根据移除规则来移除内存中的数据，或者设置了“不允许移除”，那么redis则会针对那些需要申请内存的指令返回错误信息，比如SET、LPUSH等。

   Ø 但是对于无内存申请的指令，仍然会正常响应，比如GET等。如果你的redis是主redis（说明你的redis有从redis），那么在设置内存使用上限时，需要在系统中留出一些内存空间给同步队列缓存，只有在你设置的是“不移除”的情况下，才不用考虑这个因素。

   ```bash
   # In short... if you have replicas attached it is suggested that you set a lower
   # limit for maxmemory so that there is some free RAM on the system for replica
   # output buffers (but this is not needed if the policy is 'noeviction').
   #
   # maxmemory <bytes>
   ```

3. maxmemory-policy

   Ø volatile-lru：使用LRU算法移除key，只对设置了过期时间的键；（最近最少使用）

   Ø allkeys-lru：在所有集合key中，使用LRU算法移除key

   Ø volatile-random：在过期集合中移除随机的key，只对设置了过期时间的键

   Ø allkeys-random：在所有集合key中，移除随机的key

   Ø volatile-ttl：移除那些TTL值最小的key，即那些最近要过期的key

   Ø noeviction：不进行移除。针对写操作，只是返回错误信息

   ```bash
   # Note: with any of the above policies, when there are no suitable keys for
   # eviction, Redis will return an error on write operations that require
   # more memory. These are usually commands that create new keys, add data or
   # modify existing keys. A few examples are: SET, INCR, HSET, LPUSH, SUNIONSTORE,
   # SORT (due to the STORE argument), and EXEC (if the transaction includes any
   # command that requires memory).
   #
   # The default is:
   #
   # maxmemory-policy noeviction
   ```

4. maxmemory-samples

   Ø 设置样本数量，LRU算法和最小TTL算法都并非是精确的算法，而是估算值，所以你可以设置样本的大小，redis默认会检查这么多个key并选择其中LRU的那个。

   Ø 一般设置3到7的数字，数值越小样本越不准确，但性能消耗越小。

   ```bash
   # The default of 5 produces good enough results. 10 Approximates very closely
   # true LRU but costs more CPU. 3 is faster but not very accurate.
   #
   # maxmemory-samples 5
   ```



## 四、发布与订阅

Redis 发布订阅 (pub/sub) 是一种消息通信模式：发送者 (pub) 发送消息，订阅者 (sub) 接收消息。Redis 客户端可以订阅任意数量的频道。

| ![image-20220803231247218](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220803231247218.png) | ![image-20220803231257963](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220803231257963.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |

发布和订阅命令：

```bash
# 订阅chanel1频道
subscribe chanel1
# Chanel1发布消息
publish chanel1 hello
```



## 五、Jedis使用

1、**导包**

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>3.2.0</version>
</dependency>
```

2、**连接Redis注意事项**

1. 禁用Linux的防火墙：Linux(CentOS7)里执行命令

   + **systemctl status firewalld** 查看防火墙状态

   + **systemctl stop/disable firewalld.service**  关闭防火墙 

2. redis.conf中注释掉**bind 127.0.0.1** ,然后 **protected-mode no**（关闭保护模式）

3、**阿里云服务器配置防火墙规则**，运行6379端口访问；

![image-20220807172250134](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220807172250134.png)

4、**测试连接**

```java
public class JedisDemoOne {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("120.26.223.185", 6379);
        String ping = jedis.ping();
        System.out.println("Jedis ping:" + ping);
        jedis.close();
    }
}
```

控制台打印输出：`Jedis ping:PONG` 即表示连接成功；

### 1、基本API

**更多方法参照命令行中基本数据类型的操作**

```java
/**
 * @description: keys
 */
@Test
public void keys(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    //相当于命令行`keys *`  查看当前库所有key  (匹配：keys *1)
    Set<String> keys = jedis.keys("*");
    keys.forEach(System.out::println);

    Boolean name1 = jedis.exists("name");
    Long name2 = jedis.ttl("name");

    String set = jedis.set("name", "Tom");
    System.out.println("set function return:"+set);
    String name = jedis.get("name");
    System.out.println("get function get name:" + name);

    //关闭连接
    jedis.close();
}

/**
 * @description: String
 */
@Test
public void stringTest(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    jedis.mset("str1","v1","str2","v2","str3","v3");
    System.out.println(jedis.mget("str1","str2","str3"));
    jedis.close();
}

/**
 * @description: list
 */
@Test
public void listTest(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    jedis.lpush("mylist", "tom");
    jedis.lpush("mylist", "jack");
    List<String> list = jedis.lrange("mylist",0,-1);
    for (String element : list) {
        System.out.println(element);
    }
    jedis.close();
}

/**
 * @description: set
 */
@Test
public void setTest(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    jedis.sadd("orders", "order01");
    jedis.sadd("orders", "order02");
    jedis.sadd("orders", "order03");
    jedis.sadd("orders", "order04");
    Set<String> smembers = jedis.smembers("orders");
    for (String order : smembers) {
        System.out.println(order);
    }
    jedis.srem("orders", "order02");
    jedis.close();
}

/**
 * @description: hash
 */
@Test
public void hashTest(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    jedis.hset("hash1","userName","lisi");
    System.out.println(jedis.hget("hash1","userName"));
    Map<String,String> map = new HashMap<String,String>();
    map.put("telphone","13810169999");
    map.put("address","atguigu");
    map.put("email","abc@163.com");
    jedis.hmset("hash2",map);
    List<String> result = jedis.hmget("hash2", "telphone","email");
    for (String element : result) {
        System.out.println(element);
    }
    jedis.close();
}

/**
 * @description: zset
 */
@Test
public void zsetTest(){
    Jedis jedis = new Jedis("120.26.223.85", 6379);
    jedis.zadd("zset01", 100d, "z3");
    jedis.zadd("zset01", 90d, "l4");
    jedis.zadd("zset01", 80d, "w5");
    jedis.zadd("zset01", 70d, "z6");

    Set<String> zrange = jedis.zrange("zset01", 0, -1);
    for (String e : zrange) {
        System.out.println(e);
    }
    jedis.close();
}
```

### 2、手机验证码功能

> 要求

1、输入手机号，点击发送后随机生成6位数字码，2分钟有效

​		—— Random生成验证码    —— 把验证码放在redis里面，设置过期时间120秒； 

2、输入验证码，点击验证，返回成功或失败

​		——从redis中获取验证码与输入的验证码进行对比校验

3、每个手机号每天只能输入3次

​		——incr每次发送后+1，大于2是提交不能发送

> 编码实现

```java
public class VerificationCode {
    public static void main(String[] args) {
        vodeToRedis("18370665542");
        //verify("18370665542", "187713");
    }

    /**
     * @description: 校验验证码是否正确
     * @Param: [phone 手机号码, code 验证码]
     * @Return: boolean
     */
    public static boolean verify(String phone, String code){
        Jedis jedis = new Jedis("120.26.223.85", 6379);
        String codeKey = "VerifyCode" + phone + ":code";
        String redisCode = jedis.get(codeKey);
        if (redisCode.equals(code)){
            System.out.println("校验成功");
            jedis.close();
            return true;
        }else {
            System.out.println("校验失败");
            jedis.close();
            return false;
        }
    }


    /**
     * @description: 参数6位随机验证码
     * @Return: java.lang.String
     */
    public static String getCode(){
        Random random = new Random();
        String code = "";
        for (int i = 0; i < 6; i++) {
            int rand = random.nextInt(10);
            code += rand;
        }
        return code;
    }

    /**
     * @description: 将验证码信息存储到redis中
     * @Param: [phone 手机号码, code 验证码]
     * @Return: void
     */
    public static void vodeToRedis(String phone){
        Jedis jedis = new Jedis("120.26.223.85", 6379);
        //拼接存储验证码次数以及验证码本身的Key
        String countKey = "VerifyCode" + phone + ":count";
        String codeKey = "VerifyCode" + phone + ":code";

        //每个手机只能发送三次
        String count = jedis.get(countKey);
        if (count == null){
            jedis.setex(countKey, 24*60*60, "1");
        } else if (Integer.parseInt(count) <=2 ) {
            jedis.incr(countKey);
        } else if (Integer.parseInt(count)>2) {
            System.out.println("今天次数已用尽，请明天再试");
            jedis.close();
            return;
        }

        //将验证码放在redis中
        jedis.setex(codeKey, 120, getCode());
        jedis.close();
    }
}
```

### **3、**  **Redis**与Spring Boot整合

> 依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>

    <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-pool2</artifactId>
    </dependency>
</dependencies>
```

> 配置redis

```yaml
spring:
  # redis相关配置
  redis:
    host: 120.26.223.85
    port: 6379
    database: 0 # Redis默认索引
    timeout: 1800000 # 连接超时时间（毫秒）
    lettuce:
      pool:
        max-active: 20   # 连接池最大连接数（复数表示没有限制）
        max-wait: -1     # 最大阻塞等待时间。负数表示没有限制
        max-idle: 5      # 连接池中最大1空闲连接
        min-idle: 0      # 连接池中最小空闲连接
```

> 配置类

```java
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;

/**
 * @description: Redis配置类
 * @author: HuangJiBin
 * @date: 2022/8/8  9:05
 * @since: OpenJDK 11
 */
@EnableCaching
@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        // jackson序列化所有的类
        Jackson2JsonRedisSerializer Jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        // jackson序列化的一些配置
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance);
        Jackson2JsonRedisSerializer.setObjectMapper(om);
        // String的序列化
        StringRedisSerializer stringSerializer = new StringRedisSerializer();

        //将我们的key采用String的序列化方式
        template.setKeySerializer(stringSerializer);
        //将我们的hash的key也采用String的序列化方式
        template.setHashKeySerializer(stringSerializer);
        //value采用jackson序列化方式
        template.setValueSerializer(Jackson2JsonRedisSerializer);
        //hash的value也采用jackson序列化方式
        template.setHashValueSerializer(Jackson2JsonRedisSerializer);

        template.afterPropertiesSet();
        return template;
    }

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        RedisCacheManager redisCacheManager = RedisCacheManager.builder(factory)
                        .cacheDefaults(defaultCacheConfig(10000))
                        .transactionAware()
                        .build();
        return  redisCacheManager;
    }

    private RedisCacheConfiguration defaultCacheConfig(Integer second) {
        Jackson2JsonRedisSerializer<Object> serializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL,JsonAutoDetect.Visibility.ANY);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        serializer.setObjectMapper(mapper);

        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofSeconds(second))
                .serializeKeysWith(RedisSerializationContext
                        .SerializationPair
                        .fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(
                        RedisSerializationContext
                                .SerializationPair
                                .fromSerializer(serializer))
                .disableCachingNullValues();
        return config;
    }

}

```

> 测试类

```java
@RestController
@RequestMapping("/redis")
public class RedisTestController {
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/redisTest")
    public String testRedis() {
        //设置值到redis
        redisTemplate.opsForValue().set("name","lucy");
        //从redis获取值
        String name = (String)redisTemplate.opsForValue().get("name");
        return name;
    }
}
```

## 六、Redis事务

### 1、事务

Redis事务是一个单独的隔离操作：事务中的所有命令都会**序列化**、**按顺序地执行**。事务在执行的过程中，不会被其他客户端发送来的命令请求所打断。

**Redis事务的主要作用就是串联多个命令防止别的命令插队**。

> 事务实现  **Multi、Exec、discard**

从输入`Multi`命令开始，输入的命令都会依次进入**命令队列**中，但不会执行，直到输入`Exec`后，Redis会将之前的命令队列中的命令依次执行。组队的过程中可以通过`discard`来放弃组队。 

![image-20220809093106934](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809093106934.png)

> **事务的错误处理**

+ 组队中某个命令出现了报告错误，执行时整个的所有队列都会被取消。
+ 如果执行阶段某个命令报出了错误，则**只有报错的命令不会被执行，而其他的命令都会执行，不会回滚**。这是于MySQL的区别；

### 2、锁机制

#### 1、悲观锁

![image-20220809093356431](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809093356431.png)

**悲观锁(Pessimistic Lock)**, 顾名思义，就是很悲观，每次去拿数据的时候都认为别人会修改，所以每次在拿数据的时候都会上锁，这样别人想拿这个数据就会block直到它拿到锁。**传统的关系型数据库里边就用到了很多这种锁机制**，比如**行锁**，**表锁**等，**读锁**，**写锁**等，都是在做操作之前先上锁。

#### 2、乐观锁

![image-20220809093427586](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809093427586.png)

**乐观锁(Optimistic Lock),** 顾名思义，就是很乐观，每次去拿数据的时候都认为别人不会修改，所以不会上锁，但是在更新的时候会判断一下在此期间别人有没有去更新这个数据，可以使用**版本号**等机制。**乐观锁适用于多读的应用类型，这样可以提高吞吐量**。Redis就是利用这种check-and-set机制实现事务的。

#### 3、Redis中锁的应用

在执行multi之前，先执行`watch key1 [key2]`,可以监视一个(或多个) key ，如果在事务执行之前这个(或这些) key **被其他命令所改动，那么事务将被打断。**

**`unwatch`**用于取消 `WATCH` 命令对所有 key 的监视。

### **3、**  **Redis**事务三特性

+ 单独的**隔离**操作 
  + 事务中的所有命令都会序列化、按顺序地执行。事务在执行的过程中，不会被其他客户端发送来的命令请求所打断。 
+  没有**隔离级别**的概念 
  + 队列中的命令没有提交之前都不会实际被执行，因为事务提交前任何指令都不会被实际执行
+  **不保证原子性** 
  + 事务中如果有一条命令执行失败，其后的命令仍然会被执行，没有回滚 

### **4、** Redis_事务秒杀案例

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809173515449.png" alt="image-20220809173515449" style="zoom:80%;" />

用一个字段存储商品库存、一个存储秒杀成功的用户id，用户id用set存，避免出现重复用户；

足以可能产生以下三种问题：

1. 超卖问题

   | <img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809174627042.png" alt="image-20220809174627042" style="zoom:50%;" /> | <img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809174640298.png" alt="image-20220809174640298" style="zoom: 50%;" /> |
   | ------------------------------------------------------------ | ------------------------------------------------------------ |
   |                                                              | **利用乐观锁淘汰用户，解决超卖问题。**                       |

   ```java
   public static boolean doSecKill(String uid,String prodid) throws IOException {
       //1、uid和prodid非空判断
       if (uid ==null || prodid==null){
           return false;
       }
   
       //2、连接redis
       Jedis jedis = new Jedis("120.26.223.85", 6379);
   
       //3、拼接key
       // 3.1 库存key
       String kcKey = "sk:"+prodid+":qt";
       // 3.2 秒杀成功用户key
       String userKey = "sk:"+prodid+":user";
   
       //监视库存
       jedis.watch(kcKey);
   
       //4、获取库存，如果库存null，秒杀还没开始
       String kc = jedis.get(kcKey);
       if (kc == null){
           System.out.println("秒杀还未开始，请等待...");
           jedis.close();
           return false;
       }
   
       //5、判断用户是否重复秒杀操作
       if (jedis.sismember(userKey, uid)) {
           System.out.println("已成功秒杀，请不要重复提交...");
           jedis.close();
           return false;
       }
   
       //6、判断商品数量，库存数量小于1秒杀结束
       int i = Integer.parseInt(kc);
       if (Integer.parseInt(kc) <=0){
           System.out.println("秒杀结束...");
           jedis.close();
           return false;
       }
   
       //7 秒杀过程
       //添加事务操作
       Transaction multi = jedis.multi();
       //组队
       multi.decr(kcKey);
       multi.sadd(userKey, uid);
       //执行
       List<Object> results = multi.exec();
       if (results == null || results.size() == 0){
           System.out.println("秒杀失败...");
           jedis.close();
           return false;
       }
       System.out.println("恭喜你，秒杀成功...");
       jedis.close();
       return true;
   }
   ```

   

2. 少卖问题（已经秒光，可是还有库存）

   这是乐观锁造成的库存遗留问题；当有人成功秒杀了一件商品后对库存版本进行了修改，导致后面的读取到的数据版不一致;可以使用**LUA脚本**解决该问题；

   Lua 是一个小巧的[脚本语言](http://baike.baidu.com/items/脚本语言)，Lua脚本可以很容易的被C/C++ 代码调用，也可以反过来调用C/C++的函数，Lua并没有提供强大的库，一个完整的Lua解释器不过200k，所以Lua不适合作为开发独立应用程序的语言，而是作为嵌入式脚本语言。

   LUA脚本是类似redis事务，有一定的原子性，不会被其他命令插队，可以完成一些redis事务性的操作。但是注意redis的lua脚本功能，只有在Redis 2.6以上的版本才可以使用。

   ```java
   local userid=KEYS[1]; 
   local prodid=KEYS[2];
   local qtkey="sk:"..prodid..":qt";
   local usersKey="sk:"..prodid.":usr'; 
   local userExists=redis.call("sismember",usersKey,userid);
   if tonumber(userExists)==1 then 
     return 2;
   end
   local num= redis.call("get" ,qtkey);
   if tonumber(num)<=0 then 
     return 0; 
   else 
     redis.call("decr",qtkey);
     redis.call("sadd",usersKey,userid);
   end
   return 1;
   
   ```

   

3. 连接超时问题 ——使用连接池解决；

   ```java
   public class JedisPoolUtil {
      private static volatile JedisPool jedisPool = null;
   
      private JedisPoolUtil() {
      }
   
      public static JedisPool getJedisPoolInstance() {
         if (null == jedisPool) {
            synchronized (JedisPoolUtil.class) {
               if (null == jedisPool) {
                  JedisPoolConfig poolConfig = new JedisPoolConfig();
   					//MaxTotal：控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；赋值为-1则表示不限制；
   					poolConfig.setMaxTotal(200);
   					//maxIdle：控制一个pool最多有多少个状态为idle(空闲)的jedis实例；
   					poolConfig.setMaxIdle(32);
   					//MaxWaitMillis：表示当borrow一个jedis实例时，最大的等待毫秒数，如果超过等待时间，则直接抛JedisConnectionException；
   					poolConfig.setMaxWaitMillis(100*1000);
   					poolConfig.setBlockWhenExhausted(true);
   					//testOnBorrow：获得一个jedis实例的时候是否检查连接可用性（ping()）；如果为true，则得到的jedis实例均是可用的；
   					poolConfig.setTestOnBorrow(true);  // ping  PONG
   				 
   					jedisPool = new JedisPool(poolConfig, "120.26.223.85", 6379, 60000 );
               }
            }
         }
         return jedisPool;
      }
   
      public static void release(JedisPool jedisPool, Jedis jedis) {
         if (null != jedis) {
            //jedisPool.returnResource(jedis);
            jedisPool.close();
         }
      }
   
   }
   ```



## 七、Redis持久化



### 1、 RDB（Redis DataBase）

在指定的时间间隔内将内存中的数据集快照写入磁盘， 也就是行话讲的`Snapshot快照`，它恢复时是**将快照文件直接读到内存里**。

> EDB持久化流程

![image-20220809101428628](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809101428628.png)

+ Redis会单独创建（fork）一个子进程来进行持久化，会先将数据写入到 一个临时文件中，待持久化过程都结束了，再用这个临时文件替换上次持久化好的文件。 整个过程中，主进程是不进行任何IO操作。
+ Fork的作用是复制一个与当前进程一样的进程。新进程的所有数据（变量、环境变量、程序计数器等） 数值都和原进程一致，但是是一个全新的进程，并作为原进程的子进程。
+ 在Linux程序中，fork()会产生一个和父进程完全相同的子进程，但子进程在此后多会exec系统调用，出于效率考虑，Linux中引入了“**写时复制技术**”
+ **一般情况父进程和子进程会共用同一段物理内存**，只有进程空间的各段的内容要发生变化时，才会将父进程的内容复制一份给子进程。

> RDB策略保持配置

+ redis.conf中配置文件名称，默认为dump.rdb；

+ rdb文件的保存路径，也可以修改。默认为Redis启动时命令行所在的目录下；

+ 配置文件中默认的快照配置

+ 保存命令

  + `save` ：save时只管保存，其它不管，全部阻塞。手动保存。不建议。
    + 格式：`save 秒钟 写操作次数`,   默认是1分钟内改了1万次，或5分钟内改了10次，或15分钟内改了1次。
    
    + 禁用:  不设置save指令，或者给save传入空字符串
    
      ![image-20220809103603603](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809103603603.png)
    
  + **`bgsave`：Redis会在后台异步进行快照操作， 快照同时还可以响应客户端请求。**

  + 可以通过`lastsave `命令获取最后一次成功执行快照的时间

  + `flushall`命令，也会产生`dump.rdb`文件，但里面是空的，无意义

+ 配置文件：

  + **`stop-writes-on-bgsave-error`**： 当Redis无法写入磁盘的话，直接关掉Redis的写操作。推荐yes。
  + **`rdbcompression`** **压缩文件**：对于存储到磁盘中的快照，可以设置是否进行**压缩存储**。如果是的话，redis会采用`LZF算法`进行压缩。如果不想消耗CPU来进行压缩的话，可以设置为关闭此功能。推荐yes.
  + **rdbchecksum检查完整性**：在存储快照后，还可以让redis使用CRC64算法来进行数据校验，但是这样做会增加大约10%的性能消耗，如果希望获取到最大的性能提升，可以关闭此功能

> **RDB的备份**

+ `config get dir `查询rdb文件的目录 。
+ 将`*.rdb`的文件拷贝到别的地方。

RDB的恢复：

+ 关闭Redis。
+ 先把备份的文件拷贝到工作目录下 `cp dump2.rdb dump.rdb`。
+ 启动Redis, 备份数据会直接加载。

> 优劣势

1. 优势
   1. 适合大规模的数据恢复
   2. 对数据完整性和一致性要求不高更适合使用
   3. 节省磁盘空间
   4. 恢复速度快
2. 劣势
   1. Fork的时候，内存中的数据被克隆了一份，大致**2倍的膨胀性**需要考虑
   2. 虽然Redis在fork时使用了**写时拷贝技术**,但是如果数据庞大时还是比较消耗性能。
   3. 在备份周期在一定间隔时间做一次备份，所以如果Redis意外down掉的话，就会**丢失最后一次快照后的所有修改**。

> 小结

![image-20220809102928707](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809102928707.png)

**动态停止RDB**：`redis-cli config set save ""`  #save后给空值，表示禁用保存策略



### 2、AOF（Append Only File）

以**日志**的形式来记录每个写操作（**增量保存**），将Redis执行过的所有写指令记录下来(**读操作不记录**)， **只许追加文件但不可以改写文件**，redis启动之初会读取该文件重新构建数据，换言之，redis 重启的话就根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作。

**AOF默认不开启**，可以在redis.conf中配置文件名称，文件名默认为` appendonly.aof`；AOF文件的保存路径，同RDB的路径一致。

**AOF和RDB同时开启，系统默认取AOF的数据**（数据不会存在丢失）。

> AOP持久化流程

![image-20220809110251877](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809110251877.png)

+ 客户端的请求写命令会被append追加到**AOF缓冲区**内；

+ AOF缓冲区根据AOF持久化策略[**always,everysec,no**]将操作sync同步到磁盘的AOF文件中；

+ AOF文件大小超过重写策略或手动重写时，会对AOF文件rewrite重写，压缩AOF文件容量；

+ Redis服务重启时，会重新load加载AOF文件中的写操作达到数据恢复的目的；

  

> AOP的启动与恢复

正常恢复：

+ 修改默认的`appendonly no`，改为`yes`

+ 将有数据的`aof`文件复制一份保存到对应目录(查看目录：`config get dir`)

+ 恢复：重启`redis`然后重新加载

异常恢复：

+ 修改默认的`appendonly no`，改为`yes`

+ 如遇到**AOF**文件损坏**，通过/usr/local/bin/redis-check-aof --fix appendonly.aof**进行恢复

+ 备份被写坏的AOF文件

+ 恢复：重启redis，然后重新加载

  

> AOF同步频率设置 `appendfsync `

`appendfsync always`:

​	始终同步，每次Redis的写入都会立刻记入日志；**性能较差但数据完整性比较好**。

`appendfsync everysec`:

​	每秒同步，每秒记入日志一次，如果宕机，本秒的数据可能丢失。

`appendfsync no`:

​	redis不主动进行同步，把同步交给操作系统。



> **Rewrite压缩**

​		AOF采用**文件追加**方式，文件会越来越大为避免出现此种情况，新增了**重写机制**, **当AOF文件的大小超过所设定的阈值时，Redis就会启动AOF文件的内容压缩， 只保留可以恢复数据的最小指令集**。可以使用命令`bgrewriteaof`。

**原理：**

​		AOF文件持续增长而过大时，会fork出一条新进程来将文件重写(也是先写临时文件最后再`rename`)，`redis4.0`版本后的重写，是指上就是把`rdb` 的快照，以二级制的形式附在新的`aof`头部，作为已有的历史数据，替换掉原来的流水账操作。

​		Redis会记录上次重写时的`AOF`大小，默认配置是当`AOF`文件大小是上次rewrite后大小的一倍且文件大于`64M`时触发重写。

**配置：**

`no-appendfsync-on-rewrite`：

+  `no-appendfsync-on-rewrite=yes `,不写入aof文件只写入缓存，用户请求不会阻塞，但是在这段时间如果宕机会丢失这段时间的缓存数据。（降低数据安全性，提高性能）
+ `no-appendfsync-on-rewrite=no`, 还是会把数据往磁盘里刷，但是遇到重写操作，可能会发生阻塞。（数据安全，但是性能降低）

`auto-aof-rewrite-percentage`：

+ 设置重写的基准值，文件达到100%时开始重写（文件是原来重写后文件的2倍时触发）

`auto-aof-rewrite-min-size`：

+ 设置重写的基准值，最小文件64MB。达到这个值开始重写。



> 重写流程

1. `bgrewriteaof`触发重写，判断是否当前有`bgsave`或`bgrewriteaof`在运行，如果有，则等待该命令结束后再继续执行。
2. 主进程`fork`出子进程执行重写操作，保证主进程不会阻塞。
3. 子进程遍历redis内存中数据到临时文件，**客户端的写请求同时写入aof_buf缓冲区和aof_rewrite_buf重写缓冲区保证原AOF文件完整**以及新AOF文件生成期间的新的数据修改动作不会丢失。
4. 子进程写完新的AOF文件后，向主进程发信号，父进程更新统计信息。主进程把**aof_rewrite_buf中的数据写入到新的AOF文件**。
5. 使用新的AOF文件覆盖旧的AOF文件，完成AOF重写。

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809113921224.png" alt="image-20220809113921224" style="zoom:80%;" />



> 优劣势

优势：

+ 备份机制更稳健，丢失数据概率更低。
+ 可读的日志文本，通过操作AOF稳健，可以处理误操作。

劣势：

+ 比起RDB占用更多的磁盘空间。
+ 恢复备份速度要慢。
+ 每次读写都同步的话，有一定的性能压力。
+ 存在个别Bug，造成恢复不能。

> 总结

![image-20220809110855743](E:\技术文档\Spring\SpringBoot2学习笔记\image-20220809110855743.png)

> RDB与AOP的选择

官方推荐两个都启用。

+ 如果对数据不敏感，可以选单独用RDB。

+ 不建议单独用 AOF，因为可能会出现Bug。

+ 如果只是做纯内存缓存，可以都不用。



## 八、主从复制

![image-20220809115433039](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220809115433039.png)

主机数据更新后根据配置和策略， 自动同步到备机的`master`/`slaver`机制，**Master**以写为主，**Slave**以读为主,实现读写分离，性能扩展；容灾快速恢复；

### 1、一主二从本地搭建

> 思路

本地利用三个配置文件开启三个`Redis`服务，三个配置文件分别引入`redis.conf`配置文件,单独在各自配置文件中配置`pidfile`、`port`、`dump.rdb`属性；在其中两台Redis服务中配置另一台为主机；

```xml
# 配置案例
include /myredis/redis.conf
pidfile /var/run/redis_6379.pid
port 6379
```

分别使用redis-cli连接这三台服务器，使用`info replication`命令查看当前主从复制相关的信息，发现各个服务都是主服务器；

在需要作为从服务器中执行`slaveof <主机ip> <主机port>`成为某个实例的从服务器；再次打印主从复制相关信息，发现主服务器下就有了设置的从服务器，从服务器下以及连接到主服务器；从此主服务器上进行写入从服务器上可以读到，但是无法在从服务器上进行写入；

主机挂掉，重启就行，一切如初，从服务器还是将它视为主服务器；但从机重启需重设主机信息，可以将配置增加到文件中，永久生效，另外，在从服务器停止期间主服务器写入的内容，从服务器启动后也可以获得。

### 2、主从复制原理

+ `Slave`启动成功连接到`master`后会发送一个`sync`命令;
+ `Master`接到命令启动后台的存盘进程，同时收集所有接收到的用于修改数据集命令， 在后台进程执行完毕之后，`master`将传送整个数据文件到`slave`,以完成一次完全同步;(写入到rdb文件中，发送rdb给从服务器)
+ 全量复制：而`slave`服务在接收到数据库文件数据后，将其存盘并加载到内存中。
+ 增量复制：`Master`继续将新的所有收集到的修改命令依次传给`slave`,完成同步。
+ 但是只要是重新连接master,一次完全同步（全量复制)将被自动执行。

![image-20220813113335534](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813113335534.png)

### 3、其他形式

1. 薪火相传

   上一个`Slave`可以是下一个`slave`的`Master`，`Slave`同样可以接收其他` slaves`的连接和同步请求，那么该`slave`作为了链条中下一个的`master`, 可以有效减轻master的写压力,去中心化降低风险。风险是一旦某个`slave`宕机，后面的`slave`都没法备份, 另外主机挂了，从机还是从机，无法写数据了；

   ![image-20220813114232559](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813114232559.png)

2. 反客为主

   当一个master宕机后，后面的slave可以立刻升为master，其后面的slave不用做任何修改。用 `slaveof no one  `命令将从机变为主机。但是这样的缺点的必须在主机宕机后在从机中手动输入命令；

### 4、哨兵模式（**sentinel**）

**反客为主的自动版**，能够后台监控主机是否故障，如果故障了根据投票数自动将从库转换为主库。

![image-20220813114522321](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813114522321.png)

> 搭建

1. **新建sentinel.conf文件，名字绝不能错**,该文件下配置以下内容：

   ```xml
   sentinel monitor mymaster 127.0.0.1 6379 1
   ```

   其中`mymaster`为监控对象起的服务器名称， 1 为至少有多少个哨兵同意迁移的数量。

2.  **启动哨兵**

   ```java
   redis-sentinel  /myredis/sentinel.conf
   ```

   <img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813130933355.png" alt="image-20220813130933355" style="zoom:67%;" />

3. 切换主机

   **当主机挂掉，从机选举中产生新的主机**，大概10秒左右可以看到哨兵窗口日志，切换了新的主机；

   根据从机配置文件中`replica-priority`中配置优先级，数字越小，优先级越高；

   原主机重启后会变为从机。

> 故障恢复过程

<img src="E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813131337935.png" alt="image-20220813131337935" style="zoom:80%;" />

优先级在redis.conf中默认：replica-priority 100，值越小优先级越高

偏移量是指获得原主机数据最全的

每个redis实例启动后都会随机生成一个40位的runid

> 主从复制Java获取Jedis

```Java
public class JedisSentinel {
    private static JedisSentinelPool jedisSentinelPool=null;

    public static Jedis getJedisFromSentinel(){
        if(jedisSentinelPool==null){
            Set<String> sentinelSet=new HashSet<>();
            /**哨兵的地址与端口*/
            sentinelSet.add("192.168.11.103:26379");

            JedisPoolConfig jedisPoolConfig =new JedisPoolConfig();
            //最大可用连接数
            jedisPoolConfig.setMaxTotal(10);
            //最大闲置连接数
            jedisPoolConfig.setMaxIdle(5);
            //最小闲置连接数
            jedisPoolConfig.setMinIdle(5);
            //连接耗尽是否等待
            jedisPoolConfig.setBlockWhenExhausted(true);
            //等待时间
            jedisPoolConfig.setMaxWaitMillis(2000);
            //取连接的时候进行一下测试 ping pong
            jedisPoolConfig.setTestOnBorrow(true);

            //mymaster为监控服务器名
            jedisSentinelPool=new JedisSentinelPool("mymaster",sentinelSet,jedisPoolConfig);
            return jedisSentinelPool.getResource();
        }else{
            return jedisSentinelPool.getResource();
        }
    }
}
```

## 九、集群

> 解决什么问题

+ 但Redis服务器容量不够
+ 并发写操作
+ 主从模式，薪火相传模式，主机宕机，导致ip地址发生变化，应用程序中配置需要修改对应的主机地址、端口等信息。

之前通过代理主机来解决，但是redis3.0中提供了解决方案。就是无中心化集群配置。

> 什么是集群

Redis 集群实现了对Redis的水平扩容，即启动N个redis节点，将整个数据库分布存储在这N个节点中，每个节点存储总数据的1/N。

Redis 集群通过分区（partition）来提供一定程度的可用性（availability）： 即使集群中有一部分节点失效或者无法进行通讯， 集群也可以继续处理命令请求。

### 1、集群搭建

1. 配置集群的单台服务器

   ```xml
   开启daemonize yes
   Pid文件名字
   指定端口
   Log文件名字
   Dump.rdb名字
   Appendonly 关掉或者换名字
   cluster-enabled yes    打开集群模式
   cluster-config-file nodes-6379.conf  设定节点配置文件名
   cluster-node-timeout 15000   设定节点失联时间，超过该时间（毫秒），集群自动进行主从切换。
   ```

2. 分别启动多台Redis服务器

   确保所有redis实例启动后，`nodes-xxxx.conf`文件都生成正常。

3.  **将六个节点合成一个集群**

   进入到Redis的安装目录下：`cd /opt/redis-6.2.1/src`后执行以下命令：

   ```xml
   redis-cli --cluster create --replicas 1 192.168.11.101:6379 192.168.11.101:6380 192.168.11.101:6381 192.168.11.101:6389 192.168.11.101:6390 192.168.11.101:6391
   ```

   `192.168.11.101`为各服务器的IP地址，此次即便是本地搭建一个集群案例也不能使用`127.0.0.1`，`--replicas 1 `采用最简单的方式配置集群，一台主机，一台从机。

4. 以集群方式连接到redis-cli

   `redis-cli -c -p 6379`:

   **-c 采用集群策略连接，设置数据会自动切换到相应的写主机**

5. **查看集群信息**

   **`cluster nodes `命令查看集群信息**

### 2、集群操作

> **slots**

+ 一个 Redis 集群包含 16384 个插槽（hash slot）， 数据库中的每个键都属于这 16384 个插槽的其中一个。

+ 集群使用公式` CRC16(key) % 16384 `来计算键 `key `属于哪个槽， 其中` CRC16(key) `语句用于计算键 key 的 CRC16 校验和 。

+ 集群中的每个节点负责处理一部分插槽。 举个例子， 如果一个集群有三个主节点， 其中：

  `节点 A `负责处理 0 号至 5460 号插槽。

  `节点 B `负责处理 5461 号至 10922 号插槽。

  `节点 C `负责处理 10923 号至 16383 号插槽。

> **在集群中录入值**

在redis-cli每次录入、查询键值，redis都会计算出该key应该送往的插槽，如果不是该客户端对应服务器的插槽，redis会报错，并告知应前往的redis实例地址和端口。

redis-cli客户端提供了 –c 参数实现自动重定向。如 redis-cli -c –p 6379 登入后，再录入、查询键值对可以自动重定向。

不在一个slot下的键值，是不能使用mget,mset等多键操作。

![image-20220813171818340](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813171818340.png)                               

可以通过{}来定义组的概念，从而使key中{}内相同内容的键值对放到一个slot中去。

 ![image-20220813171829168](E:\IDEA_Projects\JavaTechnologyCollection\Redis\note\RedisNote.assets\image-20220813171829168.png)

>  **查询集群中的值**

`CLUSTER GETKEYSINSLOT <slot> <count>` 返回 count 个 slot 槽中的键。

> **故障恢复**

主节点下线后从节点自动升为主节点，当原来的主节点重启后作为新主节点的从节点；

若主从节点均宕机，即某一段插槽的主从节点都宕掉：

1. `redis.conf`中`cluster-require-full-coverage` 为`yes` ，那么 ，整个集群都挂掉。
2. `cluster-require-full-coverage` 为`no `，那么，该插槽数据全都不能使用，也无法存储。

> **集群的Jedis开发**

即使连接的不是主机，集群会自动切换主机存储。主机写，从机读。无中心化主从集群。无论从哪台主机写的数据，其他主机上都能读到数据。

```java
public class JedisClusterTest {
    public static void main(String[] args) {
        Set<HostAndPort> set =new HashSet<HostAndPort>();
        set.add(new HostAndPort("192.168.31.211",6379));
        JedisCluster jedisCluster=new JedisCluster(set);
        jedisCluster.set("k1", "v1");
        System.out.println(jedisCluster.get("k1"));
    }
}
```

### 3、集群优缺点

> 优点

1. 实现扩容
2. 分摊压力
3. 无中心配置相对简单

> 缺点

1. 多键操作是不被支持的 
2. 多键的Redis事务是不被支持的。lua脚本不被支持

## 十、Redis问题解决

### 1、



### 2、



### 3、



### 4、



### 5、











