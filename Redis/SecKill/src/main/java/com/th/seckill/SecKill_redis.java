package com.th.seckill;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

import java.io.IOException;
import java.util.List;

/**
 * @description: redis订单秒杀demo
 * @author: HuangJiBin
 * @date: 2022/8/9  16:52
 * @since: 1.8
 */
public class SecKill_redis {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("120.26.223.85", 6379);
        System.out.println(jedis.ping());
        jedis.close();
    }

    /**
     * @description: 秒杀过程
     * @Param: [uid 用户id, prodid 商品id]
     * @Return: boolean
     */
    public static boolean doSecKill(String uid,String prodid) throws IOException {
        //1、uid和prodid非空判断
        if (uid ==null || prodid==null){
            return false;
        }

        //2、连接redis
        //Jedis jedis = new Jedis("120.26.223.85", 6379);

        //使用连接池的方式获取到jedis对象
        JedisPool jedisPoolInstance = JedisPoolUtil.getJedisPoolInstance();
        Jedis jedis = jedisPoolInstance.getResource();

        //3、拼接key
        // 3.1 库存key
        String kcKey = "sk:"+prodid+":qt";
        // 3.2 秒杀成功用户key
        String userKey = "sk:"+prodid+":user";

        //监视库存
        jedis.watch(kcKey);

        //4、获取库存，如果库存null，秒杀还没开始
        String kc = jedis.get(kcKey);
        if (kc == null){
            System.out.println("秒杀还未开始，请等待...");
            jedis.close();
            return false;
        }

        //5、判断用户是否重复秒杀操作
        if (jedis.sismember(userKey, uid)) {
            System.out.println("已成功秒杀，请不要重复提交...");
            jedis.close();
            return false;
        }

        //6、判断商品数量，库存数量小于1秒杀结束
        int i = Integer.parseInt(kc);
        if (Integer.parseInt(kc) <=0){
            System.out.println("秒杀结束...");
            jedis.close();
            return false;
        }

        //7 秒杀过程
        //7.1 库存-1
        //jedis.decr(kcKey);
        //7.2 把秒杀成功用户添加清单里面
        //jedis.sadd(userKey, uid);

        //添加事务操作
        Transaction multi = jedis.multi();
        //组队
        multi.decr(kcKey);
        multi.sadd(userKey, uid);
        //执行
        List<Object> results = multi.exec();
        if (results == null || results.size() == 0){
            System.out.println("秒杀失败...");
            jedis.close();
            return false;
        }
        System.out.println("恭喜你，秒杀成功...");
        jedis.close();
        return true;
    }

}
