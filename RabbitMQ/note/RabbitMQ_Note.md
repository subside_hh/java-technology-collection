# 前言
1. RabbitMQ安装：https://www.cnblogs.com/liuyangfirst/p/16002565.html
2. RabbitMQ采用 Erlang 实现的工业级的消息队列(MQ)服务器，所以在安装rabbitMQ之前，需要先安装Erlang；
3. 延时插件：`rabbitmq_delayed_message_exchange`  [github](https://github.com/rabbitmq/rabbitmq-delayed-message-exchange)
4. 