package com.th.delaytask.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @description: 使用插件的方式配置RabbitMQ rabbitmq_delayed_message_exchange
 * @author: HuangJiBin
 * @date: 2022/7/15  12:43
 * @since: 1.8
 */
@Configuration
public class DelayPluginConfiguration {
    /**队列*/
    public static final String DELAYED_PLUGIN_QUEUE_NAME = "delayed.plugin.queue";
    /**交换机*/
    public static final String DELAYED_PLUGIN_EXCHANGE_NAME = "delayed.plugin.exchange";
    /**路由Key  routingKey*/
    public static final String DELAYED_PLUGIN_ROUTING_KEY = "delayed.plugin.routingkey";

    /**
     * @description: 声明队列
     * @Return: org.springframework.amqp.core.Queue
     */
    @Bean("delayPluginQueue")
    public Queue delayPluginQueue(){
        return new Queue(DELAYED_PLUGIN_QUEUE_NAME,true);
    }

    /**
     * @description: 声明基于插件的延时交换机
     * @Return: org.springframework.amqp.core.CustomExchange
     */
    @Bean("delayPluginExchange")
    public CustomExchange delayPluginExchange(){
        HashMap<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAYED_PLUGIN_EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }

    /**
     * @description: 队列绑定交换机
     * @Param: [queue, customExchange]
     * @Return: org.springframework.amqp.core.Binding
     */
    @Bean("delayPluginBinding")
    public Binding delayPluginBinding(@Qualifier("delayPluginQueue") Queue queue, @Qualifier("delayPluginExchange") CustomExchange customExchange){
        return BindingBuilder.bind(queue)
                .to(customExchange)
                .with(DELAYED_PLUGIN_ROUTING_KEY)
                .noargs();
    }



}

