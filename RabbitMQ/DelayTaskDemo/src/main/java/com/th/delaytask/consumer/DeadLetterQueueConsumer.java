package com.th.delaytask.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import com.rabbitmq.client.Channel;
import java.time.LocalDateTime;

import static com.th.delaytask.config.DelayPluginConfiguration.DELAYED_PLUGIN_QUEUE_NAME;
import static com.th.delaytask.config.RabbitMQConfiguration.DEAD_LETTER_QUEUE_A_NAME;
import static com.th.delaytask.config.RabbitMQConfiguration.DEAD_LETTER_QUEUE_B_NAME;

/**
 * @description: 死信队列消费者
 * @author: HuangJiBin
 * @date: 2022/7/15  12:47
 * @since: 1.8
 */
@Slf4j
@Component
public class DeadLetterQueueConsumer {

    /**
     * @description: 监听死信队列A
     * @Param: [message] 发送的消息内容
     * @Return: void
     */
    @RabbitListener(queues = DEAD_LETTER_QUEUE_A_NAME, containerFactory = "rabbitListenerContainerFactory")
    public void receiveA(Message message, Channel channel){
        //获取消息
        String msg = new String(message.getBody());
        //记录日志
        log.info("当前时间：{}，死信队列A收到消息{}", LocalDateTime.now(), msg);
    }

    /**
     * @description: 监听死信队列B
     * @Param: [message] 发送的消息内容
     * @Return: void
     */
    @RabbitListener(queues = DEAD_LETTER_QUEUE_B_NAME)
    public void receiveB(Message message, Channel channel){
        //获取消息
        String msg = new String(message.getBody());
        //记录日志
        log.info("当前时间：{}，死信队列B收到消息{}", LocalDateTime.now(), msg);
    }

    /**
     * @description: 监听使用插件方式的队列
     * @param: [message, channel]
     * @Return: void
     */
    @RabbitListener(queues = DELAYED_PLUGIN_QUEUE_NAME)
    public void receivePlugin(Message message, Channel channel){
        String s = new String(message.getBody());
        log.info("DeadLetterQueueConsumer.class：【{}】，死信队列收到消息{}", LocalDateTime.now(), s);
    }

}

