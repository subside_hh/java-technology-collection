package com.th.delaytask.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * @description: 延时类型枚举
 * @author: HuangJiBin
 * @date: 2022/7/15  12:50
 * @since: 1.8
 */
@Getter
@AllArgsConstructor
public enum DelayTypeEnum {
    //10s
    DELAY_10s(1),

    //60s
    DELAY_60s(2);

    private Integer type;

    public static DelayTypeEnum getDelayTypeEnum(Integer type){
        if (Objects.equals(type, DELAY_10s.type)){
            return DELAY_10s;
        }
        if (Objects.equals(type, DELAY_60s.type)){
            return DELAY_60s;
        }
        return null;
    }
}
