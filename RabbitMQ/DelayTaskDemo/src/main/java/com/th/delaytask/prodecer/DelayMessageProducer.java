package com.th.delaytask.prodecer;

import com.th.delaytask.enums.DelayTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.th.delaytask.config.DelayPluginConfiguration.DELAYED_PLUGIN_EXCHANGE_NAME;
import static com.th.delaytask.config.DelayPluginConfiguration.DELAYED_PLUGIN_ROUTING_KEY;
import static com.th.delaytask.config.RabbitMQConfiguration.*;

/**
 * @description: 延时消息生产者
 * @author: HuangJiBin
 * @date: 2022/7/15  12:50
 * @since: 1.8
 */
@Component
@Slf4j
public class DelayMessageProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * @description: 生产者 发送消息
     * @Param: [message, type]
     * @Return: void
     */
    public void send(String message, DelayTypeEnum type){
        switch (type){
            case DELAY_10s:
                rabbitTemplate.convertAndSend(DELAY_EXCHANGE_NAME, DELAY_QUEUE_A_ROUTING_KEY, message);
                break;
            case DELAY_60s:
                rabbitTemplate.convertAndSend(DELAY_EXCHANGE_NAME, DELAY_QUEUE_B_ROUTING_KEY, message);
                break;
            default:
        }
    }

    /**
     * @description: 使用插件的方式发送消息，生成者
     * @Param: [message 发送的消息内容 , delayTime 延迟时长]
     * @Return: void
     */
    public void sendByPlugin(String message, Integer delayTime){
        rabbitTemplate.convertAndSend(DELAYED_PLUGIN_EXCHANGE_NAME, DELAYED_PLUGIN_ROUTING_KEY, message,msg ->{
            log.info("DelayMessageProducer： [{}] ,发送一条时长：[{}] ms 发送信息给延迟队列delayed.plugin.queue ： [{}] ", LocalDateTime.now(),delayTime,message);
            //发送消息的时候，延迟时长，单位 ms
            msg.getMessageProperties().setDelay(delayTime);
            return msg;
        });
    }

}