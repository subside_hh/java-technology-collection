package com.th.delaytask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @description: DelayTask项目启动类
 * @author: HuangJiBin
 * @date: 2022/7/15  12:32
 * @since: 1.8
 */
@SpringBootApplication
public class DelayTaskApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DelayTaskApplication.class, args);
        /*String[] beans = context.getBeanDefinitionNames();
        for(String bean: beans){
            System.out.println(bean);
        }*/
    }
}
