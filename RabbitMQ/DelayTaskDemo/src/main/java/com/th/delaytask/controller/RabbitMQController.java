package com.th.delaytask.controller;

import com.th.delaytask.enums.DelayTypeEnum;
import com.th.delaytask.prodecer.DelayMessageProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @description:  延时消息控制层
 * @author: HuangJiBin
 * @date: 2022/7/15  12:49
 * @since: 1.8
 */
@RestController
@Slf4j
@RequestMapping("/rabbitmq")
public class RabbitMQController {
    @Autowired
    private DelayMessageProducer delayMessageProducer;

    @RequestMapping("/send")
    public String send(String message, Integer delayType){
        log.info("当前时间：{}，消息：{}，延时类型：{}", LocalDateTime.now(), message, delayType);
        //Objects.requireNonNull 非空判断
        delayMessageProducer.send(message, Objects.requireNonNull(DelayTypeEnum.getDelayTypeEnum(delayType)));
        return message;
    }

    @RequestMapping("/sendByPlugin/{message}/{delayTime}")
    public String sendByPlugin(@PathVariable("message") String message, @PathVariable("delayTime") Integer delayTime){
        log.info("RabbitMQController.class：【{}】，消息：{}，延迟时长：{}", LocalDateTime.now(), message, delayTime);
        delayMessageProducer.sendByPlugin(message, delayTime);
        return message;
    }
}
