package com.th.delaytask.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @description: RabbitMQ配置类
 * @author: HuangJiBin
 * @date: 2022/7/15  12:45
 * @since: 1.8
 */
@Configuration
public class RabbitMQConfiguration {
    //声明4个路由key，4个队列 2个交换机

    //延迟部分
    /**延迟交换机*/
    public static final String DELAY_EXCHANGE_NAME = "delay.exchange";
    /**延迟队列*/
    public static final String DELAY_QUEUE_A_NAME = "delay.queue.a";
    public static final String DELAY_QUEUE_B_NAME = "delay.queue.b";
    /**延迟队列路由key*/
    public static final String DELAY_QUEUE_A_ROUTING_KEY = "delay.queue.a.routingkey";
    public static final String DELAY_QUEUE_B_ROUTING_KEY = "delay.queue.b.routingkey";

    //死信部分
    /**死信交换机*/
    public static final String DEAD_LETTER_EXCHANGE_NAME = "dead.letter.exchange";
    /**死信队列*/
    public static final String DEAD_LETTER_QUEUE_A_NAME = "dead.letter.queue.a";
    public static final String DEAD_LETTER_QUEUE_B_NAME = "dead.letter.queue.b";
    /**死信队列路由key*/
    public static final String DEAD_LETTER_QUEUE_A_ROUTING_KEY = "dead.letter.delay_10s.routingkey";
    public static final String DEAD_LETTER_QUEUE_B_ROUTING_KEY = "dead.letter.delay_60s.routingkey";



    /**
     * @description: 声明延迟交换机
     * @Return: org.springframework.amqp.core.DirectExchange
     */
    @Bean("delayExchange")
    public DirectExchange delayExchange(){
        return new DirectExchange(DELAY_EXCHANGE_NAME);
    }

    /**
     * @description: 声明死信交换机
     * @Return: org.springframework.amqp.core.DirectExchange
     */
    @Bean("deadLetterExchange")
    public DirectExchange deadLetterExchange(){
        return new DirectExchange(DEAD_LETTER_EXCHANGE_NAME);
    }

    /**
     * @description: 声明延迟队列A，延时10s，并且绑定到对应的死信交换机
     * @Return: org.springframework.amqp.core.Queue
     */
    @Bean("delayQueueA")
    public Queue delayQueueA(){
        HashMap<String, Object> args = new HashMap<>();
        // x-dead-letter-exchange 声明队列绑定的死信交换机
        args.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE_NAME);
        // x-dead-letter-routing-key 声明队的死信路由key
        args.put("x-dead-letter-routing-key", DEAD_LETTER_QUEUE_A_ROUTING_KEY);
        // x-message-ttl 声明队列的消息TTL存活时间
        args.put("x-message-ttl", 10000);
        return QueueBuilder.durable(DELAY_QUEUE_A_NAME).withArguments(args).build();
    }

    /**
     * @description: 声明延迟队列B，延时60s，并且绑定到对应的死信交换机
     * @Return: org.springframework.amqp.core.Queue
     */
    @Bean("delayQueueB")
    public Queue delayQueueB(){
        HashMap<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE_NAME);
        args.put("x-dead-letter-routing-key", DEAD_LETTER_QUEUE_B_ROUTING_KEY);
        args.put("x-message-ttl", 60000);
        return QueueBuilder.durable(DELAY_QUEUE_B_NAME).withArguments(args).build();
    }

    /**
     * @description: 声明延时队列A的绑定绑定关系
     * @Param: [queue, exchange]
     * @Return: org.springframework.amqp.core.Binding
     */
    @Bean
    public Binding delayBindingA(@Qualifier("delayQueueA") Queue queue, @Qualifier("delayExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(DELAY_QUEUE_A_ROUTING_KEY);
    }

    /**
     * @description: 声明延时队列B的绑定绑定关系
     * @Param: [queue, exchange]
     * @Return: org.springframework.amqp.core.Binding
     */
    @Bean
    public Binding delayBindingB(@Qualifier("delayQueueB") Queue queue, @Qualifier("delayExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(DELAY_QUEUE_B_ROUTING_KEY);
    }

    /**
     * @description: 声明死信队列A， 用于接收延迟10s处理的消息
     * @Return: org.springframework.amqp.core.Queue
     */
    @Bean("deadLetterQueueA")
    public Queue deadLetterQueueA(){
        return new Queue(DEAD_LETTER_QUEUE_A_NAME);
    }

    /**
     * @description: 声明死信队列B， 用于接收延迟60s处理的消息
     * @Return: org.springframework.amqp.core.Queue
     */
    @Bean("deadLetterQueueB")
    public Queue deadLetterQueueB(){
        return new Queue(DEAD_LETTER_QUEUE_B_NAME);
    }

    /**
     * @description: 声明死信队列A的绑定关系
     * @Param: [queue, exchange]
     * @Return: org.springframework.amqp.core.Binding
     */
    @Bean
    public Binding deadLetterBindingA(@Qualifier("deadLetterQueueA") Queue queue, @Qualifier("deadLetterExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(DEAD_LETTER_QUEUE_A_ROUTING_KEY);
    }

    /**
     * @description: 声明死信队列B的绑定关系
     * @Param: [queue, exchange]
     * @Return: org.springframework.amqp.core.Binding
     */
    @Bean
    public Binding deadLetterBindingB(@Qualifier("deadLetterQueueB") Queue queue, @Qualifier("deadLetterExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(DEAD_LETTER_QUEUE_B_ROUTING_KEY);
    }

}
